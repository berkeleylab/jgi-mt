#!/usr/bin/env python

import os
import sys
import subprocess
import hashlib
from datetime import datetime
import re
import sqlite3
import glob
import getpass
import json
import urllib2
import argparse
import time
import base64

from jira.client import JIRA

import jgidb
import sdm_curl

###### EXECUTABLE PATHES ###### 


UNSHUFFLER="unshuffle.pl"
FQPAIRER="pe_fastq_splitter.pl -pp"
SAM2FASTQ="sam2fastq.py"
READ_COUNTER="count_reads.py"
READ_TRIMMER="fastqTrimmer"
DUK="duk"
A5QC="/projectb/sandbox/rqc/prod/QAQC.scripts/scripts/A5qc.jar"
THREE_TRIM="three_trim.pl"
GUESS_QUAL_FORMAT="guess_qual_format"
SAMTOOLS="samtools"
BOWTIE2="bowtie2"
BOWTIE2_BUILD="bowtie2-build"
SAM_FILTERER="extract_mapped_pairs.py"



###### DATABASES ###### 
RIBODB="/global/projectb/sandbox/rqc/qcdb/SILVA_119/SILVA_119_LSURef_SSURef_Nr99.phylum.fasta"
NEXTERA_ADPT="/global/projectb/sandbox/gaag/bbtools/jgi-bbtools/resources/nextera.fa.gz"
TRUSEQ_ADPT="/global/projectb/sandbox/gaag/bbtools/jgi-bbtools/resources/truseq.fa.gz"
ILL_AFT="/global/dna/shared/rqc/ref_databases/qaqc/databases/illumina.artifacts/Illumina.artifacts.2013.12.no_DNA_RNA_spikeins.fa"
DNA_SPIKEIN="/global/dna/shared/rqc/ref_databases/qaqc/databases/illumina.artifacts/DNA_spikeins.artifacts.2012.10.fa"
RNA_SPIKEIN="/global/dna/shared/rqc/ref_databases/qaqc/databases/illumina.artifacts/RNA_spikeins.artifacts.2012.10.NoPolyA.fa"
PHIX="/global/dna/shared/rqc/ref_databases/qaqc/databases/phix174_ill.ref.fa"
ADPTRDB="/global/projectb/sandbox/gaag/bbtools/data/adapters.fa"
AFTDB="/projectb/sandbox/rqc/qcdb/Artifacts.adapters_primers_only/Artifacts.fa"
RIBODB_OLD="/projectb/sandbox/rqc/qcdb/all.RNA.silva.greengenes.jgi.GtRNAdb.tRNADB-CE/all.RNA.silva.greengenes.jgi.GtRNAdb.tRNADB-CE.fasta"


###### IMG DATA ######
IMG_WEB_DATA_MERFS="/global/dna/projectdirs/microbial/img_web_data_merfs"
IMG_WEB_DATA_TAXONFNA="/global/dna/projectdirs/microbial/img_web_data/taxon.fna"



modules = {
           'bowtie2'                  :   ('bowtie2',),
           'samtools'                 :   ('samtools',),
           'bbmap'                    :   ('gaag','oracle-jdk',),
           'guess_qual_format'        :   ('gaag','perl',),
           'sam2fastq.py'             :   ('qaqc',),
           'duk'                      :   ('qaqc',),
           'count_reads.py'           :   ('qaqc',),
           'fastqTrimmer'             :   ('qaqc',),
           'extract_mapped_pairs.py'  :   ('qaqc',),
           'three_trim.pl'            :   ('qaqc','perl',),
           'unshuffle.pl'             :   ('qaqc','perl',),
           'pe_fastq_splitter.pl'     :   ('qaqc','perl',),
           'a5qc'                     :   ('qaqc','java',),
           'rnnotator.pl'             :   ('/global/homes/x/xmeng/modules/rnnotator/rnnotator','perl',)
}

__LIB_RE__ = re.compile("[A-Z]{4,5}")
__MISEQ_LIB_RE__ = re.compile("M[0-9]{4}")
#__LIB_RE__ = re.compile("[A-Z][A-Z][A-Z][A-Z]")
__SPID_RE__ = re.compile("[0-9][0-9][0-9][0-9][0-9][0-9][0-9]")
__SEQUNIT_RE__ = re.compile("\d+.\d.\d+.[ATCG]+(.fastq.gz|.srf)")
def __query_field__(query):
    if __SPID_RE__.match(str(query)):
        return "WHERE i.sp_project_id  = %s"
    elif __LIB_RE__.match(query) or __MISEQ_LIB_RE__.match(query):
        return "WHERE i.lib_name='%s'"
    elif __SEQUNIT_RE__.match(str(query)):
        return "WHERE i.rqc_seq_unit_name = '%s'"
    else:
        return None
#end __query_field__

def __query_ITS__(query,sql_tmpl,verbose=False):
    sql = sql_tmpl + __query_field__(query) % query
    if verbose:
        sys.stderr.write("Querying ITS:\n%s\n" % sql)
    return jgidb.queryDb("ITS",sql)
#end __query_ITS__

def __get_arguments__(*args,**kwargs):
    query = args[0]
    verbose=False
    if len(kwargs) > 0:
        if 'verbose' in kwargs:
            verbose = kwargs['verbose']
    elif len(args) > 1:
        verbose = args[1]
    return (query,verbose)
#end __get_arguments__
    
def get_product_info(*args,**kwargs):
    '''This function queries DW and returns library name, SPID, and SP name
    :param query: a library name or a sequencing project ID to query with
    :type query: str.
    :param verbose: print the DW query to stderr
    :type verbose: bool.
    :returns: a tuple (sequencing_project_id, sequencing_product_name)
    :rtype: a tuple of strings
    '''
    (query,verbose) = __get_arguments__(*args,**kwargs)
    spid = str(query)
    if not __SPID_RE__.match(spid):
        spid = get_proj_info(spid,verbose)[0][0]
    sql = "SELECT DISTINCT i.sequencing_product_id,"\
                          "i.sequencing_product_name "\
           "FROM DW.sequencing_project i WHERE i.sequencing_project_id = %s" % str(spid)
    #cursor = __query_ITS__(spid,sql,verbose)
    cursor = jgidb.queryDb("ITS",sql)
    if cursor:
        row = cursor.next()
        ret = (row[0],row[1])
        return ret
    else:
        return None
#end get_product_name
    

def get_proj_info (*args,**kwargs):
    '''This function queries DW and returns SPID, library name, SP name, and sequnit
    :param query: a library name or a sequencing project ID to query with
    :type query: str.
    :param verbose: print the DW query to stderr
    :type verbose: bool.
    :returns: a tuple (sequencing_project_id, library_name, sequencing_project_name)
    :rtype: a tuple of strings
    '''
    (query,verbose) = __get_arguments__(*args,**kwargs)
    sql = "SELECT i.sp_project_id,"\
                 "i.lib_name,"\
                 "i.sp_project_name, "\
                 "i.rqc_seq_unit_name "\
                 "FROM dw.rqc_genealogy i "
    cursor = __query_ITS__(query,sql,verbose)
    if cursor:
        ret = list()
        for row in cursor:
            ret.append((row[0],row[1],row[2],row[3]))
        return ret
    else:
        return None
#end get_proj_info

def get_project_manager(*args,**kwargs):
    '''This function queries DW and returns project manager
    :param query: a library name or a sequencing project ID to query with
    :type query: str.
    :param verbose: print the DW query to stderr
    :type verbose: bool.
    :returns: the project manager's name
    :rtype: a string
    '''
    (query,verbose) = __get_arguments__(*args,**kwargs)
    spid = get_proj_info(query,verbose)[0][0]
    sql = "SELECT i.seq_proj_manager_name, c.EMAIL_ADDRESS  FROM DW.sequencing_project i, DW.CONTACT c  WHERE c.CONTACT_ID = i.sequencing_project_manager_id AND i.SEQUENCING_PROJECT_ID = %s" %  str(spid);
#    sql = "SELECT i.seq_proj_manager_name FROM DW.sequencing_project i WHERE i.SEQUENCING_PROJECT_ID = %s" % str(spid)
    cursor = jgidb.queryDb("ITS",sql)
    if cursor:
        row = cursor.next()
        return (row[0],row[1])
    else:
        return None
#end get_project_manager

def get_proposal_info (*args,**kwargs):
    '''This function queries DW and proposal information
    :param query: a library name or a sequencing project ID to query with
    :type query: str.
    :param verbose: print the DW query to stderr
    :type verbose: bool.
    :returns: ( proposal_id, proposal_title, pi_name, pi_email )
    :rtype: a tuple of strings
    '''
    (query,verbose) = __get_arguments__(*args,**kwargs)
    sql = "SELECT i.proposal_id, "\
                 "i.proposal_title, "\
                 "i.proposal_pi_name, "\
                 "i.proposal_pi_email "\
                 "FROM DW.rqc_genealogy i "
    cursor = __query_ITS__(query,sql,verbose)
    if cursor:
        row = cursor.next()
        return (row[0],row[1],("%s" % (row[2])),row[3])
    else:
        return None
#end get_proposal_info

def get_seq_units(library, verbose=False):
    '''This function queries DW and returns all sequence unit names for a library
    :param library: the library name to get sequnits for
    :type query: str.
    :param verbose: print the DW query to stderr
    :type verbose: bool.
    :returns: a tuple containing all sequnits for :param:library
    :rtype: a tuple 
    '''
    cmd = "module unload jamo;module load jamo/prod; jamo report select file_name where metadata.library_name=%s and metadata.fastq_type='sdm_normal' and metadata.rqc.usable != 'false' " % (library)
    if verbose:
        sys.stderr.write("Querying JAMO:\n%s\n" % cmd)
    ret=list()
    for line in os.popen(cmd).readlines():
        line=line.rstrip()
        ret.append(line)
    return tuple(ret)
#    sql = "SELECT s.rqc_seq_unit_name "\
#          "FROM DW.all_inclusive_report s WHERE s.lib_name = '%s' "\
#          "AND (s.rqc_state IS NULL OR s.rqc_state = 'usable')"\
#          "ORDER BY s.rqc_seq_unit_name"
#
#    if verbose:
#        sys.stderr.write("Querying ITS:\n%s\n" % sql)
#    sequnits  = [ row[0] for row in jgidb.queryDb("ITS", sql % library) ]
#    return tuple(sequnits)
##end get_seq_units

def get_module (*programs):
    '''Return the modules needed for running these programs
    :param programs: 
    :type program: str.
    '''
    ret = list()
    for program in programs:
        if program in modules:
            ret.append(modules[program])
        else:
            raise ValueError("Uknown program: %s" % program)
    return ret
#end get_module

def run(cmd,wait=True,out=None,err=None):
    cmd_ar = None
    if type(cmd) is list:
        cmd_ar = cmd
    elif type(cmd) is str:
        cmd_ar = cmd.split()
    else:
        raise TypeError("cmd must be a string or a list. You gave me a %s"%str(type(cmd)))

    proc = subprocess.Popen(cmd_ar, stdout=out, stderr=out)
    if wait:
        proc.wait()
    else:
        return proc
#end run

# strips off any compression extension and then any FastQ extension of a filename
def get_fastq_basename(fastq):
    ret=""
    start = fastq.rfind("/")+1
    if fastq.endswith(".gz"):
        ret = fastq[start:-3]
    elif fastq.endswith(".bz2"):
        ret = fastq[start:-4]
    else:
        ret = fastq[start:]
    if ret.endswith(".fastq"):
        ret = ret[0:-6]
    elif ret.endswith(".fq"):
        ret = ret[0:-3]
    return ret
#end get_fastq_basename

def get_seq_unit_basename(seq_unit):
    if seq_unit.endswith(".srf"):
        return seq_unit.replace(".srf", "")
    else:
        return get_fastq_basename(seq_unit)
#end get_seq_unit_basename

def ref_base(fasta):
    start_idx = fasta.rfind("/")+1
    if fasta.endswith(".fasta") or fasta.endswith(".FASTA"):
        return fasta[start_idx:-6]
    elif fasta.endswith(".fa"):
        return fasta[start_idx:-3]
    elif fasta.endswith(".fna"):
        return fasta[start_idx:-4]
    else:
        return fasta[start_idx:]
#ref_base

# returns the reader to use for this given file
def get_reader(fastq):
    if fastq.endswith(".gz"):
        return "zcat"
    elif fastq.endswith(".bz2"):
        return "bzcat"
    else:
        return "cat"
#end get_reader

# A function for testing if a list of fastqs can all be read with the sam read (i.e. cat, zcat, or bzcat)
def same_reader(fastqs):
    reader = get_reader(fastqs[0])
    if len(fastqs) > 1:
        for fq in fastqs[1:]:
            if get_reader(fq) != reader:
                return 0
    return 1

def guess_qual_format(fastq):
    cmd = [ GUESS_QUAL_FORMAT, '-type', 'fastq', fastq ]
    fmt = subprocess.check_output(cmd, stderr=open(os.devnull,"w"))[:-1]
    if (fmt == "sanger"):
        return 33
    elif fmt == "solexa" or fmt == "illumina":
        return 64
    else:
        raise ValueError("Unrecognizable qual format: %s" % fmt)
#end guess_qual_format

def get_total_file_size(fastqs):
    ret=0
    for fq in fastqs:
        ret += os.path.getsize(fq)
    return ret

def read_float(path):
    f = open(path,"r")
    f_ret = float(f.readline()[:-1])
    f.close()
    return f_ret
#end read_float

def read_int(path):
    f = open(path,"r")
    f_ret = int(f.readline()[:-1])
    f.close()
    return f_ret
#end read_int

class FastqMetaData(object):
    def __init__(self, data_string):
        ar=data_string.split(",")
        self.ncbi_name = ar[0]
        self.seq_project_name = ar[1]
        self.seq_project_id = ar[2]
        self.product_type = ar[3]
        self.sample_id = ar[4]
        self.library = ar[5]
        self.library_type = ar[6]
        self.lab_insert_size = ar[7]
        self.box_type = ar[8]
        self.run_profile = ar[9]
        self.reads = int(ar[10])
        self.bases = int(ar[11])
        self.qc_date = ar[12]
        self.qc_result = ar[13]
        self.parent_lib = ar[14]
        self.file_path = ar[15][:-1]
#end class FastqMetaData

def md5sum (path):
    md5 = hashlib.md5()
    with open(path,"rb") as f:
        for chunk in iter(lambda : f.read(128*md5.block_size), b''):
            md5.update(chunk)
    return md5.hexdigest()


def get_fasta_stats(fasta_path):
    cmd = [ "stats.sh", "-format=3", "n_", "in=%s" % fasta_path ]
    keys,values = [ x.split('\t') for x in subprocess.check_output(cmd).splitlines(False)]
    for i in xrange(len(values)):
        if "." in values[i]:
            values[i] = float(values[i])
        else:
            values[i] = int(values[i])
    return dict(zip(keys,values))
#end get_fasta_stats

def get_analyst_contact_id(verbose=False):
    analyst_fullname = get_user_name()
    contact_id = 0
    if len(analyst_fullname.split()) ==2:
        (first,last) = analyst_fullname.split()
        sql = "SELECT c.contact_id FROM DW.contact c WHERE c.first_name = '%s' AND c.last_name = '%s'" % (first,last)
        if verbose:
            sys.stderr.write("Querying ITS:\n%s\n" % sql)
        contact_id = jgidb.queryDb("ITS",sql).next()[0]
    elif (analyst_fullname=='JGI QC User'):
        contact_id = 14829
    else:
        pass
    return contact_id
#get_current_analyst

def get_IMG_sequence_location(toid):
    mg_loc = "%s/%s/assembled/fna" % (IMG_WEB_DATA_MERFS,toid)
    iso_loc = "%s/%s.fna" % (IMG_WEB_DATA_TAXONFNA,toid)
    if os.path.isdir(mg_loc):
        return mg_loc
    elif os.path.isfile(iso_loc):
        return iso_loc
    else:
        return None
#end get_IMG_sequence_location

def extract_IMG_sequence(toid, out, verbose=False):
    loc = get_IMG_sequence_location(toid)
    if os.path.isdir(loc):
        if verbose:
            sys.stderr.write("Retrieving IMG Metagenome from %s\n" % loc)
        if len(glob.glob("%s/*.sdb" % loc)) == 0:
            raise IOError("%s does not contain any sequence data" % loc)
        for sdb in glob.glob("%s/*.sdb" % loc):
            if verbose:
                sys.stderr.write("Extracting sequence from %s\n" % sdb)
            cursor = sqlite3.connect(sdb).cursor()
            for entry in cursor.execute("SELECT * FROM scaffold_fna"):
                out.write(">%s\n"% str(entry[0]))
                i = 0
                while i < len(entry[1])-100:
                    out.write("%s\n" % entry[1][i:i+100])
                    i += 100
                out.write("%s\n" % entry[1][i:])
    elif os.path.isfile(loc):
        if verbose:
            sys.stderr.write("Retrieving IMG Isolate genome from %s\n" % loc)
        for line in open(loc,"r"):
            out.write(line)
#end extract_IMG_sequence

def get_taxon_oid(spid):
    '''
    Get the IMG Taxon OID for the genome in IMG with the given sequencing project id
    '''
    sql = "SELECT t.taxon_oid FROM taxon t WHERE t.jgi_project_id = %d" % spid
    taxon_oid = jgidb.queryDb("IMG",sql).next()[0]
    return taxon_oid
#end get_taxon_oid

# make directory if exists, and return if its okay to proceed. return false if dirname exists as file
def make_dir(dirname):
    if os.path.exists(dirname):
        if os.path.isdir(dirname):
            return True
        else:
            return False
    else:
        os.mkdir(dirname)
        return True
#end make_dir

def wait_for_gp_job (jobid,wait_time=120):
    cmd = [ "isjobcomplete", jobid ]
    time.sleep(wait_time)
    # non-zero means incomplete
    while subprocess.check_call(cmd) != 0:
        time.sleep(wait_time)
#end wait_for_gp_job

# regex and command for calling my wrappers that create shell scripts that get submit to genepool
qsub_out_re = re.compile("Your job (?P<job_id>[0-9]+)")
def call_qsub_wrapper(cmd, fwd_stdout=True):
    #qsub message: Your job <job_id> ("name of job") has been submitted
    stdout = None
    try:
        stdout = subprocess.check_output(cmd)
    except Exception:
        raise Exception ("Error calling: %s" % " ".join(cmd))
    for line in stdout.split('\n'):
        if fwd_stdout:
            sys.stdout.write("%s\n" % line)
        m = qsub_out_re.match(line)
        if m:
            return m.group('job_id')
    return None
#end call_remove_rRNA

def __get_Curl__():
    return sdm_curl.Curl(os.environ.get('JAMO_HOST', "https://sdm2.jgi-psf.org"))
#end __get_Curl__

def get_fastq_abspath(file_name,library=False):
    curl = __get_Curl__()
    data = None
    if library:
        data = {'file_type':'fastq.gz','metadata.library_name': file_name, 'metadata.fastq_type':'sdm_normal', 'metadata.rqc.usable':True}
    else:
        data={'file_name':file_name}
    result = curl.post('api/metadata/query',data=data)
    if len(result) > 0:
        abspath = "%s/%s" % (result[-1]['file_path'],result[-1]['file_name'])
        return abspath
    return None
#end get_fastq_abspath

def get_release_location(spid):
    curl = __get_Curl__()
    data = {'metadata.sequencing_project_id':spid, 'metadata.template_name': 'metatranscriptome'}
    result = curl.post('api/metadata/query', data=data)
    if len(result) > 0:
        for res in result:
            print "%s: %s" % (res['file_name'], res['file_path'])
        return "%s" % result[-1]['file_path']
    return None
#end get_release_location

def get_user_name(user=None):
    if user is None:
        user = getpass.getuser()
    cmd = ["getent", "passwd", user]
    name = subprocess.check_output(cmd).split(":")[4]
    return name
#end get_user_name

def get_analyst_info(user=None, verbose=False):
    if user is None:
        user = getpass.getuser()
    analyst_fullname = get_user_name(user=user)
    email = None
    if user == 'qc_user':
        email = "qc_user@genepool.nersc.gov"
    else:
        (first,last) = analyst_fullname.split()
        sql = "SELECT c.email_address FROM DW.contact c WHERE c.first_name = '%s' AND c.last_name = '%s' AND c.institution = 'Joint Genome Institute'" % (first,last)
        if verbose:
            sys.stderr.write("Querying ITS:\n%s\n" % sql)
        email = jgidb.queryDb("ITS",sql).next()[0]
    return (analyst_fullname, email)
#get_current_analyst

def get_pmo_data(url,verbose=False):
    if verbose:
        sys.stderr.write("Calling %s\n" % url)
    return json.load(urllib2.urlopen(url))
#end get_pmo_data

def is_analysis_project_abandoned(analysis_project):
    ''' Return true if the analysis_project status_name is set to Abandonded
        This takes in an analysis_project dictionary object
    '''
    if analysis_project['status_name'] == "Abandoned":
        return True
    else:
        return all(is_analysis_task_abandoned(at) for at in analysis_project['analysis_tasks'])
#end is_analysis_project_abandoned

def is_analysis_task_abandoned(analysis_task):
    return analysis_task['status_name'] == "Abandoned"
#is_analysis_task_abandoned

__PMO_WEBSERVICE_AT__ = "http://proposals.jgi.doe.gov/pmo_webservices/analysis_task"
def get_sequencing_project_ids(analysis_task_id, verbose=False):
    url = "%s/%d" % ( __PMO_WEBSERVICE_AT__ , analysis_task_id )
    data = get_pmo_data(url, verbose) 
    sequecing_projects = data['uss_rw_analysis_task']['analysis_project']['sequencing_projects']
    ret = [ sp['sequencing_project_id'] for sp in sequecing_projects ]
    return ret
#end get_sequencing_projects

__PMO_WEBSERVICE_AP__ = "http://proposals.jgi.doe.gov/pmo_webservices/analysis_project"
def get_analysis_projects(sequencing_project_id, verbose=False, abandoned=False):
    url = "%ss?sequencing_project_id=%d" % ( __PMO_WEBSERVICE_AP__ , sequencing_project_id )
    data = [ ap['uss_analysis_project'] for ap in get_pmo_data(url, verbose) ] 
    if not abandoned:
       data = filter (lambda ap: not is_analysis_project_abandoned(ap), data)
    return data
#end get_analysis_projects

def get_analysis_project_ids(sequencing_project_id, verbose=False, abandoned=False):
    analysis_projects = get_analysis_projects(sequencing_project_id, verbose=verbose, abandoned=abandoned)
    ret = [ ap['analysis_project_id'] for ap in analysis_projects ]
    return ret
#end get_analysis_project_ids

def get_analysis_project(analysis_project_id, verbose=False):
    url = "%s/%d" % (__PMO_WEBSERVICE_AP__,analysis_project_id)
    data = get_pmo_data(url, verbose)
    ret = data['uss_analysis_project']
    return ret
#end get_analysis_project

def is_analysis_project_type(project_type, analysis_project):
    return analysis_project['analysis_project_type_name'] == project_type
#end is_analysis_project_type

def get_analysis_tasks(analysis_project_id, verbose=False, abandoned=False):
    url = "%s/%d" % ( __PMO_WEBSERVICE_AP__ , analysis_project_id )
    data = get_pmo_data(url, verbose)['uss_analysis_project']['analysis_tasks'] 
    if not abandoned:
       data = filter (lambda at: not is_analysis_task_abandoned(at), data)
    return data
#end get_analysis_tasks

def get_analysis_task_ids(analysis_project_id, verbose=False):
    analysis_tasks = get_analysis_tasks(analysis_project_id, verbose)
    return [ at['analysis_task_id'] for at in analysis_tasks ]
#end get_analysis_task_ids

def is_analysis_task_type(task_type, analysis_task):
    return analysis_task['analysis_task_type_name'] == task_type
#end is_analysis_task_type

def analysis_task_status(task_id=None,state=None):
    #url = "https://wip-stage.jgi-psf.org/pmo_webservices/analysis_task/" + str(task_id)
    url =  "https://proposals.jgi.doe.gov/pmo_webservices/analysis_task/" + str(task_id)
    enumerate = {"Created":1,"Complete":9,"In Progress":6,"Output Created and Awaiting Review":8}
    enumeraterev = dict (zip(enumerate.values(),enumerate.keys()))
    if task_id ==None:
        return json.dumps(enumerate,indent=3)

    codestring = "VG9rZW4gdG9rZW49NGExMTI2ZjFiM2JhMzIyYmQ4ZGZiZGVlMzkzMGE1M2Y=\n"
    header = {"Authorization": base64.decodestring(codestring) }

    if state==None:#get current state and return it (fetch method)
        req = urllib2.Request(url,headers=header)
        record = json.loads(urllib2.urlopen(req).read())
        status = record["uss_rw_analysis_task"]["current_status_id"]
        return status

    #convert state input to numeric if not already
    if str(state).isdigit():
        if state not in enumeraterev.keys():
            sys.stderr.write("state_id:" + str(state) + " not supported\n")
            return 0
        else:
            state_id = state
    else:
        if state in enumerate.keys():
            state_id = enumerate[state]
        else:
            sys.stderr.write("state_id:" + str(state) + " not supported\n")
            return 0

    url = url + "/state_transition"
    data = {"updated-by-cid":get_analyst_contact_id(),"target-state-id":state_id} #bfoster2521                                                                                                                 
    req = urllib2.Request(url,data=json.dumps(data),headers=header)
    code = urllib2.urlopen(req).getcode() #204 successful post of data
    if code != 204:
        sys.stderr.write("exit code:" + str(code) + " indicates unsuccessful post to " + url + " with " + json.dumps(data))
        return 0
    else:
        return state_id


def get_analysis_project_states(*args,**kwargs):
    '''
    Returns a tuple of two lists:
    the first is a list of all the completed analysis projects
    the second is a list of all the incomplete analysis projects
    '''
    
    (query,verbose) = __get_arguments__(*args,**kwargs)
    spids = set(proj[0] for proj in get_proj_info(query,verbose))
    analysis_projects = list()
    for spid in spids:
        analysis_projects += get_analysis_projects(spid, verbose)
    curl = __get_Curl__()
    complete = list()
    incomplete = list()
    apids = map(lambda ap: ap['analysis_project_id'],analysis_projects)
    qdat = {'metadata.analysis_project_id': {'$in': apids}, 'file_type':'report'}
    if verbose:
        sys.stdout.write("querying JAMO with %s\n" % str(qdat))
    data = curl.post('api/metadata/query', data=qdat)
    completed_apids = set(map(lambda x: x['metadata']['analysis_project_id'], data))
    for ap in analysis_projects:
        apid = ap['analysis_project_id']
        if apid in completed_apids:
            complete.append(ap)
        else:
            incomplete.append(ap)
    return (complete,incomplete)
#end get_analysis_project_states

def get_usable_libraries(sequencing_project_id, verbose=False):
    '''return dictionary of ret[lib][su1,su2]'''
    cmd = "module unload jamo;module load jamo/prod; jamo report select metadata.library_name,file_name where metadata.sequencing_project_id=%s and metadata.fastq_type='sdm_normal' and metadata.rqc.usable != 'false' " % (sequencing_project_id)
    if verbose:
        sys.stderr.write("Querying JAMO:\n%s\n" % cmd)
    ret=dict()
    for line in os.popen(cmd).readlines():
        line=line.rstrip()
        row=line.split("\t")
        if row[0] not in ret:
            ret[row[0]] = list()
        ret[row[0]].append(row[1])
        return ret
    return None
#end get_usable_libraries

#    sql = "SELECT a.lib_name, a.rqc_seq_unit_name "\
#                 "FROM DW.all_inclusive_report a "\
#                 "WHERE a.sp_project_id = %d "\
#                   "AND a.lib_name IS NOT NULL "\
#                   "AND a.rqc_seq_unit_name IS NOT NULL "\
#                   "AND a.sow_status IN ('Complete','In Progress','Awaiting QA/QC Analysis')"\
#                   "AND (a.rqc_state IS NULL OR a.rqc_state = 'usable')"
#    sql = sql % sequencing_project_id
#    if verbose:
#        sys.stderr.write("Querying ITS:\n%s\n" % sql)
#    cursor = jgidb.queryDb("ITS",sql)
#    if cursor:
#        ret = dict()
#        for row in cursor:
#            if row[0] not in ret:
#                ret[row[0]] = list()
#            ret[row[0]].append(row[1])
#        return ret
#    else:
#        return None
#end get_usable_libraries
     
def get_library_from_sequnit(sequnit, verbose=False):
    sql = "SELECT a.lib_name "\
                 "FROM DW.rqc_genealogy a "\
                 "WHERE a.rqc_seq_unit_name in ('%s')"
    if isinstance(sequnit, (list,tuple)):
        sql = sql % "', '".join(sequnit)
    else:
        sql = sql % sequnit
        
    if verbose:
        sys.stderr.write("Querying ITS:\n%s\n" % sql)
    cursor = jgidb.queryDb("ITS",sql)
    ret = None
    if cursor:
        ret = [ row[0] for row in cursor ]
    return ret
#end get_library_from_sequnit 



def get_duplicate_analyses(template,metadata_key):
    curl = __get_Curl__()
    jat_data = curl.post('api/analysis/query', data={'metadata.template_name': template })
    data = map(lambda x: (x['metadata'][metadata_key],x['key']), jat_data)
    released = dict()
    for metadata,jat_key in data:
        if metadata not in released:
            released[metadata] = list()
        released[metadata].append(jat_key)
    ret = [ ( x, released[x] ) for x in released.keys() if len(released[x]) > 1 ]
    return tuple(ret)
#end get_duplicate_analyses

def get_released_projects(template,*metadata_key):
    '''
    Query JAT for releases of a certain template, and return 
    the specified metadata keys for each release
    '''
    curl = __get_Curl__()
    def get_metadata(x):
        ret = list()
        for key in metadata_key:
            ret.append(x['metadata'][key])
        ret.append(x['key'])
        return ret
    jat_data = curl.post('api/analysis/query', data={'metadata.template_name': template })
    data = map(get_metadata, jat_data)
    return data
#end get_released_projects

def is_analysis_project_unreleased(analysis_project):
    curl = __get_Curl__()
    apid = analysis_project
    if isinstance(analysis_project,dict):
        apid = analysis_project['analysis_project_id']
    jat_data = curl.post('api/analysis/query', data={'metadata.analysis_project_id':apid})
    return len(jat_data) == 0
#end is_analysis_project_unreleased

def is_asm_analysis_project_unreleased(analysis_project):
    curl = __get_Curl__()
    apid = analysis_project
    if isinstance(analysis_project,dict):
        apid = analysis_project['analysis_project_id']
    jat_data = curl.post('api/metadata/query', data={'metadata.analysis_project_id':apid,'file_type':'contigs'})
    return len(jat_data) == 0
#end is_asm_analysis_project_unreleased

def is_aln_analysis_project_unreleased(analysis_project):
    curl = __get_Curl__()
    apid = analysis_project
    if isinstance(analysis_project,dict):
        apid = analysis_project['analysis_project_id']
    jat_data = curl.post('api/metadata/query', data={'metadata.analysis_project_id':apid,'file_type':'alignment'})
    return len(jat_data) == 0
#end is_asm_analysis_project_unreleased


def is_analysis_task_unreleased(analysis_task):
    curl = __get_Curl__()
    atid = analysis_task
    if isinstance(analysis_task,dict):
        atid = analysis_task['analysis_task_id']
    jat_data = curl.post('api/analysis/query', data={'metadata.analysis_task_id':atid})
    return len(jat_data) == 0
#end is_analysis_task_unreleased

def get_analysis_project_jat_key(analysis_project_id):
    curl = __get_Curl__()
    jat_data = curl.post('api/analysis/query', data={'metadata.analysis_project_id':analysis_project_id})
    keys = map(lambda x: x['key'], jat_data)
    return tuple(keys)
#end get_analysis_project_jat_key

def get_analysis_task_jat_key(analysis_task_id):
    curl = __get_Curl__()
    jat_data = curl.post('api/analysis/query', data={'metadata.analysis_task_id':analysis_task_id})
    keys = map(lambda x: x['key'], jat_data)
    return tuple(keys)
#end get_analysis_task_jat_key

def release_to_jat_with_copy_verification(template, directory, verbose=False,timeout=60):
    try:
        jat_key = release_to_jat(template, directory, verbose)
    except Exception as e:
        sys.stderr.write(e.message)
        raise
    waiting_time=0
    while(JAT_files_copied(jat_key)==False and waiting_time <= timeout):
        time.sleep(60) #sleep for 1 minute
        waiting_time = waiting_time + 1
    if JAT_files_copied(jat_key)==False:
        sys.stderr.write("JAT release " + jat_key + " failed to copy within " + str(timeout) + " minutes\n")
        raise Exception
    else:
        return jat_key
#end release_to_jat_with_copy_verification

def release_to_jat(template, directory, verbose=False):
    jat_cmd = " ".join([ "jat", "import" , template, directory ])
    if verbose:
        sys.stderr.write("Calling JAT command: %s\n" % jat_cmd)
    try:
        jat_key = subprocess.check_output(jat_cmd,shell=True).split("\n")[0].split(" ")[-1]
        return jat_key
    except subprocess.CalledProcessError as cpe:
        raise Exception("JAT release failed with the following error:\n%s" % (cpe.output))

def is_sequencing_project_released(sequencing_project_id):
    curl = __get_Curl__()
    analysis_project_ids = get_analysis_project_ids(sequencing_project_id)

__INCOMPLETE_RELEASE_FILE_STATUSES__ = {'COPY_READY','COPY_IN_PROGRESS','COPY_FAILED','REGISTERED'}
def jamo_copy_complete(jamo_file):
    return jamo_file['file_status'] not in __INCOMPLETE_RELEASE_FILE_STATUSES__

def all_files_copied(key,value):
    curl = __get_Curl__()
    result = curl.post('api/metadata/query', data={key:value})
    return reduce(lambda x,y: x and y, map(jamo_copy_complete, result))

def analysis_project_files_copied(analysis_project_id):
    return all_files_copied('metadata.analysis_project_id',analysis_project_id)
    
def analysis_task_files_copied(analysis_task_id):
    return all_files_copied('metadata.analysis_task_id',analysis_task_id)

def sequencing_project_files_copied(sequencing_project_id):
    return all_files_copied('metadata.sequencing_project_id',sequencing_project_id)

def JAT_files_copied(jat_key):
    return all_files_copied('metadata.jat_key',jat_key)

def get_argparser(usage,desc=None,epilog=None):
    '''Creates an ArgumentParser from with the given usage. Automatically
       sets help statement format to max width 80
    :rtype: argparse.ArgumentParser
    '''
    formatter_class=lambda prog: argparse.RawTextHelpFormatter(prog,max_help_position=80)
    parser = argparse.ArgumentParser(usage=usage, description=desc, epilog=epilog, formatter_class=formatter_class)
    return parser

def get_ap_tool_task_status(atid):
    cmd="module load jgi-rqc ; ap_tool.py -at " + str(atid) + " |  grep Status ";
    try:
        status = os.popen(cmd).readlines()[0].rstrip()
        status = re.sub(r'^.*\((\d+)\).*$', r'\1', status)
    except:
        status=None
    return status

def set_ap_tool_task_status(atid,status):
    cmd = "module load jgi-rqc;ap_tool.py -at " + str(atid) + " -cmd update -s " + str(status)
    try:
        status = os.popen(cmd).readlines()[0].rstrip()
        status = re.sub(r'^.*\((\d+)\).*$', r'\1', status)
    except:
        status = None
    return status

def its_ap_to_taxonoid(apid):
  oid = ""
#  url = "https://gold.jgi-psf.org/rest/analysis_project/" + str(apid)
#  url = "https://gold.jgi.doe.gov/rest/jgi_project/" + str(spid)
  url = "https://gold.jgi.doe.gov/rest/analysis_project/" + str(apid)
  #authKey=base64.b64encode(username + ':' + password)
  authKey = "amdpX2dhZzozSzgqZiV6"
  headers = {"Content-Type":"application/json", "Authorization":"Basic " + authKey}
  request = urllib2.Request(url)
  data=dict()
  for key,value in headers.items():
    request.add_header(key,value)
  try:
#    print url
    response = urllib2.urlopen(request)
    data = json.loads(response.read())
  except:
#    print "exception"
    pass
    #print response.info().headers

#  print json.dumps(data,indent=3)
  oid = int()
  if "imgTaxonOid" in data.keys():
    oid = data["imgTaxonOid"]
  return oid
