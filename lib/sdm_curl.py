"""
    
    curl.py is a script containing Curl class that makes making curl calls easy

"""

import json
try:
    import urllib2
except ImportError:
    import urllib as urllib2
import itertools
import mimetools
import mimetypes
from cStringIO import StringIO
from decimal import Decimal
from time import sleep

class CurlHttpException(Exception):

    def __init__(self, httpError ):
        self.response = httpError.readlines()
        self.url = httpError.geturl()
        self.code = httpError.getcode()
        Exception.__init__(self, 'call to: %s threw code: %d %s'%(self.url, self.code,self.response)) 
    
    def __repr__(self):
        return 'call to: %s threw code: %d'%(self.url, self.code)

cachedCurls = {}
def handler(obj):
    if isinstance(obj, type):
        return obj.__name__
    elif hasattr(obj, 'isoformat'):
        return obj.isoformat()
    elif isinstance(obj, Decimal):
        return "%.2f" % obj
    # elif isinstance(obj, ...):
    #     return ...
    elif hasattr(obj, '__str__'):
        return str(obj)
    else:
        raise TypeError('Object of type %s with value of %s is not JSON serializable' % (type(obj), repr(obj)))

def __call(method, url, **kwargs):
    split = url.split('/',3)
    server = '/'.join(split[:3])
    leftover = '/'.join(split[3:])
    if server in cachedCurls:
        curl = cachedCurls[server]
    else:
        curl = Curl(server,retry=0)
        cachedCurls[server]=curl
    if method=='GET' and 'cache' in kwargs and kwargs['cache']:
        data = kwargs
        del kwargs['cache']
        return curl.get(leftover, data, cache=True)
    return curl._Curl__call(leftover, method, data=kwargs)


def get(url, *args, **kwargs):
    return __call('GET', url, *args, **kwargs)

def post(url, *args, **kwargs):
    return __call('POST', url, *args, **kwargs)

class Curl:

    def __init__(self, server, userName = None, userPass = None, oauth = None,appToken=None, retry=3):
        self.userData = None
        if userName is not None and userPass is not None:
            self.setupAuth(userName,userPass)
        elif oauth is not None:
            self.userData = "OAuth %s" % oauth
        elif appToken is not None:
            self.userData = 'Application %s'%appToken
        self.server=server
        self.cache = {}
        self.retryAttempts = retry

    
    def setupAuth(self,userName,password):
        self.userName = userName
        self.userPass = password
        self.userData = "Basic " + (userName + ":" + password).encode("base64").rstrip()

    def __retry(self, request):
        for i in range(self.retryAttempts):
            try:
                return urllib2.urlopen(request)
            except:
                sleep(10)
        #we should store this in a file to be called if it is critical

    def __call(self, url, method, data=None, output='json'):
        fullUrl = self.server+'/'+url
        if data is not None and method in ('GET','DELETE'):
            url_values = ''
            for key in data:
                url_values += '%s=%s&'%(key,data[key])
            if url_values != '':
                fullUrl += '?' + url_values
        elif data is None:
            data = ''

        req = urllib2.Request(fullUrl)
        req.get_method = lambda : method
        if isinstance(data, MultiPartForm):
            body = str(data)
            req.add_header('Content-type', data.get_content_type())
            req.add_header('Content-length', len(body))
            req.add_data(body)
        else:
            if not isinstance(data, basestring):
                data = json.dumps(data,default=handler)
                req.add_header('Content-type','application/json')
            else:
                data = data.replace("+","%2B")
            if method not in ('GET','DELETE'):
                req.add_data(data)
                req.add_header('Content-length', len(data))

        if self.userData is not None:
            req.add_header('Authorization',self.userData)
        
        f = None

        try:
            f =  urllib2.urlopen(req)
        except urllib2.HTTPError as e:
            exception = CurlHttpException(e)
            if exception.code == 500:
                f = self.__retry(req)
            if f is None:
                raise exception
        except urllib2.URLError as e:
            #the server is not up maybe we should try again...
            f = self.__retry(req)
            if f is None:
                raise e
        response = f.read()
        f.close()
        if output == 'json':
            d = json.loads(response)
            return d
        return response

    def post(self,url,data=None,output='json', **kwargs):
        if kwargs is not None and len(kwargs)>0:
            data = kwargs
        return self.__call(url,'POST', data, output)

    def put(self,url,data=None,output='json', contenttype='application/json', **kwargs):
        if kwargs is not None and len(kwargs)>0:
            data = kwargs
        return self.__call(url, 'PUT', data, output)
 
    def delete(self,url,data=None, output='json',contenttype='application/json', **kwargs):
        if kwargs is not None and len(kwargs)>0:
            data = kwargs
        return self.__call(url, 'DELETE', data, output)       
    
    def get(self, url, data=None, output='json', cache=False,**kwargs):
        if kwargs is not None and len(kwargs)>0:
            data = kwargs
        if cache:
            if url not in self.cache:
                self.cache[url] = self.__call(url, 'GET', data, output)
            return self.cache[url]
        return self.__call(url, 'GET', data, output)

    def toStruct(self, data):
        temp = {}
        ret = lambda : 1
        for table in data:
            var = lambda : 2
            var.__dict__.update(data[table])
            temp[table]=var
        ret.__dict__.update(temp)
        return ret

class MultiPartForm(object):
    """Accumulate the data to be used when posting a form."""

    def __init__(self):
        self.form_fields = []
        self.files = []
        self.boundary = mimetools.choose_boundary()
        return

    def get_content_type(self):
        return 'multipart/form-data; boundary=%s' % self.boundary

    def add_field(self, name, value):
        """Add a simple field to the form data."""
        self.form_fields.append((name, value))
        return

    def add_file(self, fieldname, filename, fileHandle, mimetype=None):
        """Add a file to be uploaded."""
        body = fileHandle.read()
        if mimetype is None:
            mimetype = mimetypes.guess_type(filename)[0] or 'application/octet-stream'
        self.files.append((fieldname, filename, mimetype, body))
        return

    def __str__(self):
        """Return a string representing the form data, including attached files."""
        # Build a list of lists, each containing "lines" of the
        # request.  Each part is separated by a boundary string.
        # Once the list is built, return a string where each
        # line is separated by '\r\n'.  
        parts = []
        part_boundary = '--' + self.boundary

        # Add the form fields
        parts.extend(
            [ part_boundary,
              'Content-Disposition: form-data; name="%s"' % name,
              '',
               value,
            ]
            for name, value in self.form_fields
            )

        # Add the files to upload
        parts.extend(
            [ part_boundary,
              'Content-Disposition: file; name="%s"; filename="%s"' % \
              (field_name, filename),
              'Content-Type: %s' % content_type,
              '',
              body,
            ]
            for field_name, filename, content_type, body in self.files
            )

        # Flatten the list and add closing boundary marker,
        # then return CR+LF separated data
        flattened = list(itertools.chain(*parts))
        flattened.append('--' + self.boundary + '--')
        flattened.append('')
        return '\r\n'.join(flattened)
