#!/usr/bin/env python

import sys
import os
import fnmatch
import argparse
import subprocess
import pysam
import functools
import re
from time import localtime, strftime

import qcutils
import jgi_mt
from jgi_mt import reporting as rep
import jat_utils

HEADER_COUNT = 1

def get_args():
    usage="%(prog)s analysis_project_id [options]"
    desc="Make a release report for a metatranscriptome alignment"
    parser = argparse.ArgumentParser(usage=usage,description=desc,formatter_class=argparse.RawTextHelpFormatter)
    
    rep.add_standard_release_args(parser)
    rep.add_project_info_arguments(parser)
    rep.add_reporting_arguments(parser)
    rep.add_jat_arguments(parser)

    if len(sys.argv) < 2:
        parser.print_help()
        sys.exit(0)
    
    return parser.parse_args()
#end get_args

args = get_args()

analysis = jgi_mt.get_analysis_info(args.analysis_project_id, verbose=args.verbose, seq_unit_name=args.sequnit,library_name=args.lib)

#get_filtered_fastq = functools.partial(jgi_mt.get_filtered_fastq,legacy=args.legacy)
preproc_fastqs = tuple(map(jgi_mt.get_rqc_filtered_fastq_name, analysis['seq_unit_name']))

# Get QC statistics #
qc_info = rep.get_qc_info(analysis,verbose=args.verbose)

# Get stats and alignment information # 
reference_name = None
mapping_dir = None
mapping_info = dict()
if 'img_dataset_id' in analysis:
    mapping_info['img_dataset_id'] = analysis['img_dataset_id']
    mapping_dir = jgi_mt.get_mapping_dir(analysis['img_dataset_id'], *preproc_fastqs) 
    reference_name = rep.get_taxon_name(analysis['img_dataset_id'])
else:
    mapping_info['source_analysis_project_id'] = analysis['source_analysis_project_id']
    mapping_dir = jgi_mt.get_mapping_dir(jgi_mt.get_assembly_fasta(*preproc_fastqs), *preproc_fastqs)
    reference_name = "Metatranscriptome Assembly"

if not os.path.isdir(mapping_dir):
    sys.stderr.write("Cannot find mapping directory %s\n" % mapping_dir)
    sys.exit(1)

if args.verbose:
    sys.stderr.write("Getting mapping_info from %s\n" % mapping_dir)
mapping_info.update(rep.get_mapping_info(mapping_dir,args.legacy))
aligner_parameters = ""
if 'aligner_parameters' in mapping_info:
    aligner_parameters = mapping_info['aligner_parameters']

# Build reports
report_builder = rep.ReportBuilder("Metatranscriptome Alignment")
if not 'img_dataset_id' in analysis:
    report_builder.add_additional_comment("This is a self-alignment i.e. metatranscriptome reads aligned to the assembly of these reads")
report_builder.add_project_section(analysis)
report_builder.add_qc_section(qc_info)
report_builder.add_alignment_section(reference_name, mapping_info)

# Build metadata.json
exporter = jat_utils.JamoSubmissionExporter()
exporter.send_email(args.email)
for dest in args.release_to:
    exporter.add_release_dest(dest)
rep.add_analysis_metadata(exporter, analysis)
shell_script = os.path.join(os.getcwd(),rep.get_shell_script(mapping_dir))
exporter.add_output("shell_script", shell_script)
exporter.add_output_metadata(shell_script, "file_format", "latex")

bam = mapping_info['bam']
exporter.add_output("metatranscriptome_alignment", bam)
exporter.add_output_metadata(bam, "num_aligned_reads", mapping_info['mapped_reads'])
exporter.add_output_metadata(bam, "num_input_reads", mapping_info['total_reads'])
exporter.add_output_metadata(bam, "aligner", mapping_info['aligner'])
exporter.add_output_metadata(bam, "aligner_version", mapping_info['aligner_version'])
exporter.add_output_metadata(bam, "aligner_parameters", aligner_parameters)
exporter.add_output_metadata(bam, "file_format", "bam")

cov=mapping_info['bam'] + ".cov"
if os.path.isfile(cov):
    exporter.add_output("metatranscriptome_assembly_coverage", cov)
    exporter.add_output_metadata(cov, "file_format", "coverage")

bai = mapping_info['bai']
exporter.add_output("metatranscriptome_alignment_index", bai)
exporter.add_output_metadata(bai, "file_format", "bai")

# Create text report #
report_file_names = rep.make_reports("%s/" % mapping_dir, report_builder, json_exporter=exporter)
if not report_file_names['pdf']:
    sys.stderr.write("Could not create PDF. Exiting.\n")
    sys.exit(1)

# Write metadata file for submission to JAT #
sys.stdout.write("%s\n" % mapping_dir)
rep.write_jat_metadata_file(exporter,mapping_dir,args.yaml)

if args.jat_template:
    template = args.jat_template
    try:
        jat_key = qcutils.release_to_jat_with_copy_verification(template, mapping_dir, verbose=args.verbose)
        sys.stdout.write("%s\n" % jat_key)
    except Exception as e:
        sys.stderr.write(e.message)
