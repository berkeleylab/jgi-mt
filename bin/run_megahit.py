#!/usr/bin/env python

import sys
import os
import subprocess
import bz2
import getpass
import math
import argparse

sys.path.append(os.path.dirname(os.path.realpath(__file__)) + "/../lib")
sys.path.append(os.path.dirname(os.path.realpath(__file__)) + "/..")

import gputils
import qcutils
import jgi_mt

########## HELPER FUNCTIONS ##########     



########## END HELPER FUNCTIONS ##########     

VERSION="1.0"

usage="usage: %(prog)s [options] <fastq1> <fastq2> ... <fastqN>"
desc="Assembles fastq data with MEGAHIT"
#version="%prog: "+VERSION
#formatter = IndentedHelpFormatter(width=120,max_help_position=50)
parser = qcutils.get_argparser(usage=usage, desc=desc)

parser.add_argument("fastq", nargs='+', type=os.path.abspath, help="fastqs to be assembled")
parser.add_argument("-l", "--ins_length", dest="insert", metavar="INT", type=int, default=300, help="the insert size of the library [300]\n")
parser.add_argument("-b", "--outbase", dest="outbase", metavar="FILE", help="the output base for naming output files. default is basename(fastq)")
parser.add_argument("-o", "--outdir", dest="outdir", metavar="DIR", help="the output directory to save files to. default is basename(fastq).mtAssembly")
parser.add_argument("-r", "--restart", dest="restart_dir", metavar="DIR", help="restart rnnotator using the given directory")
parser.add_argument("--local", dest="local", action="store_true", default=False, help="run rRNA removal locally. default is to submit to UGE")
parser.add_argument("--norun", dest="norun", action="store_true", default=False, help="do not run or submit job, just create script and exit")
parser.add_argument("--merge", dest="merge", action="store_true", default=False, help="merge reads before assembling")
parser.add_argument("--noclean", dest="noclean", action="store_true", default=False, help="do not delete temporary or intermediate files")
parser.add_argument("--combined", dest="combined", action="store_true", default=False, help="combined assembly for naming purposes")

parser.add_argument_group(gputils.get_UGE_arguments(parser,"megahit",7,"12:00:00",16))

if len(sys.argv) == 1:
    parser.print_help()
    sys.exit(0)

args = parser.parse_args()


# SET IMPORTANT RUNTIME VARIABLES

# create an output base if it has not been given
if not args.outbase:
    args.outbase = jgi_mt.get_megahit_base(*args.fastq)

if not args.outdir:
    args.outdir = jgi_mt.get_assembly_dir(*args.fastq)

shell_script=args.outdir+".sh"
log_file=args.outdir+".log"
writer = gputils.GPShellScriptBuilder(args)
writer.add_log_file(log_file)


modules = set()
modules.add('bbtools')
#modules.add('megahit')
modules.add('megahit/1.0.6')
writer.add_modules(*modules)

if args.merge:
    writer.add_env_var("BBMERGE", "bbmerge.sh")
writer.add_env_var("REFORMAT", "reformat.sh")
writer.add_env_var("MEGAHIT", "megahit")
writer.add_env_var("MAX_RD_LEN", "155")
writer.add_env_var("INPUT_FASTQ", " ".join(args.fastq))
writer.add_env_var("OUTBASE", args.outbase)
if args.combined:
    writer.add_env_var("FQ_BASE", args.outbase)
else:
    writer.add_env_var("FQ_BASE",jgi_mt.get_base(*args.fastq))
writer.add_env_var("MEGAHIT_INPUT", "$FQ_BASE.reads.fasta")
# Make the output directory
writer.add_outdir(args.outdir,args.tmpscratch)

input_reads = "$INPUT_FASTQ"
reads_to_reformat = ("$INPUT_FASTQ",)

#
# MERGING READS
#
if args.merge:
    writer.add_env_var("MEGAHIT_INPUT", "$FQ_BASE.reads.merged.fasta")
    merged = "$FQ_BASE.reads.merged.fastq.gz"
    unmerged = "$FQ_BASE.reads.unmerged.fastq.gz"
    
    log_msg = "Merging $INPUT_FASTQ" 
    cmd = "cat $INPUT_FASTQ | $BBMERGE in=stdin.fq.gz out=%s int=t outu=%s 2> merge.log" % (merged,unmerged)
    writer.add_command(cmd,log_msg)
    writer.add_command("MAX_RD_LEN=`grep Insert\ range merge.log | perl -pe 's/^Insert\ range:\W+\d+ - (\d+)/\\1/'`")
    reads_to_reformat = (merged,unmerged)

reformat_log = "Converting %s to Fasta for megahit" % (" ".join(reads_to_reformat))
reformat_cmd = "cat %s | $REFORMAT in=stdin.fq.gz out=$MEGAHIT_INPUT ow=true 2> reformat.log" % (" ".join(reads_to_reformat))
writer.add_command(reformat_cmd,reformat_log)

#log_msg = "Assembling $MEGAHIT_INPUT with megahit, using max_rd_len=$MAX_RD_LEN"
#megahit_cmd = "$MEGAHIT -r $MEGAHIT_INPUT -o $OUTBASE --cpu-only -m 100e9 --k-max 123 -l $MAX_RD_LEN > megahit.out 2> megahit.err"

log_msg = "Assembling $MEGAHIT_INPUT with megahit"
megahit_cmd = "$MEGAHIT --k-list 23,43,63,83,103,123 --continue -o $OUTBASE --12 $MEGAHIT_INPUT > megahit.out 2> megahit.err"

writer.add_command(megahit_cmd,log_msg)

shell_script=args.outdir+".sh"
sh = open(shell_script,"w")
writer.write_script(sh)
sh.close()
# DONE CREATING SCRIPT

if args.norun:
    sys.exit(0)

if args.local:
    sys.stdout.write("Running MEGAHIT assembly locally\n")
    cmd=[ "bash", shell_script  ]
    subprocess.Popen(cmd, stderr=subprocess.STDOUT, stdout=open(log_file,"w"))
else:
    sys.stdout.write("Submitting MEGAHIT assembly job to genepool\n")
    gputils.submit_to_genepool(shell_script, args.name, hold = args.hold)


