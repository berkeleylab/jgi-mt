#!/usr/bin/env python

import json
import urllib2
import sys
import argparse
import subprocess

import sdm_curl
import qcutils

def get_args():
    parser = argparse.ArgumentParser(usage="%(prog)s [options] its_key")
    parser.add_argument("its_key", type=str, help="the library name or sequencing project id")
    parser.add_argument("--jat_release", dest="jat_release", action="store_true", default=False, help="release project to JAT")
    parser.add_argument("--execute", action='store_true', default=False, help="execute release commands, do not just print to stdout")
    parser.add_argument("--lib", type=str, help="the library name for the data being released")
    parser.add_argument("--sequnit", type=str, default=None, help="the sequence unit used for this analysis")
    parser.add_argument("--bbtools", action="store_true", default=False, help="reads were filtered with BBTools. use RQC single-letter extension")
    group = parser.add_mutually_exclusive_group()
    group.add_argument("--standard", action='store_true', default=False, help="ignore alignments to IMG references. only report standard MT analysis releases")
    group.add_argument("--nonstandard", action='store_true', default=False, help="ignore standard MT analysis. only report alignments to IMG references.")
    group.add_argument("--force_all", action='store_true', default=False, help="print all release commands, regardless of whether an AP has been released")
    return parser.parse_args()


if __name__ == "__main__":
    
    args = get_args()
    
    spid = qcutils.get_proj_info(args.its_key)[0][0]
    comment = None
    release_cmd = None 
    for ap in qcutils.get_analysis_projects(spid):
        if args.force_all or qcutils.is_analysis_project_unreleased(ap['analysis_project_id']):
            if ap['analysis_product_id'] == 40:
                continue
            if ap['img_dataset_id'] or ap['source_analysis_project_id']:
                if args.standard:
                     if ap['img_dataset_id']:
                         continue
                elif args.nonstandard:
                    if ap['source_analysis_project_id']:
                        continue
                comment = "# Align reference: %s" % (str(ap['img_dataset_id']) if ap['img_dataset_id'] else "Self")
                release_cmd = "release_mt_alignment.py %s" % (ap['analysis_project_id'])
            else:
                if args.nonstandard:     
                    continue
                comment = "# Assembly"
                release_cmd = "release_mt_assembly.py %s" % (ap['analysis_project_id'])
            if args.jat_release:
                release_cmd += " --jat_release"
            if args.sequnit:
                release_cmd += " --sequnit %s" % args.sequnit
            if args.lib:
                release_cmd += " --lib %s" % args.lib
            if args.bbtools:
                release_cmd += " --bbtools"

            if args.execute:
                sys.stdout.write("%s\n" % comment)
                subprocess.call(release_cmd, shell=True)
            else:
                sys.stdout.write("%s\n%s\n" % (comment,release_cmd))
    

