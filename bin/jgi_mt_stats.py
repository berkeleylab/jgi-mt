#!/usr/bin/env python

from __future__ import division
import argparse
import sys
import os
import json
import re
import time
import urllib2
import subprocess
import glob
import collections
import copy

#genepool and denovo
RQC_REPORT = "/global/dna/projectdirs/PI/rqc/prod/jgi-rqc-pipeline/report/rqc_report.py"
RQC_JAT_CHECK = "/global/dna/projectdirs/PI/rqc/prod/jgi-rqc-pipeline/report/rqc_report.py"

if 'NERSC_HOST' in os.environ.keys():
    if os.environ['NERSC_HOST'].lower()=='genepool':
        cmd = 'module show jgi-mga/2.0 2>&1 |  grep JGI_MGA_DIR'
        JGI_MGA_DIR = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE).stdout.read().rstrip()
        JGI_MGA_DIR = re.sub(r'^.*\s(\/.*)$',r'\1',JGI_MGA_DIR) + '/lib'
        sys.path.insert(0,JGI_MGA_DIR)
    elif os.environ['NERSC_HOST'].lower()=='denovo':
        JGI_MGA_DIR = '/global/projectb/sandbox/gaag/bfoster/2.0/jgi-mga/lib'
        sys.path.insert(0,JGI_MGA_DIR)
    else:
        print "Nersc host: " + os.environ['NERSC_HOST'] + " not recognized\n"
        
import jgi_mga_utils

VERSION = "1.0.0"

'''
jgi_mt_stats.py 
given a run directory, create:

   metadata.json for jat import 
   rqc-stats.txt key value pairs file for rqc import
   rqc-stats.pdf pdf file with read qc and assembly stats
   sigs tables:
      Table_3_library_information.txt
      Table_4_sequence_processing.txt
      Table_5_metagenome_statistics.txt

depends on the following files:


'''

def main():
    '''main'''
    parser = argparse.ArgumentParser(description='Description.')
    parser.add_argument('-v', '--version', action='version', version="%(prog)s (version " + VERSION + ")")
    parser.add_argument("-i", "--inputdir", required=True, help="input directory.\n")
    parser.add_argument("-l", "--legacy_templates", required=False, default=False, action='store_true', help="to specify creating 2 AT metadata.jsons.\n")
    parser.add_argument("-n", "--no_rqc_pdf", required=False, default=False, action='store_true', help="to skip rqc_pdf.\n")
    parser.add_argument("-d", "--debug", required=False, default=False, action='store_true', help="debug.\n")
    args = parser.parse_args()

    args.inputdir = os.path.realpath(args.inputdir)
    dir = args.inputdir
    if os.path.exists(args.inputdir) == 0:
        sys.exit()

    #gather files
    files = dict()
    files['setup_file'] = jgi_mga_utils.get_glob(os.path.join(dir,'setup/setup.json'))
    files['assembly_contigs'] = jgi_mga_utils.get_glob(os.path.join(dir,'*/assembly.contigs.fasta'))
    files['assembly_cmd'] = jgi_mga_utils.get_glob(os.path.join(dir,'assy/command.bash'))
    files['assembly_stats'] = jgi_mga_utils.get_glob(os.path.join(dir,'*/assembly.scaffolds.fasta.stats.tsv'))
    files['alignment'] = jgi_mga_utils.get_glob(os.path.join(dir,'*/pair*sorted*bam'))
    files['alignment_cmd'] = jgi_mga_utils.get_glob(os.path.join(dir,'readMapp*/command.bash'))
    files['alignment_idx'] = jgi_mga_utils.get_glob(os.path.join(dir,'*/pair*sorted*bam.bai'))
    files['alignment_cov'] = jgi_mga_utils.get_glob(os.path.join(dir,'*/cov*txt'))
    files['shell_file'] =  jgi_mga_utils.get_glob(os.path.join(dir,'*/reproduce.bash'))
    files['readme_txt'] =  jgi_mga_utils.get_glob(os.path.join(dir,'*/README.txt'))
    files['readme_pdf'] =  jgi_mga_utils.get_glob(os.path.join(dir,'*/README.pdf'))

    if "" in files.values():
        print json.dumps(files,indent=3)
        sys.exit()
    elif args.debug:
        print json.dumps(files,indent=3)

    

    # output metadata.json
    metadata = create_metadata_json(args.inputdir,files,args)
    if args.legacy_templates:
        metadata_assy, metadata_aln = split_metadata_json(metadata,files['setup_file'])
        with open(args.inputdir + "/" + str(metadata_assy['metadata']['analysis_task_id'][0]) + ".assy.metadata.json", "w") as f:        
            f.write(json.dumps(metadata_assy,indent = 3))
        with open(args.inputdir + "/" + str(metadata_aln['metadata']['analysis_task_id'][0]) + ".aln.metadata.json", "w") as f:        
            f.write(json.dumps(metadata_aln,indent = 3))
    else:
        with open(args.inputdir + "/" + str(metadata['metadata']['analysis_task_id'][0]) + ".metadata.json", "w") as f:        
            f.write(json.dumps(metadata,indent = 3))

    #get filtered file for stats, raw file for stats and metadata.json
    assembly_fasta = get_output_file(metadata, "metatranscriptome_assembly", 0)
    if len(metadata['inputs']) !=1 or len(metadata['metadata']['seq_unit_name']) !=1:
        sys.exit("Not ready for multiple inputs\n" + json.dumps(metadata['inputs'],indent=3))

    filtered_file = os.path.basename(metadata['inputs'][0])
    raw_file = metadata['metadata']['seq_unit_name'][0]
    
    kvp = dict()
    kvp["stats.date"] = time.strftime("%Y%m%d")
#    kvp["pipeline_version"] = subprocess.Popen('jgi_mga_meta_rqc.py --version 2>&1', stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True).stdout.read().rstrip()


    # file size > 0
#    kvp["sanity_check.file_size_check"] = file_size_check(metadata, 1000)
    kvp["sanity_check.release_spid_in_jamo"] = ",".join([str(spid) for spid in release_spid_in_jamo(metadata)])
    kvp["sanity_check.release_ap_in_jamo"] = ",".join([str(apid) for apid in release_ap_in_jamo(metadata)])
    kvp["sanity_check.release_ap_to_taxonoid"] = its_ap_to_taxonoid(metadata)


    #add all output metadata
    for output in metadata["outputs"]:
        if "metadata" in output.keys():
            for key in output["metadata"].keys():
                kvp["outputs." + output["label"] + "." + key] = output["metadata"][key]


    #add SIGS table to kvp
    kvp.update(get_sigs_table(metadata, filtered_file, raw_file,  kvp, files))
 
   #write stats
    rqc_stats = args.inputdir + "/rqc-stats.txt"
    with open(rqc_stats, "w") as writefile:
        for key in sorted(kvp.keys()):
            string_ = str()
            try:
                string_ = str(kvp[key])
            except:
                string_ = kvp[key].encode('utf-8')
            writefile.write(key + "=" + string_ + "\n")


    files = dict()
    reportfinal = get_output_file(metadata, "readme", 0)
    reportdir = os.path.dirname(reportfinal)

    files[os.path.basename(reportfinal)] = reportfinal
    files[os.path.basename(os.path.splitext(reportfinal)[0] + ".pdf")] = os.path.splitext(reportfinal)[0] + ".pdf"


    if args.no_rqc_pdf == False:
        rqc_stats_pdf = call_rqc_report(metadata, args.inputdir, "rqc-stats.pdf", rqc_stats, debug=True)
    # modify metadata.json to add qc_report
     ############create SIG table text file
    sigs= hasher()
    sigs = create_sig_subset(rqc_stats)
    sigs['3']['filename'] = 'Table_3_library_information.txt'
    sigs['4']['filename'] = 'Table_4_sequence_processing.txt'
    sigs['5']['filename'] = 'Table_5_metagenome_statistics.txt'

    for table_id in sigs.keys():
        with open(args.inputdir + "/" + sigs[table_id]['filename'], "w") as writefile:
            for line in sigs[table_id]['data']:
                writefile.write(line)

    return

def split_metadata_json(md,setup_file):
    metadata_assy=copy.deepcopy(md)
    metadata_aln=copy.deepcopy(md)

    print ">assy"
    #remove alignment related outputs from assy metadata
    aln_labels = ['metatranscriptome_alignment', 'metatranscriptome_alignment_index','metatranscriptome_assembly_coverage','graphical_report']
    tmp_outputs = list()
    for i in range(0,len(metadata_assy['outputs'])):
        if metadata_assy['outputs'][i]['label'] not in aln_labels:
            print metadata_assy['outputs'][i]['label']
            #move text_report to readme to match old template
            if metadata_assy['outputs'][i]['label']=="text_report":
                metadata_assy['outputs'][i]['label']="readme"

            tmp_outputs.append(metadata_assy['outputs'][i])
    metadata_assy['outputs'] = tmp_outputs

    print ">aln"
    #remove assembly related outputs from aln metadata
    assy_labels = ['metatranscriptome_assembly','rqc_graphical_report','sigs_3_report','sigs_4_report','sigs_5_report']
    tmp_outputs = list()
    for i in range(0,len(metadata_aln['outputs'])):
        if metadata_aln['outputs'][i]['label'] not in assy_labels:
            print metadata_aln['outputs'][i]['label']
            #move text_report to readme to match old template
            if metadata_aln['outputs'][i]['label']=="text_report":
                metadata_aln['outputs'][i]['label']="readme"
            tmp_outputs.append(metadata_aln['outputs'][i])
    metadata_aln['outputs'] = tmp_outputs

    #adjust ap/at for alignment
    with open(setup_file, "r") as f:
        setup_metadata=json.loads(f.read())
    assembly_ap = setup_metadata['metadata']['analysis_project_id'][0]
    all_ap = jgi_mga_utils.web_services("ap_byspid", int(setup_metadata['metadata']['sequencing_project_id'][0]))
    for ap in all_ap:
        if ap['uss_analysis_project']['source_analysis_project_id']==assembly_ap and ap['uss_analysis_project']['analysis_product_id'] in [38,62]:#mapping to self
            analysis_project = ap['uss_analysis_project']['analysis_project_id']
            source_analysis_project = ap['uss_analysis_project']['source_analysis_project_id']
                   
            if assembly_ap == source_analysis_project:
                for task in ap['uss_analysis_project']['analysis_tasks']:
                    if task['current_status_id'] not in [9,10,12,13,99]: #"RNA Assembly and Mapping"
                        #- "complete", 9
                        #- "abandoned", 10
                        #- "on hold", 12
                        #- "deleted", 13
                        #- "rework", 99
                        if task['analysis_task_type_id']in[57]:#mapping to self
                            metadata_aln['metadata']['analysis_project_id']=[analysis_project]
                            metadata_aln['metadata']['source_analysis_project_id']=[assembly_ap]
                            metadata_aln['metadata']['analysis_task_id']=[task['analysis_task_id']]



    return metadata_assy,metadata_aln

def create_metadata_json(dir,file_paths,args):
    '''
    outputs:
       metatranscriptome_assembly
       metatranscriptome_alignment
       metatranscriptome_alignment_index
       metatranscriptome_assembly_coverage
       shell_script
       rqc_graphical_report
       sigs_3_report
       sigs_4_report
       sigs_5_report
       text_report
       graphical_report

    inputs:
    file_paths['setup_file'] = jgi_mga_utils.get_glob(os.path.join(dir,'setup/setup.json'))
    file_paths['assembly_contigs'] = jgi_mga_utils.get_glob(os.path.join(dir,'*/assembly.contigs.fasta'))
    file_paths['assembly_stats'] = jgi_mga_utils.get_glob(os.path.join(dir,'*/assembly.scaffolds.fasta.stats.tsv'))
    file_paths['alignment'] = jgi_mga_utils.get_glob(os.path.join(dir,'*/pair*sorted*bam'))
    file_paths['alignment_idx'] = jgi_mga_utils.get_glob(os.path.join(dir,'*/pair*sorted*bam.bai'))
    file_paths['alignment_cov'] = jgi_mga_utils.get_glob(os.path.join(dir,'*/cov*txt'))
    file_paths['shell_file'] =  jgi_mga_utils.get_glob(os.path.join(dir,'*/reproduce.bash'))
    file_paths['readme_txt'] =  jgi_mga_utils.get_glob(os.path.join(dir,'*/README.txt'))
    file_paths['readme_pdf'] =  jgi_mga_utils.get_glob(os.path.join(dir,'*/README.pdf'))
    '''   
    files = hasher()
    files['metatranscriptome_assembly']['file'] = file_paths['assembly_contigs']
    files['metatranscriptome_assembly']['final_basename'] = 'final.contigs.fasta'
    files['metatranscriptome_assembly']['metadata']['file_format'] = 'fasta'

    files['metatranscriptome_alignment']['file'] = file_paths['alignment']
    files['metatranscriptome_alignment']['final_basename'] = 'reads.v.final.contigs.sorted.bam'
    files['metatranscriptome_alignment']['metadata']['file_format'] = 'bam'

    files['metatranscriptome_alignment_index']['file'] = file_paths['alignment_idx']
    files['metatranscriptome_alignment_index']['final_basename'] = 'reads.v.final.contigs.sorted.bam.bai'
    files['metatranscriptome_alignment_index']['metadata']['file_format'] = 'bai'

    files['metatranscriptome_assembly_coverage']['file'] = file_paths['alignment_cov']
    files['metatranscriptome_assembly_coverage']['final_basename'] = 'reads.v.final.contigs.sorted.bam.cov'
    files['metatranscriptome_assembly_coverage']['metadata']['file_format'] = 'cov'

    files['shell_script']['file'] = file_paths['shell_file']
    files['shell_script']['metadata']['file_format'] = 'sh'

    files['text_report']['file'] = file_paths['readme_txt']
    files['text_report']['metadata']['file_format'] = 'text'

    files['graphical_report']['file'] = file_paths['readme_pdf']
    files['graphical_report']['metadata']['file_format'] = 'pdf'
    
    
    #not yet created
    if args.no_rqc_pdf == False:
        files['rqc_graphical_report']['file']= 'rqc-stats.pdf'
        files['rqc_graphical_report']['metadata']['file_format'] = 'pdf'
    files['sigs_3_report']['file'] = 'Table_3_library_information.txt'
    files['sigs_3_report']['metadata']['file_format'] = 'text'
    files['sigs_4_report']['file'] = 'Table_4_sequence_processing.txt'
    files['sigs_4_report']['metadata']['file_format'] = 'text'
    files['sigs_5_report']['file'] = 'Table_5_metagenome_statistics.txt'
    files['sigs_5_report']['metadata']['file_format'] = 'text'


    #get final_list of files and link if needed
    for file_ in files:
        if not files[file_]['file'] : #file does not exist
#            files[file_]['file'] = 'NA'
            continue
        if files[file_]['final_basename']: #link friendlier name if exists
            cmd = 'cd ' + os.path.dirname(files[file_]['file']) + ' && ln -sf ' + os.path.basename(files[file_]['file']) + " ./" + files[file_]['final_basename']
            out = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE).stdout.readlines()
            files[file_]['source']= files[file_]['file']
            files[file_]['file']= re.sub(os.path.basename(files[file_]['file']) + '$',  files[file_]['final_basename'],files[file_]['file'])

        files[file_]['file'] = re.sub(dir.rstrip("/") + "/",r'',files[file_]['file']) #strip dir to make the file path relative

    #build output section
    outputs = list()
    for file_ in files.keys():
        tmp = hasher()
        if not files[file_]['file']:
            continue
        if "metadata" in files[file_].keys():
            tmp['metadata'].update(files[file_]['metadata'])

        if file_ == 'metatranscriptome_assembly':
            statsfile = file_paths['assembly_stats']
            stats = subprocess.Popen('cat ' + statsfile, shell=True, stdout=subprocess.PIPE).stdout.readlines()
            if len(stats)!=2:
                sys.exit("No valid stats file ")
            headers=re.sub(r'^#',r'',stats[0]).split()
            data=stats[1].split()
            if len(headers) != len(data):
                sys.exit("Not a valid stats file " + statsfile)
            tmp['metadata'].update(dict(zip(headers,[float(i) for i in data])))
            tmp['metadata'].update(detect_assembler(file_paths['assembly_cmd']))
        elif file_ == "metatranscriptome_alignment":
            tmp['metadata']=detect_aligner(file_paths['alignment_cmd'])
        tmp['label'] = file_
        tmp['file'] = files[file_]['file']
        outputs.append(tmp)
        
    #finalize
    metadata=hasher()
    metadata['send_email'] = False
    metadata['release_to'] = ['portal','img']
    metadata['email'] = dict()
    with open(file_paths['setup_file'], "r") as f:
        setup_metadata=json.loads(f.read())
    metadata.update(setup_metadata)
    metadata['outputs'] = outputs
    return metadata 

def detect_aligner(bash_file):
    '''
    detects aligner based on command file and returns 
    {'assembler':'','file_format':'','assembler_parameters':'','assembler_version':''}
    TODO: flesh out other aligners
    '''
    tmp = {'aligner':'', 'file_format':'', 'aligner_parameters':'', 'aligner_version':'', 'num_input_reads':0, 'num_aligned_reads':0}
    command = ""
    with open(bash_file, "r") as f:
        for line in f.readlines():
            if line.find("bbmap") != -1:
                command = command + ";" + line.rstrip()
        command = command.lstrip(';')
    with open(os.path.dirname(bash_file) + '/command.bash.e') as f:        
        num_aligned_reads = 0
        for line in f.readlines():
            if line.find("ersion") !=-1:
                if re.match('^.*ersion\s(\d+\.*\d*)\s.*$',line):
                    version = re.sub(r'^.*ersion\s(\d+\.*\d*)\s.*$',r'\1',line.rstrip())
            if line.startswith("Reads Used"):
                num_input_reads = float(re.sub(r'^.*:\s+(\d+)\s+\(.*$',r'\1',line).rstrip())
            if line.startswith("mapped"):
                num_aligned_reads += float(re.sub(r'^.*?%\s+(\d+)\s+.*$',r'\1',line))

    tmp['aligner'] = "bbmap"
    tmp['file_format'] = "bam"
    tmp['aligner_parameters']  = command
    tmp['aligner_version'] = version
    tmp['num_input_reads']  = num_input_reads
    tmp['num_aligned_reads'] = num_aligned_reads
    return tmp

def detect_assembler(bash_file):
    '''
    detects assembler based on command file and returns 
    {'assembler':'','file_format':'','assembler_parameters':'','assembler_version':''}
    TODO: flesh out spades
    '''
    megahit = False
    spades = False
    tmp = {'assembler':'','file_format':'','assembler_parameters':'','assembler_version':''}
    with open(bash_file, "r") as f:
        for line in f.readlines():
            if line.find("megahit") != -1:
                megahit = True
            if line.find("spades") != -1:
                spades = True
    if spades:
        pass
    elif megahit:
        command = ""
        with open(bash_file, "r") as f:
            for line in f.readlines():
                if line.find("megahit") != -1:
                    command = command + ";" + line.rstrip()
        command = command.lstrip(';')
        command = re.sub(r'--12\s\S+',r'',command)
        log = glob.glob(os.path.join(os.path.dirname(bash_file),'*/log'))
        with open(log[0], "r") as f:
            version = f.readlines()[0].rstrip()
        tmp['assembler'] = "megahit"
        tmp['file_format'] = "fasta"
        tmp['assembler_parameters']  = command
        tmp['assembler_version'] = version
    return tmp
        
def create_sig_subset(rqc_stats_file):
    return_dict = hasher()
    sig_lines = []
    with open(rqc_stats_file, "r") as readfile:
        for line in readfile.readlines():
            if line.find("SIG") != -1:
                sig_string = re.sub(r'^.*SIG\.(\d).*$',r'\1',line.rstrip())
                if sig_string not in return_dict:
                    return_dict[sig_string]['data'] = list()
                return_dict[sig_string]['data'].append(line)
    return return_dict


def call_rqc_report(metadata, dir, name, rqcstats, debug):
    #module load jgi-rqc;$JGI_RQC_DIR/report/rqc_report.py  -t mt-summary.tex -i BZWCA -of rqc-stats.pdf -f ../../rqc-stats.txt -o $PWD ... works
    #module load jgi-rqc;$JGI_RQC_DIR/report/rqc_report.py  -t mg-summary.tex -i BZWCA -f ../rqc-stats.txt,../rqc-files.txt -of rqc-stats.pdf
    filename = dir + "/" + name
    cmd = RQC_REPORT + " -t mt-summary.tex "
    cmd += " -i " + metadata['metadata']['library_name'][0]
    cmd += " -of " + filename
    cmd += " -f " + rqcstats
    cmd += " -o " + dir

    if debug:
        print cmd
    proc = subprocess.Popen(cmd, shell=True)
    (output, err) = proc.communicate()
    p_status = proc.wait()
    if debug:
        if output != None:
            sys.stdout.write("OUT:\n" + output)
        if err != None:
            sys.stdout.write("ERR:\n" + err)

    if p_status == 0:
        pass
    else:
        sys.exit(cmd + " not successful\n")
    return filename 


def get_sigs_table(metadata, filtered_file, raw_file, kvp, file_paths):
    '''
    Given metadata, and key value pairs,  create sigs table for rqc stats.
    args: metadata data structure
    returns: modified kvp with "assy.SIGS" table added"

    Table 3. Library information.
    Sample Label(s)
    Sample prep method
    Library prep method(s)
    Sequencing platform(s)
    Sequencing chemistry
    Sequence size (GBp)
    Number of reads
    Single-read or paired-end sequencing
    Sequencing library insert size
    Average read length
    Standard deviation for read length

    Table 4. Sequence processing
    Tool(s) used for quality control
    Number of sequencs removed by quality control procedures
    Number of sequences that passed quality control procedures
    Number of artificial duplicate reads

    Table 5. Metagenome statistics
    Libraries Used
    Assembly tool(s) used
    Number of contigs after assembly
    Number of singletons after assembly
    minimal contig length
    Total bases assembled
    Contig n50
    % of Sequences assembled
    Measure for % assembled (either from assembly output or via mapping of reads to contigs; in the latter case provide tool and parameters used)

    
    '''
    
    filt = jgi_mga_utils.web_services('jamo', {'file_name':filtered_file})
    raw = jgi_mga_utils.web_services('jamo', {'file_name':raw_file})
    if len(raw) != 1 or len(filt) !=1:
        sys.exit("raw or filtered !=1 see " + raw_file + ", or " + filtered_file)
    raw = raw[0]
    filt = filt[0]

    #SIG table 3
    kvp["assy.SIG.3.Sequence_size_GBp"] = raw['metadata']['rqc']['read_qc']['base_count']/1000000000
    kvp["assy.SIG.3.Sequence_size_GBp"] = "{:.1f}".format(kvp["assy.SIG.3.Sequence_size_GBp"])
    kvp["assy.SIG.3.Number_of_reads"] = raw['metadata']['rqc']['read_qc']['read_count']
    kvp["assy.SIG.3.Average_read_length"] = "{:.1f}".format((raw['metadata']['rqc']['read_qc']['read1_length'] + raw['metadata']['rqc']['read_qc']['read2_length'])/2)
    kvp["assy.SIG.3.Standard_deviation_for_read_length"] = raw['metadata']['rqc']['read_qc']['illumina_read_insert_size_std_insert']
    #get Jamo info from library 

    kvp["assy.SIG.3.Sample_Labels"] = raw['metadata']['sow_segment']['sample_name']
    kvp["assy.SIG.3.Sample_prep_method"]=""
    kvp["assy.SIG.3.Library_prep_methods"] = raw['metadata']['sow_segment']['actual_library_creation_queue']
    kvp["assy.SIG.3.Sequencing_platforms"] = raw['metadata']['physical_run']['platform_name']
    if kvp["assy.SIG.3.Sequencing_platforms"].find("llumina") != -1:
        kvp["assy.SIG.3.Sequencing_chemistry"] = "SBS"
    else:
        kvp["assy.SIG.3.Sequencing_chemistry"]=""

    kvp["assy.SIG.3.Single-read_or_paired-end_sequencing"] = raw['metadata']['physical_run']['physical_run_type']
    kvp["assy.SIG.3.Sequencing_library_insert_size"] = str(raw['metadata']['rqc']['read_qc']['illumina_read_insert_size_avg_insert'])
            
    #SIG table 4
    kvp["assy.SIG.4.Tools_used_for_quality_control"] = 'rqc_filter.sh pipeline version ' + filt['metadata']['filter_pipeline_version'] + " from bbtools version " + filt['metadata']['bbtools_version']
    kvp["assy.SIG.4.Number_of_sequences_that_passed_quality_control_procedures"] = int(filt['metadata']['filter_reads_count'])
    kvp["assy.SIG.4.Number_of_sequences_removed_by_quality_control_procedures"] = int(kvp["assy.SIG.3.Number_of_reads"]) - int(filt['metadata']['filter_reads_count']) 
    kvp["assy.SIG.4.Number_of_artificial_duplicate_reads"] = "NA"

    #SIG table 5
    kvp["assy.SIG.5.Libraries_Used"] = raw['metadata']["sow_segment"]["library_name"]
    kvp["assy.SIG.5.Assembly_tools_used"] = kvp["outputs.metatranscriptome_assembly.assembler"] + "_" + kvp["outputs.metatranscriptome_assembly.assembler_version"]
    kvp["assy.SIG.5.Number_of_contigs_after_assembly"] = kvp["outputs.metatranscriptome_assembly.n_contigs"]
    kvp["assy.SIG.5.Number_of_singletons_after_assembly"] = ""
    kvp["assy.SIG.5.minimal_contig_length"] = 200
    kvp["assy.SIG.5.Total_bases_assembled"] = kvp["outputs.metatranscriptome_assembly.contig_bp"]
    kvp["assy.SIG.5.Contig_n50"] = kvp["outputs.metatranscriptome_assembly.ctg_N50"]

    covstats = file_paths['alignment_cov']

    total_reads_input_to_aligner = kvp["assy.SIG.4.Number_of_sequences_that_passed_quality_control_procedures"]
    sum_aligned = 0
    sum_aligned_gt10k = 0
    with open(covstats, "r") as readfile:
        for line in readfile:
            if line.startswith("#"):
                continue
            fields = line.split("\t")
            sum_aligned += int(fields[6]) + int(fields[7])
            if int(fields[2]) > 10000:
                sum_aligned_gt10k += int(fields[6]) + int(fields[7])
    perc_aligned = "{:.1f}".format(float(sum_aligned/float(total_reads_input_to_aligner)*100))
    perc_gt10k_aligned = "{:.1f}".format(float(sum_aligned_gt10k/float(total_reads_input_to_aligner)*100))
    kvp["assy.SIG.5.percent_of_Sequences_assembled"] = perc_aligned
    kvp["assy.SIG.5.percent_of_Sequences_assembled_gt10k"] = perc_gt10k_aligned
    kvp["assy.SIG.5.Measure_for_percent_assembled"] = "\"reads used as input to the assembler were mapped back to the assembled reference using aligner " + kvp["outputs.metatranscriptome_alignment.aligner"] + " version " + kvp["outputs.metatranscriptome_alignment.aligner_version"] + " with options ambiguous=random.\""
    kvp["assy.SIG.5.Number_of_singletons_after_assembly"] =  total_reads_input_to_aligner - sum_aligned
    return kvp

def get_output_file(metadata, key, index=0):
    '''
    Given metadata key and index, return all the output files
    args: metadata.json data structure
          label of the output
    returns:
          output file path
    '''
    output_file = ""
    for output in metadata["outputs"]:
        if output["label"] == key:
            output_file = output["file"]
            output_file = output_file.split(":")[index]
    return output_file

def its_ap_to_taxonoid(metadata):
    '''
    args: metadata.json data strucure
    returns:taxonoid for apid
    '''
    oid = ""
    apid = ""
    if "analysis_project_id" in metadata["metadata"].keys():
        if len(metadata["metadata"]["analysis_project_id"]) == 1:
            apid = metadata["metadata"]["analysis_project_id"][0]

    #url = "https://gold.jgi-psf.org/rest/analysis_project/" + str(apid)
    url = "https://gold.jgi.doe.gov/rest/analysis_project/" + str(apid)
    #authKey=base64.b64encode(username + ':' + password)
    auth_key = "amdpX2dhZzozSzgqZiV6"
    headers = {"Content-Type":"application/json", "Authorization":"Basic " + auth_key}
    request = urllib2.Request(url)
    data = dict()
    for key, value in headers.items():
        request.add_header(key, value)
    try:
        response = urllib2.urlopen(request)
        data = json.loads(response.read())
    except:
        pass
    oid = int()
    if "imgTaxonOid" in data.keys():
        oid = data["imgTaxonOid"]
    return oid

def file_size_check(metadata, min_size):
    '''takes metadata data structure and returns umapped merged count'''
    metadata_outputs = dict()
    all_greater_than_min = True
    for filename in metadata["outputs"]:
        fileloc = filename["file"].split(":")[0]
        metadata_outputs[filename["file"]] = os.stat(os.path.abspath(fileloc)).st_size
        if metadata_outputs[filename["file"]] < min_size:
            all_greater_than_min = False
    return all_greater_than_min


def release_spid_in_jamo(metadata):
    '''takes metadata data structure and returns whether spid(s) is alread in jamo or not'''
    released = []
    #get mapping readcount from metadata.json
    spids = []
    spids = metadata['metadata']['sequencing_project_id']
    for spid in spids:
        result = jgi_mga_utils.web_services("jamo", {"metadata.sequencing_project_id":int(spid), "file_type":"scaffolds"})
        jamo_rel_file_count = len(result)
        if jamo_rel_file_count > 0:
            released.append(spid)
    return released

def release_ap_in_jamo(metadata):
    '''takes metadata data structure and returns whether apid is already in jamo or not'''
    released = []
    #get mapping readcount from metadata.json
    apids = []
    apids = metadata['metadata']['analysis_project_id']
    for apid in apids:
        result = jgi_mga_utils.web_services("jamo", {"metadata.analysis_project_id":int(apid), "file_type":"scaffolds"})
        jamo_rel_file_count = len(result)
        if jamo_rel_file_count > 0:
            released.append(apid)
    return released


def has_missing_keys(dict_a, dict_b):
    '''compare two sets and return if they are equal len'''
    has_missing = False
#    if set(dict_a.keys()) == set(dict_b.keys()):
#        has_missing = True
    if len(set(dict_a.keys()) - set(dict_b.keys())) != 0 or  len(set(dict_b.keys()) - set(dict_a.keys())) != 0:
        has_missing = True
    return has_missing

def hasher():
    return collections.defaultdict(hasher)

                            
if __name__ == "__main__":
    main()




#__________Unused________________
def fasta_agp_cov_consistent(agp, fasta, cov):
    '''
    Given agp, fasta and coverage files,
    return whether they all have the same count
    '''
    contig_count_success = True
    #agp
    agp_contigs = dict()
    agp_scaffolds = dict()
    with open(agp, "r") as readfile:
        for line in readfile:
            if line.startswith("#"):
                pass
            else:
                fields = line.split()
                try:
                    val = int(fields[-2])
                except ValueError:
                    pass
                if fields[5] >=200:
                    agp_contigs[fields[5]] = 1
                agp_scaffolds[fields[0]] = 1

    #fasta
    fasta_scaffolds = dict()
    with open(fasta, "r") as readfile:
        for line in readfile:
            if line.startswith(">"):
                header = re.sub('^>', '', line)
                header = header.rstrip("\n")
                fasta_scaffolds[header] = 1

    cov_scaffolds = dict()
    with open(cov, "r") as readfile:
        for line in readfile:
            if line.startswith("ID"):
                pass
            elif line.startswith("#"):
                pass
#                print "# in header"
            else:
                fields = line.split()
                scaff = re.sub('_c\d+$', '', fields[0])
                cov_scaffolds[scaff] = 1

    if 0:#debug
        with open("agp.txt",'w') as writefile:
            writefile.write("\n".join(agp_scaffolds) + "\n")
        with open("scafffasta.txt",'w') as writefile:
            writefile.write("\n".join(fasta_scaffolds) + "\n")
        with open("cov.txt",'w') as writefile:
            writefile.write("\n".join(cov_scaffolds) + "\n")
            
    if has_missing_keys(agp_scaffolds, fasta_scaffolds):
        contig_count_success = False
        sys.stdout.write("agp, fasta don't match\n")
    elif has_missing_keys(agp_scaffolds, cov_scaffolds):
        contig_count_success = False
        sys.stdout.write("agp, cov don't match\n")
    return contig_count_success
def metadata_config_readme_inputs_consistent(metadata, readme):
    '''takes metadata data structure and read me and ensures that inputs are consistant'''
    metadata_inputs = dict()
    input_count_success = True
    for filename in metadata["inputs"]:
        metadata_inputs[filename] = 1

    readme_inputs = dict()
    with open(readme, "r") as readfile:
        for line in readfile:
            if line.startswith("Filtered"):
                header = re.sub(r'^FilteredData:\s', '', line)
                header = header.rstrip("\n")
                readme_inputs[header] = 1

    if has_missing_keys(metadata_inputs, readme_inputs):
        input_count_success = False
    return input_count_success

def metadata_json_check(metadata_file):
    '''
    Given metadata.json file, run rqc jat sanity check
    args: metadata.json file
    returns: true if pass false if fail or None if not on genepool
    '''
    return_val = None
    cmd = RQC_JAT_CHECK + " -f " + metadata_file
    sub_p = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True)
    (output, err) = sub_p.communicate()
    p_status = sub_p.wait()
    if p_status == 0:
        return_val = True
    else:
        return_val = False
    return return_val



