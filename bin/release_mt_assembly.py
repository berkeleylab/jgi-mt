#!/usr/bin/env python

import sys
import os
import argparse
import urllib2
import json
import glob
import subprocess
import functools
from time import localtime, strftime

import qcutils
import jgi_mt
from jgi_mt import reporting as rep
import jat_utils
import jgidb

HEADER_COUNT = 1

def get_args():
    usage="%(prog)s analysis_project_id [options]"
    desc="Make a release report for a metatranscriptome assembly"
    parser = argparse.ArgumentParser(usage=usage,description=desc,formatter_class=argparse.RawTextHelpFormatter)
    
    rep.add_standard_release_args(parser)
    rep.add_project_info_arguments(parser)
    rep.add_reporting_arguments(parser)
    rep.add_jat_arguments(parser)

    if len(sys.argv) < 2:
        parser.print_help()
        sys.exit(0)
    
    return parser.parse_args()
#end get_args

#### END FUNCTIONS ####

args = get_args()

analysis = jgi_mt.get_analysis_info(args.analysis_project_id, verbose=args.verbose, seq_unit_name=args.sequnit, library_name=args.lib)

preproc_fastq = jgi_mt.get_rqc_filtered_fastq_name(analysis['seq_unit_name'][0])
assembly_dir = jgi_mt.get_assembly_dir(preproc_fastq)
assembly_fasta = os.path.join(os.getcwd(),assembly_dir,jgi_mt.get_assembly_fasta(preproc_fastq))
assembler_info = rep.extract_assembler_info(assembly_dir)

if not os.path.isdir(assembly_dir):
    sys.stderr.write("Cannot find assembly directory %s\n" % assembly_dir)
    sys.exit(1)

if not os.path.isfile(assembly_fasta):
    sys.stderr.write("Could not find %s in %s\n" % (assembly_fasta, assembly_dir))
    sys.exit(1)

#BEGIN create reports and release information

report_builder = rep.ReportBuilder("Metatranscriptome Assembly")

report_builder.add_project_section(analysis)

# Get QC statistics #
qc_info = rep.get_qc_info(analysis,verbose=args.verbose)
report_builder.add_qc_section(qc_info)

# Get information for assembly #
if args.verbose:
    sys.stderr.write("Getting assembly stats for %s\n" % os.path.relpath(assembly_fasta, os.getcwd()))
report_builder.add_assembly_section(assembly_fasta, assembler_info)

# write info for JAMO release 
exporter = jat_utils.JamoSubmissionExporter()
exporter.send_email(args.email)
for dest in args.release_to:
    exporter.add_release_dest(dest)

rep.add_analysis_metadata(exporter, analysis)
exporter.add_output("metatranscriptome_assembly", assembly_fasta)
exporter.add_output_metadata(assembly_fasta,"assembler",assembler_info['assembler'])
exporter.add_output_metadata(assembly_fasta,"assembler_version",assembler_info['assembler_version'])
exporter.add_output_metadata(assembly_fasta,"assembler_parameters",assembler_info['assembler_parameters'])
exporter.add_output_metadata(assembly_fasta,"file_format","fasta")

fasta_stats = qcutils.get_fasta_stats(assembly_fasta)
for key in fasta_stats.keys():
    val = fasta_stats[key]
    exporter.add_output_metadata(assembly_fasta,key,val)

shell_script = os.path.join(os.getcwd(),rep.get_shell_script(assembly_dir))
exporter.add_output("shell_script", shell_script)
exporter.add_output_metadata(shell_script, "file_format", "text")

# create text report
report_file_names = rep.make_reports("%s/" % assembly_dir, report_builder, json_exporter=exporter)
if not report_file_names['pdf']:
    sys.stderr.write("Could not create PDF. Exiting.\n")
    sys.exit(1)

#############################################
# Write metadata file for submission to JAT #
#############################################
sys.stdout.write("%s\n" % assembly_dir)
rep.write_jat_metadata_file(exporter,assembly_dir,args.yaml)

if args.jat_template:
    template = args.jat_template
    try:
        jat_key = qcutils.release_to_jat_with_copy_verification(template, assembly_dir, verbose=args.verbose)
        sys.stdout.write("%s\n" % jat_key)
    except Exception as e:
        sys.stderr.write(e.message)

