#!/usr/bin/env python

from __future__ import division
import argparse
import sys
import os
import subprocess
import glob
import json
import collections
import re
import time
import datetime
import urllib2
import base64
import json

import EnvironmentModules as EnvMod
for x in (os.getenv("PYTHONPATH") if os.getenv("PYTHONPATH") else "").split(":"):
    if x not in sys.path:
        sys.path.append(x)
import qcutils


#writes stats file
#writes rqc page

version = "0.0.1"

def main():
    sanityCheckSuccess = True
    parser = argparse.ArgumentParser(description='Description.')
    
    parser.add_argument('-v','--version', action='version',version="%(prog)s (version " +  version + ")")

    parser.add_argument("-d","--dir",required=True,help="dir.\n")
    parser.add_argument("-s","--statsoutput",required=True,help="key value pairs file.\n")
#    parser.add_argument("-f","--filesoutput",required=True,help="key value pairs file.\n")


    args = parser.parse_args()
    args.dir = os.path.realpath(args.dir)
    args.dir = args.dir + '/'
    if (os.path.exists(args.dir) == 0):
        sys.exit()
    if args.statsoutput.startswith("/"):
        sys.exit("files should not be absolute paths")

    metadatafiles = get_glob(args.dir + "*/metadata.json")

    metadata=[]
    for mfile in metadatafiles:
        f = open(mfile,"r")
        metadata.append(json.loads(f.read()))
        f.close()

    kvp = dict()
    kvp["stats.date"] = time.strftime("%Y%m%d")

    kvp["sanity_check.metadata_empty_keys"] = ""
    kvp["sanity_check.release_spid_in_jamo"] = ""
    kvp["sanity_check.release_ap_in_jamo"] = ""
    kvp["sanity_check.release_ap_to_taxonoid"] = ""

    kvp["sanity_check.file_size_check"] =True
    kvp["outputs.assembly.contig_bp"]=""
    kvp["outputs.alignment.aligned_reads"]=""

    for md in metadata:
        empty_keys = metadata_empty_keys(md)
        for key in empty_keys:
            if kvp["sanity_check.metadata_empty_keys"]=="":
                kvp["sanity_check.metadata_empty_keys"]=str(key)
            else:
                kvp["sanity_check.metadata_empty_keys"]+="," + str(key)

        spid_in_jamo=release_spid_in_jamo(md)
        for spid in spid_in_jamo:
            if kvp["sanity_check.release_spid_in_jamo"]=="":
                kvp["sanity_check.release_spid_in_jamo"]=str(spid)
            else:
                kvp["sanity_check.release_spid_in_jamo"]+="," + str(spid)

        apid_in_jamo=release_ap_in_jamo(md)
        for apid in apid_in_jamo:
            if kvp["sanity_check.release_ap_in_jamo"]=="":
                kvp["sanity_check.release_ap_in_jamo"]=str(apid)
            else:
                kvp["sanity_check.release_ap_in_jamo"]+= "," + str(apid)

        kvp["sanity_check.release_ap_to_taxonoid"]=qcutils.its_ap_to_taxonoid(md)

        for output in md['outputs']:
            if output['label'] in ["metatranscriptome_assembly","metatranscriptome_alignment"]:
                if kvp["sanity_check.file_size_check"]==True:
                    size_ok=file_size_check(md,1000)
                    if not size_ok:
                        kvp["sanity_check.file_size_check"] = False
            if output['label'] in ["metatranscriptome_assembly"]:
                if kvp["outputs.assembly.contig_bp"]=="":
                    kvp["outputs.assembly.contig_bp"]=str(output['metadata']["contig_bp"])
                else:
                    kvp["outputs.assembly.contig_bp"]+="," + str(output['metadata']["contig_bp"])
            if output['label'].find("metatranscriptome_alignment") !=-1 and output['label'].find("metatranscriptome_alignment_") ==-1:
                if kvp["outputs.alignment.aligned_reads"]=="":
                    kvp["outputs.alignment.aligned_reads"]=str(output["metadata"]["num_aligned_reads"])
                else:
                    kvp["outputs.alignment.aligned_reads"]+="," + str(output["metadata"]["num_aligned_reads"])
                    

    #add all output metadata
    for mfile in metadata:
        for output in mfile["outputs"]:
            if "metadata" in output.keys():
                for key in output["metadata"].keys():
                    kvp["outputs." + output["label"] + "." + key] = output["metadata"][key]
#        kvp["mv." + output["label"]] = output["file"]

 

    reportfinal = get_output_file(metadata[0],"readme",1)
    reportdir = os.path.dirname(get_output_file(metadata[0],"readme"))

    #Add log fail check
    rqc_status = get_glob(args.dir + "rqc_status.log")[0]
    rqc_log = get_glob(args.dir + "mtaa-*.log")[0]
    kvp["rqc_status_log_failures"]= strings_in_file(rqc_status,["failure","failed"])
    kvp["rqc_status_log_email_sent"] = strings_in_file(rqc_status,["email report generated and mailed to"])
#    kvp["rqc_log_python_error"] = strings_in_file(rqc_log,["Traceback"])

    #write stats
    with open (args.dir + "/" + args.statsoutput,"w") as f:
        for key in sorted(kvp.keys()):
            f.write(key + "=" + str(kvp[key]) + "\n")
    f.close()
    files = dict()

    sys.exit()
    
#    files[os.path.basename(reportfinal)] = reportfinal
#    files[os.path.basename(os.path.splitext(reportfinal)[0] + ".pdf")] = os.path.splitext(reportfinal)[0] + ".pdf"
#    files["report_avg_fold_vs_len.bmp"] = reportdir + "/report_avg_fold_vs_len.bmp"
#    files["report_gc.bmp"]              = reportdir + "/report_gc.bmp"
#    files["report_gc_vs_avg_fold.bmp"]  = reportdir + "/report_gc_vs_avg_fold.bmp"
#    files["report.release.html"] = reportdir + "/report.release.html"
#    files["assembledTaxMap"] = reportdir + "/../taxMap/out.sam.gz.tax.sorted"

    #write files
    with open (args.dir + "/" + args.filesoutput,"w") as f:
        for key in files.keys():
            f.write(key + "=" + files[key] + "\n")
    f.close()

    return


def get_output_file(metadata,key,index=0):
    file = ""
    for output in metadata["outputs"]:
        if output["label"] == key:
            file = output["file"]
#            file = file.split(":")[index]
    return file
    
def its_ap_to_taxonoid(metadata):
  oid = ""
  apids = []
  if "analysis_project_id" in metadata["metadata"].keys():
      if hasattr(metadata['metadata']['analysis_project_id'], '__iter__'):
          apid = metadata['metadata']['analysis_project_id'][0]
      else:
          apid = metadata['metadata']['analysis_project_id']


  url = "https://gold.jgi.doe.gov/rest/analysis_project/" + str(apid)
  #authKey=base64.b64encode(username + ':' + password)                                                                                                                                                                                                             
  authKey = "amdpX2dhZzozSzgqZiV6"
  headers = {"Content-Type":"application/json", "Authorization":"Basic " + authKey}
  request = urllib2.Request(url)
  for key,value in headers.items():
    request.add_header(key,value)
  response = urllib2.urlopen(request)
  #print response.info().headers                                                                                                                                                                                                                                   
  data = json.loads(response.read())
  if "imgTaxonOid" in data.keys() and data["imgTaxonOid"] != None:
      oid = data["imgTaxonOid"]
  return oid

def reads_raw(metadata):
    output = ""
    for file in metadata["outputs"]:
        if file["label"] == "reads_filtered":
            output = file["metadata"]["num_input_reads"]
    return output

def reads_filtered(metadata):
    output = ""
    for file in metadata["outputs"]:
        if file["label"] == "reads_filtered":
            output = file["metadata"]["num_input_reads"] - file["metadata"]["contam_filtered"]
    return output

def reads_perc_artifact(metadata):
    output = ""
    for file in metadata["outputs"]:
        if file["label"] == "reads_filtered":
            output = file["metadata"]["perc_artifact"]
    return output

def reads_unmapped(metadata):
    output = ""
    for file in metadata["outputs"]:
        if file["label"] == "metagenome_unmapped_merged":
            output = file["metadata"]["readcount_input"] * 2
    return output

def reads_unmapped_merged(metadata):
    output = ""
    for file in metadata["outputs"]:
        if file["label"] == "metagenome_unmapped_merged":
            output = file["metadata"]["readcount"] * 2
    return output

def reads_unmapped_unmerged(metadata):
    output = ""
    for file in metadata["outputs"]:
        if file["label"] == "metagenome_unmapped_unmerged":
            output = file["metadata"]["readcount"] * 2
    return output

def file_size_check(metadata,min_size):
    '''check size of all outputs'''
    metadata_outputs = dict()
    all_greater_than_min = True
    for file in metadata["outputs"]:
        fileloc = file["file"].split(":")[0]
        metadata_outputs[file["file"]] = os.stat(os.path.abspath(fileloc)).st_size
        if metadata_outputs[file["file"]] <min_size:
            all_greater_than_min = False
    return all_greater_than_min
    

def release_spid_in_jamo(metadata):
    released = []
    #get mapping readcount from metadata.json
    spids = []
    if hasattr(metadata['metadata']['analysis_project_id'], '__iter__'):
        spids = metadata['metadata']['sequencing_project_id']
    else:
        spids = [metadata['metadata']['sequencing_project_id']]
    for spid in spids:
        cmd = "module load jamo; jamo info all custom \"metadata.sequencing_project_id=" + str(spid) + "\" | grep AUTO  | wc -l" 
        jamo_rel_file_count = int(os.popen(cmd).readlines()[0])
        if jamo_rel_file_count > 0:
            released.append(spid)
    return released


def release_ap_in_jamo(metadata):
    released = []
    #get mapping readcount from metadata.json
    spids = []
    if hasattr(metadata['metadata']['analysis_project_id'], '__iter__'):
        spids = metadata['metadata']['analysis_project_id']
    else:
        spids = [metadata['metadata']['analysis_project_id']]
    for spid in spids:
        cmd = "module load jamo; jamo info all custom \"metadata.analysis_project_id=" + str(spid) + "\" | grep AUTO  | wc -l" 
        #print cmd
        jamo_rel_file_count = int(os.popen(cmd).readlines()[0])
        if jamo_rel_file_count > 0:
            released.append(spid)
    return released


    
            
def get_glob(glob_txt):
    globs = glob.glob(glob_txt)
    if len(globs) == 0:
        sys.stdout.write("No " + glob_txt)
    return globs



def metadata_empty_keys(metadata):
    #check for empty metadata keys and values with 0
    empty = []
    #check metadata in all outputs for missing data excluding the following
    exclude_fields = ["gap_pct","scaf_pct_gt50K","scaf_n_gt50K"]
    for i in range(0,len(metadata["outputs"])):
        for key in metadata["outputs"][i].keys():
            if key == "metadata":
                for key2 in metadata["outputs"][i]["metadata"]:
                    if metadata["outputs"][i]["metadata"][key2] == "" and key2 not in exclude_fields:
                        empty.append(key2)
                    elif metadata["outputs"][i]["metadata"][key2] ==0 and key2 not in exclude_fields:
                        empty.append(key2)


    #check main metadata section for empty fields
    exclude_fields = []
    for key in metadata["metadata"]:
        if hasattr(metadata["metadata"][key], '__iter__'):
            if len(metadata["metadata"][key])<1:
                empty.append(key)
        elif str(metadata["metadata"][key])=="":
            empty.append(key)
        elif isinstance(metadata["metadata"][key],int) and metadata["metadata"][key]==0:
            empty.append(key)
        else:
            pass

    if len(empty) > 0:
        return empty
    else:
        return []
    
def strings_in_file(_file,str_list):
    '''returns False if string from str_list is found in file'''
    in_file = False
    try:
        with open(_file,'r') as f:
            for line in f.readlines():
                for str in str_list:
                    if str in line:
                        in_file = True
    except:
        in_file="__FILE_ERROR__"
    return in_file

def has_missing_keys(a,b):
    has_missing = False
    if (len(set(a.keys()) - set(b.keys())) !=0 or  len(set(b.keys()) - set(a.keys())) !=0):
        has_missing=True
    return has_missing
    
            
if __name__ == "__main__":
        main()

