#!/usr/bin/env python

import sys
import os
import gzip
import bz2
import subprocess
import getpass
import argparse

sys.path.append(os.path.dirname(os.path.realpath(__file__)) + "/../lib")
sys.path.append(os.path.dirname(os.path.realpath(__file__)) + "/..")

import gputils
import qcutils
import jgi_mt


VERSION="1.0"


usage="usage: %(prog)s [options] <ref> <fastq>"
desc="Maps all reads from a list of fastqs against a reference sequence.\n\n\n"
parser = qcutils.get_argparser(usage=usage, desc=desc)
parser.add_argument("reference", type=os.path.abspath, help="the reference to map to")
parser.add_argument("fastq", type=os.path.abspath, help="fastq to map against reference")
parser.add_argument("-b", "--outbase", dest="outbase", metavar="FILE", help="the output base for naming output files. default is basename(fastq).v.basename(ref)")
parser.add_argument("-o", "--outdir", dest="outdir", metavar="FILE", help="the output directory to save files to. default is basename(fastq).v.basename(ref)")
parser.add_argument("--local", dest="local", action="store_true", default=False, help="run read mapping locally. default is to submit to UGE")
parser.add_argument("--norun", dest="norun", action="store_true", default=False, help="do not run or submit job, just create script and exit")
parser.add_argument("--cov", dest="cov", action="store_true", default=False, help="create coverage file")

group = parser.add_mutually_exclusive_group()
group.add_argument("--merge_unmapped", dest="merge_unmapped", action="store_true", default=False, help="merge unmapped reads")
group.add_argument("--discard_unmapped", dest="discard_unmapped", action="store_true", default=False, help="do not save unmapped reads")

parser.add_argument_group(gputils.get_UGE_arguments(parser,"read_mapping",mem=7,time="12:00:00",pe_slots=16))

if len(sys.argv) == 1:
    parser.print_help()
    sys.exit(0)

args = parser.parse_args()

reference = args.reference

# create an output base if it has not been given
if not args.outbase:
    args.outbase = jgi_mt.get_mapping_base(args.reference,args.fastq)

if not args.outdir:
    args.outdir = jgi_mt.get_mapping_dir(args.reference,args.fastq)

#begin: create a set of all modules we need to load

modules = set()
modules.add('bbtools')
modules.add('samtools')

#end:   create a set of all modules we need to load


shell_script=args.outdir+".sh"
log_file=args.outdir+".log"
writer = gputils.GPShellScriptBuilder(args)
writer.add_log_file(log_file)

writer.add_modules(*modules)

writer.add_env_var("BBMAP", "bbmap.sh")
writer.add_env_var("BBMERGE", "bbmerge.sh")
writer.add_env_var("SAMTOOLS", "samtools")
writer.add_env_var("OUTBASE", args.outbase)
writer.add_env_var("REF", reference)
writer.add_env_var("INPUT_FASTQ", args.fastq)

writer.add_outdir(args.outdir,args.tmpscratch)

# OUTPUT FILES
output_base="${OUTBASE}"
bam = jgi_mt.get_bam(base=output_base,sorted_bam=False)

if (args.fastq.endswith("fastq.gz")or args.fastq.endswith("fq.gz")):
    input="stdin.fq.gz"
elif(args.fastq.endswith(".fasta")or args.fastq.endswith(".fa")):
    input="stdin.fasta"
elif(args.fastq.endswith(".fastq") or args.fastq.endswith(".fq")):
    input="stdin.fastq"
else:
    sys.exit("Can't recognize format of " + args.fastq)
    

map_cmd = "cat $INPUT_FASTQ | $BBMAP int=t ambig=best nodisk=t in=%s ref=$REF out=%s printunmappedcount=t " % (input,bam)
unmapped = output_base+".unmapped.fastq.gz"
if not args.discard_unmapped:
    map_cmd += "outu=%s " % unmapped
if args.cov:
    map_cmd += "covstats=%s " % bam.replace(".bam",".bam.cov")

map_cmd += "2> map.log"
log = "Running mapping and writing to %s" % bam
writer.add_command(map_cmd, log)

if args.merge_unmapped:
    log = "Merging unmapped reads %s" % unmapped
    merged = output_base + ".unmapped.merged.fastq.gz"
    unmerged = output_base + ".unmapped.unmerged.fastq.gz"
    merge_cmd = "$BBMERGE int=t in=%s out=%s outu=%s hist=merge.hist.txt " % (unmapped,merged,unmerged)
    merge_cmd += "2> merge.log"
    writer.add_command(merge_cmd, log)

## STORAGE COMMAND
log = "Sorting BAM %s" % bam
sorted_bam = jgi_mt.get_bam(base=output_base,sorted_bam=True)
sort_cmd = "$SAMTOOLS sort %s %s 2> sort.log" % (bam, sorted_bam[0:-4])
writer.add_command(sort_cmd, log)

log = "Indexing BAM %s" % sorted_bam
index_cmd = "$SAMTOOLS index %s %s 2> index.log" % (sorted_bam, sorted_bam.replace(".bam",".bai"))
writer.add_command(index_cmd, log)

writer.add_command("rm %s" % bam)

shell_script=args.outdir+".sh"
sh = open(shell_script,"w")
writer.write_script(sh)
sh.close()

# DONE CREATING SCRIPT

if args.norun:
    sys.exit(0)

if args.local:
    sys.stdout.write("Running read mapping locally\n")
    cmd=[ "bash", shell_script  ]
    subprocess.Popen(cmd, stderr=subprocess.STDOUT, stdout=open(log_file,"w"))
else:
    sys.stdout.write("Submitting read mapping to genepool\n")
    gputils.submit_to_genepool(shell_script,args.name,args.hold)
