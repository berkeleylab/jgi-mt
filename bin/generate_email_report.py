#!/usr/bin/env python

import sys
import argparse
import subprocess
import string as _string
from time import localtime, strftime

import sdm_curl
import qcutils
import jgi_mt
from jgi_mt import reporting as rep

def get_args():
    usage="%(prog)s sequnit spid lib [options]"
    desc="Make a release report for a metatranscriptome assembly"
    epilog="Required modules: bbtools, qaqc, jamo"
    parser = qcutils.get_argparser(usage,desc=desc,epilog=epilog)
    parser.add_argument("sequnit", type=str, default=None, help="the sequence unit used for this analysis")
    parser.add_argument("spid", type=int, default=None, help="the sequencing project ID for this analysis")
    parser.add_argument("lib", type=str, help="the library name for the data being released")
    parser.add_argument("-c", "--comment", dest="comment", type=str, help="additional text to prepend to report")
    parser.add_argument("-v", "--verbose", action="store_true", default=False, help="print more information about what is being done to standard error")
    parser.add_argument("-f", "--force", action="store_true", default=False, help="force reporting, even if ticket is Completed")
    parser.add_argument("--legacy", action="store_true", default=False, help="library was processed using old methods")
    email_group=parser.add_mutually_exclusive_group()
    email_group.add_argument("-e", "--email", type=str, metavar='TO', help="email report to specified recipient")
    email_group.add_argument("--asm_release", action="store_true", default=False, help="email report to asm_release")
    
    if len(sys.argv) < 2:
        parser.print_help()
        sys.exit(0)
    
    return parser.parse_args()
#end get_args

#### END FUNCTIONS ####

args = get_args()
if args.asm_release:
    args.email="asm_release@quagmire.jgi-psf.org"


# get all the project information we need
library_name = args.lib
sequencing_project_id = args.spid
seq_unit_name = args.sequnit
sequencing_project_name = qcutils.get_proj_info(sequencing_project_id, verbose=args.verbose)[0][2]
proposal_id, proposal_title, pi_name, pi_email = qcutils.get_proposal_info(sequencing_project_id, verbose=args.verbose)


project_info = { 'sequencing_project_id'     : [sequencing_project_id],
                 'sequencing_project_name'   : sequencing_project_name,
                 'seq_unit_name'             : [seq_unit_name,], 
                 'library_name'              : [library_name],
                 'proposal_title'            : proposal_title,
                 'proposal_id'               : proposal_id,
                 'pi_name'                   : pi_name,
                 'pi_email'                  : pi_email }

analysis_projects = qcutils.get_analysis_projects(sequencing_project_id,verbose=args.verbose)
assembly_ap, mapping_self_ap, mapping_other_aps = jgi_mt.separate_aps(analysis_projects)

#BEGIN create reports and release information

report_builder = rep.ReportBuilder("Metatranscriptome Analysis Release")
report_builder.add_project_section(project_info)

#####################
# Get QC statistics #
#####################
#qc_info = rep.get_qc_info(project_info,verbose=args.verbose)[0]
#report_builder.add_qc_section(qc_info)
#preproc_fastq = _string.replace(project_info["seq_unit_name"][0],".fastq.gz",".anqrpht.fastq.gz")
preproc_fastq = _string.replace(project_info["seq_unit_name"][0],".fastq.gz",".filter-MTF.fastq.gz")
report_builder.add_cleanedinputfile_section(preproc_fastq)

################################
# Get information for assembly #
################################
assembly_fasta = rep.get_assembly_fasta(sequencing_project_id=sequencing_project_id, verbose=args.verbose)
assembler_info = rep.get_assembler_info(sequencing_project_id=sequencing_project_id, verbose=args.verbose)
report_builder.add_assembly_section(assembly_fasta, assembler_info)

##################################
# Get information for alignments #
##################################
mapping_info = rep.get_mapping_stats(mapping_self_ap['analysis_project_id'],verbose=args.verbose)
mapping_info['analysis_project_id'] = mapping_self_ap['analysis_project_id']
reference_name = "Metatranscriptome Assembly"
report_builder.add_alignment_section(reference_name, mapping_info)

for ap in mapping_other_aps:
    mapping_info = rep.get_mapping_stats(ap['analysis_project_id'],verbose=args.verbose)
    mapping_info['analysis_project_id'] = ap['analysis_project_id']
    mapping_info['img_dataset_id'] = ap['img_dataset_id']
    reference_name = rep.get_taxon_name(mapping_info['img_dataset_id'])
    report_builder.add_alignment_section(reference_name, mapping_info)

report_names = rep.make_reports("%s."%library_name,report_builder)
text_file_name = report_names['txt']
pdf_file_name = report_names['pdf']
if not pdf_file_name:
    sys.stderr.write("Could not create PDF. Exiting.\n")
    sys.exit(1)
sys.stderr.write("Report written to %s and %s\n" % (text_file_name, pdf_file_name))

if args.email:
    recipient = args.email
    (analyst_fullname, analyst_email) = qcutils.get_analyst_info(verbose=args.verbose)
    from_email = "%s <%s>" % (analyst_fullname, analyst_email)
    subject = "Metatranscriptome - %d - %s" % (sequencing_project_id, sequencing_project_name)
    reply_to = "Alex Copeland <accopeland@lbl.gov>, Alicia Clum <aclum@lbl.gov>, Brian Foster <bfoster@lbl.gov>, Adam Rivers <arrivers@lbl.gov>"
    email_cmd = "cat %s | EMAIL=\"%s\" REPLYTO=\"%s\" mutt -s \"%s\" -a %s -- %s" % (text_file_name, from_email, reply_to, subject, pdf_file_name, recipient)
    sys.stderr.write("Emailing report to %s\n" % recipient)
    if args.verbose:
        sys.stderr.write("%s\n" % email_cmd)
    proc = subprocess.check_call(email_cmd,shell=True)
