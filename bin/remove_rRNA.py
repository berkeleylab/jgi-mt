#!/usr/bin/env python

import sys
import os
import gzip
import bz2
import subprocess
import getpass
import argparse

import gputils
import qcutils
import jgi_mt

VERSION="1.0"


usage="usage: %(prog)s [options] <fastq1> <fastq2> ... <fastqN>"
desc="Maps all reads from a list of fastqs against the ribosomal RNA database, and extracts reads thats do not map to any entries on the database\n\n\n"
version="%(prog)s " +VERSION
#parser = argparse.ArgumentParser(usage=usage,version=version,description=desc)
parser = qcutils.get_argparser(usage,desc=desc)
parser.add_argument("fastq", type=os.path.abspath, help="fastq files to clean")
parser.add_argument("-b", "--outbase", dest="outbase", metavar="FILE", help="the output base for naming output files. default is basename(fastq)")
parser.add_argument("-o", "--outdir", dest="outdir", metavar="FILE", help="the output directory to save files to. default is basename(fastq).rRNA.filter")
parser.add_argument("-d", "--database", dest="database", metavar="FILE", default=qcutils.RIBODB, help="the rRNA database to screen against. default is "+qcutils.RIBODB)
parser.add_argument("--noclean", dest="noclean", action="store_true", default=False, help="do not clean up intermediate fastq and sam files")
parser.add_argument("--local", dest="local", action="store_true", default=False, help="run rRNA removal locally. default is to submit to UGE")
parser.add_argument("--norun", dest="norun", action="store_true", default=False, help="do not run or submit job, just create script and exit")


parser.add_argument_group(gputils.get_UGE_arguments(parser,"remove_rRNA",mem=7,pe_slots=16))

if len(sys.argv) == 1:
    parser.print_help()
    sys.exit(0)

args = parser.parse_args()

# create an output base if it has not been given
if not args.outbase:
    args.outbase = jgi_mt.get_base(args.fastq)

# change how these strings get built
if not args.outdir:
    args.outdir = args.outbase + jgi_mt.PREPROC_OUTDIR_SUFFIX

shell_script=args.outdir+".sh"
log_file=args.outdir+".log"
writer = gputils.GPShellScriptBuilder(args)
writer.add_log_file(log_file)


# figure out which modules we need to load
modules = { "bbtools" }
writer.add_modules(*modules)


# write executable and database names
writer.add_env_var("BBDUK", "bbduk.sh")
writer.add_env_var("BBMAP", "bbmap.sh")
writer.add_env_var("RM_HU", "removehuman.sh")
writer.add_env_var("BBCOUNT", "bbcountunique.sh")
writer.add_env_var("ADPTRDB", "%s,%s" % (qcutils.NEXTERA_ADPT,qcutils.TRUSEQ_ADPT))
writer.add_env_var("AFTDB", "%s,\"\\\n\"%s,\"\\\n\"%s,\"\\\n\"%s" % (qcutils.ILL_AFT,qcutils.DNA_SPIKEIN,qcutils.RNA_SPIKEIN,qcutils.PHIX))
writer.add_env_var("RIBODB_DIR", "%s" % (os.path.dirname(qcutils.RIBODB)))
writer.add_env_var("OUTBASE", args.outbase)
writer.add_env_var("INPUT_FASTQ", args.fastq)

writer.add_outdir(args.outdir,args.tmpscratch)

#
# Trim adapter, low quality bases, and remove artifacts
#
process_cleaned_fastq = "$OUTBASE.anqtp.fastq"
adapter_stats = "$OUTBASE.adapter.stats"
artifact_stats = "$OUTBASE.artifact.stats"

log = "Trimming adapter and low quality bases, and removing artifacts"
cmd =  "$BBDUK -Xmx55g t=16 int=t in=$INPUT_FASTQ out=stdout.fq ref=$ADPTRDB ktrim=r k=25 mink=12 tpe=t tbo=t qtrim=r trimq=10 maq=10 maxns=3 minlen=50 hdist=1 stats=%s 2> adapter.log|\\\n" % (adapter_stats)
cmd += "$BBDUK -Xmx55g t=16 int=t in=stdin.fq out=%s ref=$AFTDB k=31 hdist=1 stats=%s 2> artifact.log" % (process_cleaned_fastq,artifact_stats)
writer.add_command(cmd,log)
#writer.add_command(cmd)

#
# Remove ribosomal RNA and human contam
#
rrna_cleaned_fastq = process_cleaned_fastq.replace(".fastq","hR.fastq.gz")
rrna_fastq = process_cleaned_fastq.replace(".fastq",".rRNA.fastq.gz")
rrna_stats = "$OUTBASE.artifact.stats"
human_stats = "$OUTBASE.human.stats"

log = "Removing rRNA and human sequence"
cmd =  "$BBMAP -Xmx80g t=16 int=t in=%s outu=stdout.fq outm=%s path=$RIBODB_DIR scafstats=%s fast=t minid=0.90 local=t printunmappedcount=t 2> rRNA.log|\\\n" % (process_cleaned_fastq, rrna_fastq, rrna_stats)
cmd += "$RM_HU -Xmx30g t=16 int=t in=stdin.fq outu=%s scafstats=%s printunmappedcount=t 2> human.log" % (rrna_cleaned_fastq,human_stats)
writer.add_command(cmd,log)
#writer.add_command(cmd)


mersample_counts = rrna_cleaned_fastq.replace(".fastq.gz",".mersamples.txt")
log = "Counting 20mers in %s" % rrna_cleaned_fastq
cmd = "$BBCOUNT -Xmx80g in=%s out=%s cumulative=t interval=1000000 k=20 percent=t count=t plb=t 2> mersample.log" % (rrna_cleaned_fastq,mersample_counts)
writer.add_command(cmd,log)

cleanup_files = process_cleaned_fastq

shell_script=args.outdir+".sh"
sh = open(shell_script,"w")
writer.write_script(sh)
sh.close()

# DONE CREATING SCRIPT


if args.norun:
    sys.exit(0)

if args.local:
    sys.stdout.write("Running rRNA removal locally\n")
    cmd=[ "bash", shell_script  ]
    subprocess.Popen(cmd, stderr=subprocess.STDOUT, stdout=open(log_file,"w"))
else:
    sys.stdout.write("Submitting rRNA removal job to genepool\n")
    gputils.submit_to_genepool(shell_script, args.name, hold = args.hold)
