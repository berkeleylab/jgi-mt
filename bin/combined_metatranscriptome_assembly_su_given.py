#!/usr/bin/env python

import sys
import os
import argparse
import re
import math
import json
import urllib2
import functools

import qcutils
import gputils
import jira_utils
import jgi_mt

INPUT_LIBRARY = False

def get_preproc_time(fastq):
    size = os.path.getsize(fastq)
    hours = math.ceil(0.5 + 5.787841666666667e-10 * size)
    return "%0.f:00:00" % hours
#end get_preproc_time

def ticket_or_lib(string):
    global INPUT_LIBRARY
    if re.match("SEQQC-[0-9]{3,5}",string):
        return string
    elif re.match("[A-Z]{4,5}",string) or re.match("M[0-9]{4,5}",string):
        INPUT_LIBRARY = True
        return string
    else:
        raise argparse.ArgumentTypeError("%s is not a JIRA ticket or a library" % string)
#end ticket_or_lib

def get_args():
    usage="%(prog)s analysis_project_id [options]"
    desc = "Run the combined assembly pipeline"
    epilog = "Dependent on the following modules: oracle_client, gaag, python, perl, jamo"
    #parser = argparse.ArgumentParser(, description=desc, epilog=epilog)
    parser = qcutils.get_argparser(usage,desc=desc,epilog=epilog)
    
    parser.add_argument("analysis_project_id", type=int, help="Analysis Project ID for Combined Assembly")
    parser.add_argument("--ticket", type=str, help="JIRA ticket to update and work off")
    parser.add_argument("-t", "--time", metavar="HH:MM:SS", default="72:00:00", help="the run time to request when submitting to genepool. [72:00:00]")
    parser.add_argument("-m", "--assem_memory", type=int, choices=[1,2,3,4,5], default=1, help="memory level for assembly: 1=120G, 2=256G, 3=500G, 4=1T, 5=2T")
    parser.add_argument("--no_assembly", action='store_true', default=False, help="do not do assembly")
    parser.add_argument("--no_align", action='store_true', default=False, help="do not do alignments")
    parser.add_argument("-d", "--debug", action='store_true', default=False, help="do not submit jobs")
    parser.add_argument("-v", "--verbose", action="store_true", default=False, help="print more information about what is being done to standard error")

    return parser.parse_args()
#end get_args

def write_jira_ticket(key):
    out = open("jira_ticket_key","w")
    out.write("%s\n" % key)
    out.close()
#end write_jira_ticket

def run_cmd(cmd,debug):
    if not debug:
        jobid = qcutils.call_qsub_wrapper(cmd)
        return jobid
    else:
        cmd.insert(1,"--norun")
        qcutils.call_qsub_wrapper(cmd,fwd_stdout=False)
        sys.stderr.write("%s\n" % (" ".join(cmd)))
        return None
#run_cmd
    
def get_input_fastq(sequencing_project_id,verbose=False):
    qdata = {'metadata.sequencing_project_id':sequencing_project_id, 'metadata.filtered_type': 'COMPRESSED FILTERED FASTQ'}
    if verbose:
        sys.stderr.write("Querying JAMO with %s\n" % str(qdata))
    result = qcutils.__get_Curl__().post('api/metadata/query',data=qdata)
    return "%s/%s" % (result[0]['file_path'],result[0]['file_name'])

def link_file(fastq_abspath):
    fastq_file = fastq_abspath.split("/")[-1]
    if not os.path.islink(fastq_file):
        os.symlink(fastq_abspath, fastq_file)
    return fastq_file

if __name__ == "__main__":

    args = get_args()

    #######################################
    # Get information and setup workspace #
    #######################################
    jira = jira_utils.get_JIRA()
    issue = None
    library_name = None
    fastq_abspath = None
    if not args.debug:
        if args.ticket:
            sys.stdout.write("Updating ticket %s\n" % args.ticket)
            issue = jira.issue(args.ticket)
            if '23' in jira.transitions(issue):
                jira.transition_issue(issue, '23')
        
        # start work on ticket
        if issue:
            sys.stderr.write("starting work for %s\n" % issue.key)
            sys.stderr.write("\n")
            jira.add_comment(issue, "working directory: %s" % os.path.abspath('.'))
        
    
    preprocess_jobid = "0000000"
    assembly_jobid = "1111111"
    # submit preprocess job 
    analysis_project = qcutils.get_analysis_project(args.analysis_project_id, args.verbose)
    spids = map(lambda sp: sp['sequencing_project_id'], analysis_project['sequencing_projects'])
    libraries = map(lambda sp: qcutils.get_proj_info(sp)[0][1], spids)
    fastqs = map(functools.partial(get_input_fastq,verbose=args.verbose),spids)
    
    wd = "combined_assembly.%d" % args.analysis_project_id
    
    if not os.path.isdir(wd):
        os.mkdir(wd)
    os.chdir(wd)

    input_fastqs = map(link_file, fastqs)
    
    assembly_jobid = "1111111"
    assem_outdir = jgi_mt.get_combined_assembly_dir(args.analysis_project_id)
    assem_base = jgi_mt.get_combined_assembly_base(args.analysis_project_id)
    metatranscriptome_fasta = assem_outdir + '/' + jgi_mt.get_combined_assembly_fasta(args.analysis_project_id)
    if not args.no_assembly:
        assem_memory = [ "7", "15", "31", "62" , "112" ]
        memory,runtime = (assem_memory[args.assem_memory-1], args.time)
        if not os.path.isfile(metatranscriptome_fasta) or args.force:
            sys.stderr.write("submitting combined metatranscriptome assembly\n")
            sys.stderr.write("requesting %sG of memory per slot and %s hours of runtime\n" % (memory,runtime))
            assembly_cmd = [ "run_megahit.py", "-b", assem_base, "-o", assem_outdir, "-m", memory, "-t", runtime, "--tmpscratch", "-N", "comb_asm.%d" % args.analysis_project_id ]
            for fastq in input_fastqs:
                assembly_cmd += [ fastq ]
            if not args.debug:
                assembly_jobid = run_cmd(assembly_cmd, False)
                if issue:
                    jira.add_comment(issue, "rnnotator assembly submitted to genepool. Job ID %s" % assembly_jobid)
            else:
                run_cmd(assembly_cmd, True)
            sys.stderr.write("\n")
        else:
            sys.stderr.write("assembly complete, skipping Rnnotator assembly\n")
            
    map_reads_cmd = [ "map_mt_reads.py", "--tmpscratch" ] 
    if not args.no_align:
        for (lib,fastq) in zip(libraries,input_fastqs):
            mapping_dir = jgi_mt.get_combined_assembly_mapping_dir(args.analysis_project_id, fastq)
            mapping_base = jgi_mt.get_combined_assembly_mapping_base(args.analysis_project_id, fastq)
            metatranscriptome_alignment_bam = jgi_mt.get_bam(fastq=fastq, reference=metatranscriptome_fasta)
            if not os.path.isfile(metatranscriptome_alignment_bam) or args.force:
                sys.stderr.write("submitting mapping to combined metatranscriptome assembly\n")
                assembly_map_cmd = map_reads_cmd + [ "-o", mapping_dir, "-b", mapping_base, "--discard_unmapped", "-N", "%s.map.%d" % (lib,args.analysis_project_id), "--hold", assembly_jobid, metatranscriptome_fasta, fastq ]    
                if not args.debug:
                    assembly_map_jobid = run_cmd(assembly_map_cmd, False)
                    if issue:
                        jira.add_comment(issue, "mapping %s to combined assembly submitted to genepool. Job ID %s" % (lib,assembly_map_jobid))
                else:
                    run_cmd(assembly_map_cmd, True)
                sys.stderr.write("\n")
            else:
                sys.stderr.write("alignment to combined metatranscriptome assembly complete\n")
