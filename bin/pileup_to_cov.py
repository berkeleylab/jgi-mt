#!/usr/bin/env python

import sys
import fileinput

'''Takes a samtools sorted mpileup file as input file or stdin and returns two column coverage file'''
buffer = ""
counter = 0
running_sum=0
print "ID\tAvg_fold"

for line in fileinput.input():
    fields = line.split("\t")
    id=fields[0]
    cov=int(fields[-3])
    if buffer=="":
        buffer = id
    if id==buffer:
        running_sum=running_sum+cov
        counter=counter+1
        buffer==id
    else: #hit new contig
        ave = "%.0f" % (float(running_sum)/float(counter))
        print buffer + "\t" + ave
        running_sum=0+cov
        counter=1
        buffer=id

ave = "%.0f" % (float(running_sum)/float(counter))
print buffer + "\t" + ave

        
