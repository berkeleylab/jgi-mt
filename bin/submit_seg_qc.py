#!/usr/bin/env python

import sys
import subprocess

import qcutils
import jgi_mt
import sdm_curl

parser = qcutils.get_argparser(usage="%(prog)s its_key", desc="build and submit segment QC for the given MT")
parser.add_argument("its_key", type=str, help="sequencing project ID, libary name, or sequence unit")
parser.add_argument("-d", "--debug", action='store_true', default=False, help="do not submit jobs")
parser.add_argument("-v", "--verbose", action="store_true", default=False, help="print more information about what is being done to standard error")

args = parser.parse_args()
curl = sdm_curl.Curl("https://sdm2.jgi-psf.org")

sequencing_project_id, library_name, sequencing_project_name, seq_unit_name = qcutils.get_proj_info(args.its_key,verbose=args.verbose)[0]

analysis_projects = qcutils.get_analysis_projects(sequencing_project_id,verbose=args.verbose)
assembly_ap, mapping_self_ap, mapping_other_aps = jgi_mt.separate_aps(analysis_projects)

(total_reads, perc_low_qual, perc_artifact, perc_rRNA) = (None, None, None, None)
try:
    (total_reads, perc_low_qual, perc_artifact, perc_rRNA, bbtools_cleaned) = jgi_mt.reporting.get_qc_stats(sequencing_project_id, curl, args.verbose)
except Exception as e:
    sys.stderr.write("Error getting QC statistics: %s\n" % e.message)
    sys.exit(1)

assembly_fasta = None
try:
    assembly_fasta = jgi_mt.reporting.get_assembly_fasta(sequencing_project_id, curl, args.verbose)
except Exception as e:
    sys.stderr.write("Error getting Assembly statistics: %s\n" % e.message)
    sys.exit(1)

(total_reads, mapped_reads) = (None, None)
try:
    (total_reads, mapped_reads) = jgi_mt.reporting.get_mapping_stats(mapping_self_ap['analysis_project_id'], curl, args.verbose)
except Exception as e:
    sys.stderr.write("Error getting Self-Alignment statistics: %s\n" % e.message)
    sys.exit(1)


template = jgi_mt.reporting.build_seg_qc_template(seq_unit_name, total_reads, perc_low_qual, perc_artifact, perc_rRNA, assembly_fasta, mapped_reads)

template_file_name = "%s.seg_qc" % library_name
with open(template_file_name,'w') as out:
    out.write(template)

if not args.debug:
    jgi_mt.reporting.submit_seg_qc(template_file_name)

