#!/usr/bin/env python
'''
jgi_mt_run.py:
takes a filtered fastq file and runs_metatranscriptome pipeline

TODO:
add genepool/cori switch

'''


from __future__ import division
import argparse
import sys, os
import json
import re
import subprocess
import collections

#if 'NERSC_HOST' in os.environ.keys():
#    if os.environ['NERSC_HOST'].lower()=='genepool':
#        cmd = 'module show jgi-mga/2.0 2>&1 |  grep JGI_MGA_DIR'
#        dir = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE).stdout.read().rstrip()
#        dir = re.sub(r'^.*\s(\/.*)$',r'\1',dir) + '/lib'
#        sys.path.insert(0,dir)
#        RQC_REPORT = "module load jgi-rqc;$JGI_RQC_DIR/report/rqc_report.py "
#    elif os.environ['NERSC_HOST'].lower()=='denovo':
#        dir = '/global/projectb/sandbox/gaag/bfoster/2.0/jgi-mga/lib'
#        sys.path.insert(0,dir)
#        RQC_REPORT = "/global/dna/projectdirs/PI/rqc/prod/jgi-rqc-pipeline/report/rqc_report.py"
#    else:
#        print "Nersc host: " + os.environ['NERSC_HOST'] + " not recognized\n"
#import jgi_mga_utils


VERSION = "1.0.0"
DEFAULT_TEMPLATE = "metatranscriptome_separate_ats.yaml"
RUNNER = "/global/projectb/sandbox/gaag/bfoster/2.0/jgi-mga/bin/pipeline_runner.py"

def main():
    '''main'''
    parser = argparse.ArgumentParser(description='Description.')
    parser.add_argument("-f", "--file", required=True, help="comma separated files to assemble and map.\n")
    parser.add_argument("-t", "--template", required=False,  help="filtered file to assemble and map. default is " + DEFAULT_TEMPLATE )
    parser.add_argument("-r", "--run_dir", required=False, help="run_dir to run. default is current working directory\n")
    parser.add_argument("-o", "--outputfile", required=False, default="run.yaml", help="runfile. default = run.yaml\n")
    parser.add_argument("-x", "--execute", required=False, default = False, action='store_true', help="execute using surm.\n")
    parser.add_argument("-l", "--local", required=False, default = False, action='store_true', help="execute locally.\n")
    parser.add_argument("-c", "--cluster_name", required=False, default ="metat", help="cluster name.\n")
    parser.add_argument("-d", "--debug", required=False, default = False, action='store_true', help="debugging.\n")
    parser.add_argument('-v', '--version', action='version', version="%(prog)s (version " + VERSION + ")")

    args = parser.parse_args()

    if not os.path.exists(args.file):
        sys.exit("Can't find " + args.file)
    inputs = " ".join(args.file.split(","))
        
    
    if not args.template:
        args.template = os.path.dirname(os.path.realpath(__file__)) + "/" + DEFAULT_TEMPLATE
    with open(args.template) as f:
        template = f.read()
    template = re.sub(r'__INPUT_FILES__',inputs,template)
    
    if not args.run_dir:
        args.run_dir =  os.getcwd()

    runfile = args.run_dir.rstrip("/") + "/" + os.path.basename(args.outputfile)
    with open(runfile, "w") as f:
        f.write(template)


    cmd = "cd " + args.run_dir + "; " +  RUNNER + " -f " + runfile
    with open(runfile + ".cmd", "w") as file:
        file.write(cmd + "\n")

    cmd = "cd " + args.run_dir + ';sbatch -t 1200 --mincpus=16 --mem=120G -D $PWD -J ' + args.cluster_name + ' --wrap="bash run.yaml.cmd"'
    slurmfile = runfile + ".cmd.slurm"
    with open(slurmfile, "w") as file:
        file.write(cmd + "\n")

    if not args.execute:
        sys.exit()
    else:
        if args.local:
            cmd = "cd " + args.run_dir +  ';bash run.yaml.cmd'
        p = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True)
        (output, err) = p.communicate()
        p_status = p.wait()
        if (p_status == 0):
            sys.stdout.write(output)
            with open(slurmfile + ".o", "w") as file:
                file.write(output)
            with open(slurmfile + ".e", "w") as file:
                file.write(err)
        else:
            sys.stdout.write("Subprocess failed for " + cmd)
            sys.stdout.write("STDOUT\n" + output + "\n\n")
            sys.stderr.write("STDERR\n" + err + "\n\n")
            sys.exit(p_status)


#     metadata = hasher()
#     #get jamo record for filtered file fill in spid, library, sequnit
#     jamo = jgi_mga_utils.web_services("jamo", {"file_name":os.path.basename(args.file)})
#     if len(jamo)==1:
#         jamo = jamo[0]
#     else:
#         sys.exit("multiple jamo results for " + args.file)
#     metadata['metadata']['library_name'] = [jamo['metadata']['library_name']]
#     metadata['metadata']['sequencing_project_id'] = [jamo['metadata']['sequencing_project_id']]
#     metadata['metadata']['seq_unit_name'] = jamo['metadata']['seq_unit_name']
#     metadata['metadata']['analysis_task_id'] = []
#     metadata['metadata']['analysis_project_id'] = []

#     if len(metadata['metadata']['sequencing_project_id']) !=1 :
#         sys.exit("need one spid per su " + json.dumps(metadata,indent=3))
#     ap_ = jgi_mga_utils.web_services('ap_byspid',{'sequencing_project_id':metadata['metadata']['sequencing_project_id'][0]})
#     for ap in ap_:
#         for task in ap['uss_analysis_project']['analysis_tasks']:
#             if task['analysis_task_type_id'] in [48, 83] and task['current_status_id'] not in [9,10,12,13,99]: #"RNA Assembly and Mapping"
#                 #- "complete", 9
#                 #- "abandoned", 10
#                 #- "on hold", 12
#                 #- "deleted", 13
#                 #- "rework", 99
#                 metadata['metadata']['analysis_project_id'].append(ap['uss_analysis_project']['analysis_project_id'])
#                 metadata['metadata']['analysis_task_id'].append(task['analysis_task_id'])

    
#     with open(args.outputfile, "w") as f:
#         f.write(json.dumps(metadata,indent = 3))
#     if args.debug:
#         with open(args.outputfile + ".jamo" , "w") as f:
#             f.write(json.dumps(jamo,indent = 3))
#         with open(args.outputfile + ".ap" , "w") as f:
#             f.write(json.dumps(ap_,indent = 3))

#     if len(metadata['metadata']['analysis_task_id']) !=1 or len(metadata['metadata']['analysis_project_id']) !=1:
#         print json.dumps(metadata['metadata'],indent=3)
#         sys.exit("Something wrong with AP/AT setup")


# def hasher():
#     return collections.defaultdict(hasher)

if __name__ == "__main__":
    main()
