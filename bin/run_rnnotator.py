#!/usr/bin/env python

import sys
import os
import subprocess
import bz2
import getpass
import math
import argparse

import gputils
import qcutils
import jgi_mt

########## HELPER FUNCTIONS ##########     


def get_mem(file_gigabytes,num_threads):
    #gigabytes = (1.845 *file_gigabytes + 1.527)/num_threads
    gigabytes = (2.845 *file_gigabytes + 1.527)/num_threads
    gigabytes = int(math.ceil(gigabytes))
    gigabytes = 6 if gigabytes < 6 else gigabytes
    return gigabytes

def get_time(file_gigabytes):
    hours = 0.3032*file_gigabytes
    # add 4 hours for the read mapping
    hours = int(math.ceil(hours) + 4)
    hours = 6 if hours < 6 else hours
    return str(hours)+":00:00"

########## END HELPER FUNCTIONS ##########     

VERSION="1.0"

usage="usage: %(prog)s [options] <fastq1> <fastq2> ... <fastqN>"
desc="Assembles fastq data with Rnnotator"
#version="%prog: "+VERSION
#formatter = IndentedHelpFormatter(width=120,max_help_position=50)
parser = qcutils.get_argparser(usage=usage, desc=desc)

parser.add_argument("fastq", nargs='+',type=os.path.abspath, help="fastqs to be assembled")
parser.add_argument("-l", "--ins_length", dest="insert", metavar="INT", type=int, default=300, help="the insert size of the library [300]\n")
parser.add_argument("-b", "--outbase", dest="outbase", metavar="FILE", help="the output base for naming output files. default is basename(fastq)")
parser.add_argument("-o", "--outdir", dest="outdir", metavar="DIR", help="the output directory to save files to. default is basename(fastq).mtAssembly")
parser.add_argument("-r", "--restart", dest="restart_dir", metavar="DIR", help="restart rnnotator using the given directory")
parser.add_argument("--phred64", action="store_true", help="quality scores are in phred64. default assumes phred33")
parser.add_argument("--local", dest="local", action="store_true", default=False, help="run rRNA removal locally. default is to submit to UGE")
parser.add_argument("--norun", dest="norun", action="store_true", default=False, help="do not run or submit job, just create script and exit")
parser.add_argument("--noclean", dest="noclean", action="store_true", default=False, help="do not delete temporary or intermediate files")
parser.add_argument("--norm_depth", dest="norm_depth", default=40, type=int, help="the depth to normalize to before assembly")

parser.add_argument_group(gputils.get_UGE_arguments(parser,"rnnotator",7,"72:00:00",16))

if len(sys.argv) == 1:
    parser.print_help()
    sys.exit(0)

args = parser.parse_args()


# SET IMPORTANT RUNTIME VARIABLES
RNNOTATOR="rnnotator.pl"

# create an output base if it has not been given
if not args.outbase:
    args.outbase = jgi_mt.get_rnnotator_base(*args.fastq)

if not args.outdir:
    args.outdir = jgi_mt.get_assembly_dir(*args.fastq)

shell_script=args.outdir+".sh"
log_file=args.outdir+".log"
writer = gputils.GPShellScriptBuilder(args)
writer.add_log_file(log_file)


modules = set()
modules.update(*qcutils.get_module('rnnotator.pl'))
modules.add('bbtools')

writer.add_modules(*modules)


writer.add_env_var("RNNOTATOR", RNNOTATOR)
writer.add_env_var("BBMERGE","bbmerge.sh")
writer.add_env_var("BBNORM","bbnorm.sh")
writer.add_env_var("INPUT_FASTQ", " ".join(args.fastq))
writer.add_env_var("OUTBASE",args.outbase)
writer.add_env_var("FQ_BASE",jgi_mt.get_base(*args.fastq))

writer.add_outdir(args.outdir,args.tmpscratch)

#
# MERGING READS
#
merged = "$FQ_BASE.merged.fastq.gz"
unmerged = "$FQ_BASE.unmerged.fastq.gz"

log_msg = "Merging $INPUT_FASTQ" 
cmd = "cat $INPUT_FASTQ | $BBMERGE in=stdin.fq.gz out=%s int=t outu=%s 2> merge.log" % (merged,unmerged)
writer.add_command(cmd,log_msg)

#
# NORMALIZE READS
#
bbnorm_cmd = "$BBNORM in=%s extra=%s passes=1 mindepth=2 " + ("target=%d" % args.norm_depth) + " out=%s "
merged_norm = merged.replace(".fastq",".norm.fastq")
unmerged_norm = unmerged.replace(".fastq",".norm.fastq")
log_msg = "Normalizing merged reads: %s" % merged
cmd = (bbnorm_cmd + "int=f 2> norm_merged.log") % (merged,unmerged,merged_norm)
writer.add_command(cmd,log_msg)

log_msg = "Normalizing unmerged reads: %s" % unmerged
cmd = (bbnorm_cmd + "int=t 2> norm_unmerged.log") % (unmerged,merged,unmerged_norm)
writer.add_command(cmd,log_msg)

log_msg = "Assembling $INPUT_FASTQ with Rnnotator"
RNNOTATOR_OPTIONS = "-o $OUTBASE -l $OUTBASE.log -n 4 -strS %s -strP %d %s " % (merged_norm, args.insert, unmerged_norm)
cmd = "$RNNOTATOR " + RNNOTATOR_OPTIONS +" > rnnotator.out 2> rnnotator.err"
writer.add_command(cmd,log_msg)

final_contigs = "%s/final_contigs.fa" % (args.outbase)
writer.add_required_output(final_contigs)

shell_script=args.outdir+".sh"
sh = open(shell_script,"w")
writer.write_script(sh)
sh.close()

# DONE CREATING SCRIPT

if args.norun:
    sys.exit(0)

if args.local:
    sys.stdout.write("Running Rnnotator assembly locally\n")
    cmd=[ "bash", shell_script  ]
    subprocess.Popen(cmd, stderr=subprocess.STDOUT, stdout=open(log_file,"w"))
else:
    sys.stdout.write("Submitting Rnnotator assembly job to genepool\n")
    gputils.submit_to_genepool(shell_script, args.name, hold = args.hold)


