#!/usr/bin/env python
'''
jgi_mga_setup.py:
takes a filtered fastq file and gets ap/at metadata and sets run 

TODO:
add genepool/cori switch

'''


from __future__ import division
import argparse
import sys, os
import json
import re
import subprocess
import collections

if 'NERSC_HOST' in os.environ.keys():
    if os.environ['NERSC_HOST'].lower()=='genepool':
        cmd = 'module show jgi-mga/2.0 2>&1 |  grep JGI_MGA_DIR'
        dir = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE).stdout.read().rstrip()
        dir = re.sub(r'^.*\s(\/.*)$',r'\1',dir) + '/lib'
        sys.path.insert(0,dir)
        RQC_REPORT = "module load jgi-rqc;$JGI_RQC_DIR/report/rqc_report.py "
    elif os.environ['NERSC_HOST'].lower()=='denovo':
        dir = '/global/projectb/sandbox/gaag/bfoster/2.0/jgi-mga/lib'
        sys.path.insert(0,dir)
        RQC_REPORT = "/global/dna/projectdirs/PI/rqc/prod/jgi-rqc-pipeline/report/rqc_report.py"
    else:
        print "Nersc host: " + os.environ['NERSC_HOST'] + " not recognized\n"
                
import jgi_mga_utils


VERSION = "1.0.0"


def main():
    '''main'''
    parser = argparse.ArgumentParser(description='Description.')
    parser.add_argument('-v', '--version', action='version', version="%(prog)s (version " + VERSION + ")")
    parser.add_argument("-f", "--file", required=True, help="filtered file to assemble and map.\n")
    parser.add_argument("-d", "--debug", required=False, default = False, action='store_true', help="debugging.\n")
    parser.add_argument("--force", required=False, default = False, action='store_true', help="force creation with lastest ap.\n")
    parser.add_argument("-o", "--outputfile", required=True, help="output file json\n")
    args = parser.parse_args()

    if not os.path.exists(args.file):
        sys.exit("Can't find " + args.file)


    metadata = hasher()
    metadata['inputs'] = [args.file]
    #get jamo record for filtered file fill in spid, library, sequnit
    jamo = jgi_mga_utils.web_services("jamo", {"file_name":os.path.basename(args.file)})
    if len(jamo)==1:
        jamo = jamo[0]
    else:
        sys.exit("multiple jamo results for " + args.file)
    metadata['metadata']['library_name'] = [jamo['metadata']['library_name']]
    metadata['metadata']['sequencing_project_id'] = [jamo['metadata']['sequencing_project_id']]
    metadata['metadata']['seq_unit_name'] = jamo['metadata']['seq_unit_name']
    metadata['metadata']['analysis_task_id'] = []
    metadata['metadata']['analysis_project_id'] = []

    if len(metadata['metadata']['sequencing_project_id']) !=1 :
        sys.exit("need one spid per su " + json.dumps(metadata,indent=3))
    ap_ = jgi_mga_utils.web_services('ap_byspid',{'sequencing_project_id':metadata['metadata']['sequencing_project_id'][0]})
    for ap in ap_:
        for task in ap['uss_analysis_project']['analysis_tasks']:
            if task['analysis_task_type_id'] in [48, 83]:
                if task['current_status_id'] not in [9,10,12,13,99]: #"RNA Assembly and Mapping"
                    #- "complete", 9
                    #- "abandoned", 10
                    #- "on hold", 12
                    #- "deleted", 13
                    #- "rework", 99
                    metadata['metadata']['analysis_project_id'].append(ap['uss_analysis_project']['analysis_project_id'])
                    metadata['metadata']['analysis_task_id'].append(task['analysis_task_id'])
                elif args.force and task['current_status_id']==9:
                    metadata['metadata']['analysis_project_id'].append(ap['uss_analysis_project']['analysis_project_id'])
                    metadata['metadata']['analysis_task_id'].append(task['analysis_task_id'])
    if len(metadata['metadata']['analysis_project_id'])!=1 or len(metadata['metadata']['analysis_task_id'])!=1:
        if args.force:
            metadata['metadata']['analysis_project_id'] = [sorted([int(x) for x in metadata['metadata']['analysis_project_id']])[-1]]
            metadata['metadata']['analysis_task_id'] = [sorted([int(x) for x in metadata['metadata']['analysis_task_id']])[-1]]
        else:
            print json.dumps(metadata,indent=3)
            sys.exit("\nSomething wrong with AT\n")

            
    
    with open(args.outputfile, "w") as f:
        f.write(json.dumps(metadata,indent = 3))
    if args.debug:
        with open(args.outputfile + ".jamo" , "w") as f:
            f.write(json.dumps(jamo,indent = 3))
        with open(args.outputfile + ".ap" , "w") as f:
            f.write(json.dumps(ap_,indent = 3))

    if len(metadata['metadata']['analysis_task_id']) !=1 or len(metadata['metadata']['analysis_project_id']) !=1 or metadata['metadata']['analysis_task_id'][0]==0 or metadata['metadata']['analysis_project_id'][0]==0:
        print json.dumps(metadata['metadata'],indent=3)
        if not args.force:
            sys.exit("Something wrong with AP/AT setup")


def hasher():
    return collections.defaultdict(hasher)

if __name__ == "__main__":
    main()
