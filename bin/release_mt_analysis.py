#!/usr/bin/env python

import sys
import os
import fnmatch
import argparse
import subprocess
import pysam
import functools
import re
import string as _string
from time import localtime, strftime

import qcutils
import jgi_mt
from jgi_mt import reporting as rep
import jat_utils

HEADER_COUNT = 1

def get_args():
    usage="%(prog)s analysis_project_id [options]"
    desc="Make a release report for a metatranscriptome analysis"
    parser = argparse.ArgumentParser(usage=usage,description=desc,formatter_class=argparse.RawTextHelpFormatter)
    
    rep.add_standard_release_args(parser)
    rep.add_project_info_arguments(parser)
    rep.add_reporting_arguments(parser)
    rep.add_jat_arguments(parser)

    if len(sys.argv) < 2:
        parser.print_help()
        sys.exit(0)
    
    return parser.parse_args()
#end get_args

args = get_args()

analysis = jgi_mt.get_analysis_info(args.analysis_project_id, verbose=args.verbose, seq_unit_name=args.sequnit,library_name=args.lib)

# Get QC statistics #
#qc_info = rep.get_qc_info(analysis,verbose=args.verbose)
#preproc_fastq = qc_info[0]['fastq'][0]
if ".srf" in analysis["seq_unit_name"][0]:
    preproc_fastq = _string.replace(analysis["seq_unit_name"][0],".srf",".anqrpht.fastq.gz")
else:
    preproc_fastq = _string.replace(analysis["seq_unit_name"][0],".fastq.gz",".filter-MTF.fastq.gz")
#    preproc_fastq = _string.replace(analysis["seq_unit_name"][0],".fastq.gz",".anqrpht.fastq.gz")
#    preproc_fastq = _string.replace(analysis["seq_unit_name"][0],".fastq.gz",".anqtp.hR.fastq.gz")
    
#preproc_fastq = "combined.input.fastq.gz" 

report_builder = rep.ReportBuilder()
report_builder.add_project_section(analysis)
report_builder.add_cleanedinputfile_section(preproc_fastq)
#for info in qc_info:
#    report_builder.add_qc_section(info)

exporter = jat_utils.JamoSubmissionExporter()
try:
    input = os.popen("module load jamo; jamo info all filename " +  preproc_fastq + ' | cut -f 2 -d " " ').readlines()[0].rstrip()
except:
    sys.exit("input file:" + preproc_fastq + " not found in jamo")

exporter.add_input(input)
exporter.send_email(args.jat_email)
for dest in args.release_to:
    exporter.add_release_dest(dest)
rep.add_analysis_metadata(exporter, analysis)

analysis_dir = None
if analysis['analysis_product_id'] in { 10, 11, 40, 61 }: 
    # this is an assembly project
    assembly_fasta = None
    assembly_info = None
    if analysis['analysis_product_id'] == 40:
        # this is a combined assembly project
        report_builder.set_title('Combined Metatranscriptome Assembly')
        analysis_dir = jgi_mt.get_combined_assembly_dir(analysis['analysis_project_id'])
        assembly_fasta = os.path.join(os.getcwd(),analysis_dir,jgi_mt.get_combined_assembly_fasta(args.analysis_project_id))
        assembler_info = rep.extract_assembler_info(analysis_dir)
    else:
        # this is just a single-library assembly
        if analysis['analysis_product_id'] == 61:
            report_builder.set_title('Metatranscriptome Eukaryotic Community Assembly')
        else:
            report_builder.set_title('Metatranscriptome Assembly')
        analysis_dir = jgi_mt.get_assembly_dir(preproc_fastq)
        assembly_fasta = os.path.join(os.getcwd(),analysis_dir,jgi_mt.get_assembly_fasta(preproc_fastq))
        assembler_info = rep.extract_assembler_info(analysis_dir)
    
    if not os.path.isdir(analysis_dir):
        sys.stderr.write("Cannot find assembly directory %s\n" % analysis_dir)
        sys.exit(1)
    
    if not os.path.isfile(assembly_fasta):
        sys.stderr.write("Could not find %s in %s\n" % (assembly_fasta, analysis_dir))
        sys.exit(1)

    if args.verbose:
        sys.stderr.write("Getting assembly stats for %s\n" % os.path.relpath(assembly_fasta, os.getcwd()))

    report_builder.add_assembly_section(assembly_fasta, assembler_info)

    exporter.add_output("metatranscriptome_assembly", assembly_fasta)
    exporter.add_output_metadata(assembly_fasta,"assembler",assembler_info['assembler'])
    exporter.add_output_metadata(assembly_fasta,"assembler_version",assembler_info['assembler_version'])
    exporter.add_output_metadata(assembly_fasta,"assembler_parameters",assembler_info['assembler_parameters'])
    exporter.add_output_metadata(assembly_fasta,"file_format","fasta")
    
    fasta_stats = qcutils.get_fasta_stats(assembly_fasta)
    for key in fasta_stats.keys():
        val = fasta_stats[key]
        exporter.add_output_metadata(assembly_fasta,key,val)

else:
    # this is a mapping project
    report_builder.set_title('Metatranscriptome Alignment')
    reference_name = None
    analysis_dir = None
    mapping_info = dict()
    if analysis['analysis_product_id'] in {38, 62}:
        # this is a mapping to self project
        mapping_info['source_analysis_project_id'] = analysis['source_analysis_project_id']
        analysis_dir = jgi_mt.get_mapping_dir(jgi_mt.get_assembly_fasta(preproc_fastq), preproc_fastq)
        reference_name = "Metatranscriptome Assembly"
        report_builder.add_additional_comment("This is a self-alignment i.e. metatranscriptome reads aligned to the assembly of these reads")
    else:
        # this is a mapping to other project
        if 'img_dataset_id' in analysis:
            # this is a mapping to an IMG reference
            mapping_info['img_dataset_id'] = analysis['img_dataset_id']
            analysis_dir = jgi_mt.get_mapping_dir(analysis['img_dataset_id'], preproc_fastq) 
            reference_name = rep.get_taxon_name(analysis['img_dataset_id'])
        else:
            # this is a mapping to a combined assembly
            mapping_info['source_analysis_project_id'] = analysis['source_analysis_project_id']
            analysis_dir = jgi_mt.get_combined_assembly_mapping_dir(mapping_info['source_analysis_project_id'], preproc_fastq)
            reference_name = "Combined Metatranscriptome Assembly"
            report_builder.add_additional_comment("This is an alignment to a combined metatranscriptome assembly")
    
    if not os.path.isdir(analysis_dir):
        sys.stderr.write("Cannot find mapping directory %s\n" % analysis_dir)
        sys.exit(1)
    
    if args.verbose:
        sys.stderr.write("Getting mapping_info from %s\n" % analysis_dir)

    mapping_info.update(rep.get_mapping_info(analysis_dir,args.legacy))
    aligner_parameters = ""
    if 'aligner_parameters' in mapping_info:
        aligner_parameters = mapping_info['aligner_parameters']
    
    # Build reports
    report_builder.add_alignment_section(reference_name, mapping_info)
    
    bam = mapping_info['bam']
    exporter.add_output("metatranscriptome_alignment", bam)
    exporter.add_output_metadata(bam, "num_aligned_reads", mapping_info['mapped_reads'])
    exporter.add_output_metadata(bam, "num_input_reads", mapping_info['total_reads'])
    exporter.add_output_metadata(bam, "aligner", mapping_info['aligner'])
    exporter.add_output_metadata(bam, "aligner_version", mapping_info['aligner_version'])
    exporter.add_output_metadata(bam, "aligner_parameters", aligner_parameters)
    exporter.add_output_metadata(bam, "file_format", "bam")
    
    bai = mapping_info['bai']
    exporter.add_output("metatranscriptome_alignment_index", bai)
    exporter.add_output_metadata(bai, "file_format", "bai")

    if 'cov' in mapping_info.keys():
        cov = mapping_info['cov']
        exporter.add_output("metatranscriptome_assembly_coverage", cov)
        exporter.add_output_metadata(cov, "file_format", "cov")
        
# Build metadata.json
shell_script = os.path.join(os.getcwd(),rep.get_shell_script(analysis_dir))
exporter.add_output("shell_script", shell_script)
exporter.add_output_metadata(shell_script, "file_format", "latex")

# Create text report #
report_file_names = rep.make_reports("%s/" % analysis_dir, report_builder, json_exporter=exporter)
if not report_file_names['pdf']:
    sys.stderr.write("Could not create PDF. Exiting.\n")
    sys.exit(1)

# Write metadata file for submission to JAT #
sys.stdout.write("%s\n" % analysis_dir)
rep.write_jat_metadata_file(exporter,analysis_dir,args.yaml)

if args.jat_template:
    template = args.jat_template
    try:
        jat_key = qcutils.release_to_jat(template, analysis_dir, verbose=args.verbose)
        sys.stdout.write("%s\n" % jat_key)
    except Exception as e:
        sys.stderr.write(e.message)
