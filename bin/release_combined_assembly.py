#!/usr/bin/env python

import sys
import os
import fnmatch
import argparse
import subprocess
import pysam
import functools
import re
from time import localtime, strftime

import qcutils
import jgi_mt
from jgi_mt import reporting as rep
import jat_utils

HEADER_COUNT = 1

def get_args():
    usage="%(prog)s analysis_project_id [options]"
    desc="Make a release report for a metatranscriptome analysis"
    parser = argparse.ArgumentParser(usage=usage,description=desc,formatter_class=argparse.RawTextHelpFormatter)

    parser.add_argument("--no_alignments", dest='alignment', action='store_false', default=True, help='do not release alignments, just release assembly')
    parser.add_argument("--no_assembly", dest='assembly', action='store_false', default=True, help='do not release assembly, just release alignments')

    rep.add_standard_release_args(parser)
    parser.add_argument('-d', '--debug', action='store_true', default=False, help='do not run release commands, just print to stdout')
    rep.add_project_info_arguments(parser)
    rep.add_reporting_arguments(parser)
    rep.add_jat_arguments(parser)
    
    email_group=parser.add_mutually_exclusive_group()
    asm_rel = "asm_release@quagmire.jgi-psf.org"
    email_group.add_argument("-e", "--email", type=str, metavar='TO', default=asm_rel, help="email report to specified recipient [%s]" % asm_rel)
    email_group.add_argument("--no_email", action="store_true", default=False, help="email report to asm_release")

    if len(sys.argv) < 2:
        parser.print_help()
        sys.exit(0)
    
    return parser.parse_args()
#end get_args

args = get_args()

assembly_ap = qcutils.get_analysis_project(args.analysis_project_id,verbose=args.verbose)

release_args = list()
if args.jat_template:
    assembly_template = args.jat_template
args_to_ignore = {'no_email', 'email', 'alignment', 'assembly', 'jat_template', 'analysis_project_id', 'debug'}
for (key,value) in args._get_kwargs():
    if key in args_to_ignore:
        continue
    elif key == 'release_to':
        if value == ['img', 'portal']:
            continue
    if isinstance(value,bool):
        if value:
            release_args.append("--%s" % key)
    else:
        if value is None:
            continue
        elif isinstance(value,list):
            release_args.append("--%s" % key)
            release_args.extend(value)
        else:
            release_args.append("--%s" % key)
            release_args.append(value)

release_cmds = list()

cmd = [ "release_mt_analysis.py", str(args.analysis_project_id) ] 
cmd.extend(release_args)
if assembly_template:
    cmd[2:2] = ['--jat_template', assembly_template]
if args.assembly and qcutils.is_analysis_project_unreleased(args.analysis_project_id):
    release_cmds.append(" ".join(cmd))

mapping_aps = jgi_mt.get_mapping_to_combined_aps(args.analysis_project_id,verbose=args.verbose)
if args.alignment:
    if assembly_template:
        mapping_template = 'metatranscriptome_alignment'
        if 'eukaryotic' in assembly_template:
            mapping_template = 'eukaryotic_metatranscriptome_alignment'
        cmd[3] = mapping_template
    for ap in mapping_aps:
        if qcutils.is_analysis_project_unreleased(ap):
            cmd[1] = str(ap['analysis_project_id'])
            release_cmds.append(" ".join(cmd))


for cmd in release_cmds:
    sys.stdout.write("%s\n" % cmd)
    if not args.debug:
        subprocess.call(cmd,shell=True)
    

if not args.no_email:
    report_builder = rep.ReportBuilder("Combined Metatranscriptome Assembly Release")
    analysis = jgi_mt.get_analysis_info(args.analysis_project_id, verbose=args.verbose)
    qc_info = rep.get_qc_info(analysis,verbose=args.verbose)
    report_builder.add_project_section(analysis)
    for i in xrange(len(qc_info)):
        qc_info[i]['sequencing_project_name'] = analysis['sequencing_project_name'][i]
        report_builder.add_qc_section(qc_info[i])
    assembly_fasta = rep.get_assembly_fasta(analysis_project_id=args.analysis_project_id, verbose=args.verbose)
    assembler_info = rep.get_assembler_info(analysis_project_id=args.analysis_project_id, verbose=args.verbose)
    report_builder.add_assembly_section(assembly_fasta, assembler_info)
    
    for ap in mapping_aps:
        mapping_info = rep.get_mapping_stats(ap['analysis_project_id'],verbose=args.verbose)
        mapping_info['sequencing_project_name'] = ap['sequencing_projects'][0]['sequencing_project_name']
        mapping_info['analysis_project_id'] = ap['analysis_project_id']
        reference_name = "Combined Metatranscriptome Assembly"
        report_builder.add_alignment_section(reference_name, mapping_info)

    report_names = rep.make_reports("%d."%args.analysis_project_id,report_builder)
    text_file_name = report_names['txt']
    pdf_file_name = report_names['pdf']
    if not pdf_file_name:
        sys.stderr.write("Could not create PDF. Exiting.\n")
        sys.exit(1)
    sys.stderr.write("Report written to %s and %s\n" % (text_file_name, pdf_file_name))

    recipient = args.email
    (analyst_fullname, analyst_email) = qcutils.get_analyst_info(verbose=args.verbose)
    from_email = "%s <%s>" % (analyst_fullname, analyst_email)
    subject = "Metatranscriptome - %d - %s" % (args.analysis_project_id, assembly_ap['analysis_project_name'])
    reply_to = "Alex Copeland <accopeland@lbl.gov>, Alicia Clum <aclum@lbl.gov>, %s" % from_email
    email_cmd = "cat %s | EMAIL=\"%s\" REPLYTO=\"%s\" mutt -s \"%s\" -a %s -- %s" % (text_file_name, from_email, reply_to, subject, pdf_file_name, recipient)
    sys.stderr.write("Emailing report to %s\n" % recipient)
    if args.verbose:
        sys.stderr.write("%s\n" % email_cmd)
    proc = subprocess.check_call(email_cmd,shell=True)


