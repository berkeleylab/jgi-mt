#!/usr/bin/env python

import sys
import os
import argparse
import re
import math
import json
import urllib2
from time import localtime, strftime
import socket
import subprocess


sys.path.append(os.path.dirname(os.path.realpath(__file__)) + "/../lib")
sys.path.append(os.path.dirname(os.path.realpath(__file__)) + "/..")

import qcutils
import jgi_mt

INPUT_LIBRARY = False
DEFAULT_STATUS_LOG = "rqc_status.log"


def get_filter_time(fastq):
    size = os.path.getsize(fastq)
    hours = math.ceil(0.5 + 5.787841666666667e-10 * size)
    return "%0.f:00:00" % hours
#end get_filter_time

def get_args():
    
    usage="%(prog)s fastq spid lib [options]"
    desc = "Run the metatranscriptome pipeline"
    epilog = "Dependent on the following modules: oracle_client, gaag, python, perl, jamo, qaqc"
    parser = qcutils.get_argparser(usage,desc=desc,epilog=epilog)

    parser.add_argument("fastq", type=str, help="the Sequence Unit to run pipeline on")
    parser.add_argument("spid", type=int, help="the sequencing project id")
    parser.add_argument("lib", type=str, help="the library name")
    parser.add_argument("-o", "--outdir", type=str, help="the directory to write output to")
    parser.add_argument("-l", "--log", default=DEFAULT_STATUS_LOG, type=str, help="the RQC status log file [%s]" % DEFAULT_STATUS_LOG)
    parser.add_argument("--filter", action='store_true', default=False, help="filter reads before assembly and alignment")
    parser.add_argument("--no_assembly", action='store_true', default=False, help="do not do assembly")
    parser.add_argument("--no_align", action='store_true', default=False, help="do not do alignments")
    parser.add_argument("--rnnotator", action='store_true', default=False, help="run assembly with Rnnotator. default is to use MEGAHIT")
    parser.add_argument("-m", "--assem_memory", type=int, choices=[1,2,3,4], default=1, help="memory level for assembly: 1=120G, 2=256G, 3=500G, 4=1T [120G]")
    parser.add_argument("--increase_memory", action='store_true', default=False, help="bump up memory request from predicted value")
    parser.add_argument("--increase_time", action='store_true', default=False, help="bump up time request from predicted value")
    parser.add_argument("--no_jat_release", action='store_true', default=True, help="do not release to JAT when jobs complete. just run release scripts")
    parser.add_argument("--force", action='store_true', default=False, help="force rerun. do not skip steps if they are complete")
    parser.add_argument("-v", "--verbose", action="store_true", default=False, help="be informative about what is being done")
    parser.add_argument("-d", "--debug", action='store_true', default=False, help="do not submit jobs")

    email_group=parser.add_mutually_exclusive_group()
    asm_rel = "asm_release@quagmire.jgi-psf.org"
    email_group.add_argument("-e", "--email", type=str, metavar='TO', default=asm_rel, help="email report to specified recipient [%s]" % asm_rel)
    email_group.add_argument("--no_email", action="store_true", default=False, help="email report to asm_release")

    return parser.parse_args()
#end get_args

def release_analysis_project(ap, sequencing_product_id=None, release_to_jat=False, library_name=None, seq_unit_name=None, verbose=False):
    apid = ap['analysis_project_id']
    release_procedure = jgi_mt.reporting.get_release_procedure(sequencing_product_id, ap['analysis_product_id'], verbose=verbose)
    template = release_procedure['jat_template']
    cmd = [ 'release_mt_analysis.py', str(apid) ]
    if library_name:
        cmd[1:1] = [ "--lib", library_name ]
    if seq_unit_name:
        cmd[1:1] = [ "--sequnit", seq_unit_name ]
    if release_to_jat:
        cmd[1:1] = [ "--jat_template", template ]
    ret = None
    if verbose:
        sys.stderr.write("\nCalling release command: %s\n" % " ".join(cmd))
        cmd[1:1] = [ '--verbose' ]
    try:
        output = subprocess.check_output(cmd).split('\n')
        ret = { 'jat_template': template, 'release_dir': output[0] }
        rqc_message = "metadata.json for analysis project %d created in %s" % (ap['analysis_project_id'], output[0])
        if release_to_jat:
            ret['jat_key'] = output[1]
            rqc_message += " (JAT key %s)" % output[1]
        write_rqc_log_message(rqc_message)
    except subprocess.CalledProcessError as cpe:
        write_rqc_log_message("failed to created metadata.json for analysis project %d" % ap['analysis_project_id'])
        raise Exception("error calling release command: %s" % (" ".join(cmd)))
    return ret

def run_cmd(cmd,job_name,hold=None):
    cmd_to_submit = cmd
    cmd_to_submit[1:1] = [ "--no_email" ]
    cmd_to_submit[1:1] = [ "--tmpscratch" ]
    cmd_to_submit[1:1] = [ "-N", job_name ]
    cmd_to_submit[1:1] = [ "--callback", "%s:%d" % (HOST,PORT) ]
    holds = list()
    if hold:
        # add the job dependency argument
        cmd_to_submit[1:1] = [ "--hold", hold ]
        holds.append(hold)
    jobid = None
    if DEBUG:
        cmd_to_submit[1:1] = [ "--norun" ]
        sys.stderr.write("%s\n" % (" ".join(cmd_to_submit)))
        qcutils.call_qsub_wrapper(cmd_to_submit,fwd_stdout=False)
        jobid = "%07d" % (len(JOBS)+1)
    else:
        sys.stderr.write("%s\n" % (" ".join(cmd_to_submit)))
        jobid = qcutils.call_qsub_wrapper(cmd_to_submit)


    # add the job information the the jobs database
    JOBS[jobid] = { "job_name": job_name, "holds": list() }
    if hold:
        # for the independent job, add the job that is dependent on it
        JOBS[hold]["holds"].append(jobid)
    return jobid
#run_cmd

mem_vals = [ "7.5", "15", "30", "62.5" ]
def calculate_memory(fracRandUniMer,numSeq, inc_mem=False):
    memory = 200.6*fracRandUniMer + 1.622e-6*numSeq - 84.19
    idx = -1
    if memory <= 120:
        idx = 0
    elif memory <= 248:
        idx = 1
    elif memory <= 512:
        idx = 2
    else:
        idx = 3
    if inc_mem:
        idx += 1
    return mem_vals[idx]
#end calculate_memory

def calculate_runtime(fracRandUniMer,numSeq,inc_time):
    runtime = 5496*fracRandUniMer + 6.521e-4*numSeq + 1563
    if runtime < 43200:
        runtime = 12
    else:
        runtime = int(runtime/3600 + 6)
    if inc_time:
        runtime += 12
    return "%d:00:00" % runtime
#end calculate_runtime

def get_complexity_stats(fastq):
    mersample = fastq.replace("fastq.gz","mersample.txt")
    data = None
    with open(mersample,'r') as mersample_in:
        for line in mersample_in:
            data = line
    data = data.split('\t')
    numSeq = int(data[0])
    fracRandUniMer = float(data[-1])
    return (numSeq,fracRandUniMer)
#end get_complexity_stats

def get_memory_and_runtime(fastq, inc_mem=False, inc_time=False):
    (numSeq,fracRandUniMer) = get_complexity_stats(fastq)
    memory = calculate_memory(fracRandUniMer,numSeq,inc_mem)
    runtime = calculate_runtime(fracRandUniMer,numSeq,inc_time)
    return (memory,runtime)
#end get_memory_and_runtime

def job_successful(job_id):
    pass
#end job_successful

def write_rqc_log_message(msg):
    RQC_LOG.write("%s,%s\n" % (msg,strftime("%Y-%m-%d %H:%M:%S", localtime())))
    RQC_LOG.flush()
#end write_rqc_log_message

def get_bam_output_file(reference, fastq):
    return "%s/%s" % (jgi_mt.get_mapping_dir(reference,fastq), jgi_mt.get_bam(reference=reference,fastq=fastq))

if __name__ == "__main__":
    args = get_args()
    global RQC_LOG, JOBS, DEBUG, HOST, PORT
    RELEASE_DIRS = list()
    DEBUG = args.debug
    FAILED_JOBS = list()
    EXCEPTIONS_FOUND = False
    RELEASE_TO_JAT = not args.no_jat_release

    sys.path.insert(1,"./")

    HOST = socket.gethostname()
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.bind(("0.0.0.0", 0))
    PORT = s.getsockname()[1]

    #######################################
    # Get information and setup workspace #
    #######################################

    fastq_abspath = os.path.abspath(args.fastq)
    sys.stderr.write("Running Metatranscriptome Pipeline on %s\n\n" % fastq_abspath)
    fq_base = qcutils.get_fastq_basename(args.fastq)
    SEQUNIT = ".".join(fq_base.split('.')[:4])+".fastq.gz"
    SEQUENCING_PROJECT_ID = args.spid
    SEQUENCING_PRODUCT_ID = qcutils.get_product_info(SEQUENCING_PROJECT_ID,verbose=args.verbose)[0]
    LIBRARY_NAME = args.lib

    wd = "%s.mtp" % fq_base
    if args.outdir:
        wd = args.outdir
    if not os.path.isdir(wd):
        os.mkdir(wd)
    os.chdir(wd)
    sys.stderr.write("Working in %s\n" % wd)

    RQC_LOG = open(args.log , "a")

    do_assembly = not args.no_assembly

    fastq_file = fastq_abspath.split("/")[-1]
    if not os.path.islink(fastq_file):
        os.symlink(fastq_abspath, fastq_file)

    analysis_projects = qcutils.get_analysis_projects(SEQUENCING_PROJECT_ID)
    assembly_ap, mapping_self_ap, mapping_other_aps = jgi_mt.separate_aps(analysis_projects)
    if assembly_ap == None:
        sys.stderr.write("**** No assembly analysis project found for sequencing project %d\n" % SEQUENCING_PROJECT_ID)
        sys.exit(1)
    if mapping_self_ap == None:
        sys.stderr.write("**** No mapping-to-self analysis project found for sequencing project %d\n" % SEQUENCING_PROJECT_ID)
        sys.exit(1)

    additional_mappings = { ap['img_dataset_id']: ap for ap in mapping_other_aps }



    JOBS = dict()

    filter_jobid = None
    assembly_jobid = None
    # submit filter job
    filtered_fastq = jgi_mt.get_filtered_fastq(jgi_mt.get_base(fastq_file))
    if args.filter:
        if not os.path.isfile(filtered_fastq):
            sys.stderr.write("submitting rRNA removal\n")
            filter_time = get_filter_time(fastq_abspath)
            job_name = "%s.rmRibo" % LIBRARY_NAME
            filter_cmd = [ "remove_rRNA.py", "-t", filter_time, fastq_file ]
            filter_jobid = run_cmd(filter_cmd, job_name)
            write_rqc_log_message("filtering submitting as %s (Job ID %s)" % (job_name, filter_jobid))
            sys.stderr.write("\n")
        else:
            sys.stderr.write("filtering done, skipping rRNA removal\n\n")
    else:
        filtered_fastq = fastq_file

    ### ASSEMBLY AND MAPPING TO SELF ####
    map_reads_cmd = [ "map_mt_reads.py", "--cov" ]
    if do_assembly or args.force:
        if qcutils.is_asm_analysis_project_unreleased(assembly_ap) or args.force:
            mem_options = [ "7", "15", "21", "62" ]
            memory,runtime = (mem_options[args.assem_memory-1], "12:00:00")
            #if args.comp_req_res:
            #    memory,runtime = get_memory_and_runtime(filtered_fastq,args.increase_memory,args.increase_time)
            metatranscriptome_fasta = "%s/%s" % (jgi_mt.get_assembly_dir(filtered_fastq),jgi_mt.get_megahit_fasta(filtered_fastq))
            assembly_cmd = [ "run_megahit.py", "-m", memory, "-t", runtime, filtered_fastq ]
            if args.rnnotator:
                metatranscriptome_fasta = jgi_mt.get_rnnotator_fasta(filtered_fastq)
                assembly_cmd[0] = "run_rnnotator.py"
            # If assembly is complete, do not run again
            if not os.path.isfile(metatranscriptome_fasta):
                sys.stderr.write("submitting metatranscriptome assembly\n")
                sys.stderr.write(" - requesting %sG of memory per slot and %s hours of runtime\n" % (memory,runtime))
                job_name = "%s.asm" % LIBRARY_NAME
                assembly_jobid = run_cmd(assembly_cmd, job_name, hold=filter_jobid)
                write_rqc_log_message("assembly (AP %d) submitted as %s (Job ID %s)" % (assembly_ap['analysis_project_id'], job_name, assembly_jobid))
                JOBS[assembly_jobid]['analysis_project'] = assembly_ap
                sys.stderr.write("\n")
            else:
                 #TODO ADD release command here
                sys.stderr.write("assembly complete but unreleased. releasing now... ")
                try:
                    output = release_analysis_project(assembly_ap, sequencing_product_id=SEQUENCING_PRODUCT_ID, release_to_jat=RELEASE_TO_JAT, library_name=LIBRARY_NAME, seq_unit_name=SEQUNIT, verbose=args.verbose)
                    RELEASE_DIRS.append((output['jat_template'], output['release_dir']))
                    sys.stderr.write("release successful!\n")
                except Exception as e:
                    EXCEPTIONS_FOUND = True
                    sys.stderr.write("released failed: %s\n" % e.message)

            ### MAPPING TO SELF ###
            # If mapping to self complete, do not run again
            if qcutils.is_aln_analysis_project_unreleased(mapping_self_ap) or args.force:
                metatranscriptome_alignment_bam = get_bam_output_file(metatranscriptome_fasta, filtered_fastq)
                if not os.path.isfile(metatranscriptome_alignment_bam):
                    sys.stderr.write("submitting mapping to metatranscriptome assembly\n")
                    job_name = "%s.map.MT" % LIBRARY_NAME
                    assembly_map_cmd = map_reads_cmd + [ "--merge_unmapped", metatranscriptome_fasta, filtered_fastq ]
                    assembly_map_jobid = run_cmd(assembly_map_cmd, job_name, hold=assembly_jobid)
                    write_rqc_log_message("alignment to self (AP %d) submitted as %s (Job ID %s)" % (mapping_self_ap['analysis_project_id'], job_name, assembly_map_jobid))
                    JOBS[assembly_map_jobid]['analysis_project'] = mapping_self_ap
                    sys.stderr.write("\n")
                else:
                    #TODO ADD release command here
                    sys.stderr.write("alignment to metatranscriptome assembly complete but unreleased. releasing now... ")
                    try:
                        output = release_analysis_project(mapping_self_ap, sequencing_product_id=SEQUENCING_PRODUCT_ID, release_to_jat=RELEASE_TO_JAT, library_name=LIBRARY_NAME, seq_unit_name=SEQUNIT, verbose=args.verbose)
                        RELEASE_DIRS.append((output['jat_template'], output['release_dir']))
                        sys.stderr.write("release successful!\n")
                    except Exception as e:
                        EXCEPTIONS_FOUND =  True
                        sys.stderr.write("released failed: %s\n" % e.message)
            else:
                sys.stderr.write("alignment to metatranscriptome assembly complete and released, skipping alignment to self\n\n")
        else:
            sys.stderr.write("assembly is complete and released, skipping assembly\n\n")
    else:
        sys.stderr.write("skipping assembly\n\n")


    ### MAPPING TO OTHERS ###
    if not args.no_align:
        # This will store pathes to reference files and output bams
        additional_mapping_files = dict()

        if len(additional_mappings) > 0:
            sys.stderr.write("Found the following references in ITS: %s\n" % ", ".join(str(x) for x in additional_mappings.keys()))
            ref_dir = "reference"
            if not os.path.isdir(ref_dir):
                os.mkdir(ref_dir)
            # Get References files and job params for IMG references
            for oid in additional_mappings.keys():
                ref_file = "%s/%s.fasta" % (ref_dir,oid)
                reference_alignment_bam = get_bam_output_file(ref_file, filtered_fastq)
                if not DEBUG:
                    try:
                        ref_out = open(ref_file,"w")
                        qcutils.extract_IMG_sequence(oid,ref_out)
                        ref_out.close()
                        additional_mapping_files[oid] = (ref_file,reference_alignment_bam)
                    except Exception as e:
                        EXCEPTIONS_FOUND =  True
                        sys.stderr.write("Could not extract reference fasta file for %d. Skipping this mapping.\n" % oid)
                        continue
                else:
                    additional_mapping_files[oid] = (ref_file,reference_alignment_bam)

            for reference in additional_mapping_files.keys():
                (ref_file,reference_alignment_bam) = additional_mapping_files[reference]
                mapping_ap = additional_mappings[reference]['analysis_project_id']
                if qcutils.is_aln_analysis_project_unreleased(mapping_ap) or args.force:
                    if not os.path.isfile(reference_alignment_bam):
                        sys.stderr.write("submitting mapping to reference %s\n" % reference)
                        job_name = "%s.map.%s" % (LIBRARY_NAME,reference)
                        mapping_cmd = map_reads_cmd + [ "--discard_unmapped", ref_file, filtered_fastq ]
                        mapping_jobid = run_cmd(mapping_cmd, job_name, hold=filter_jobid)
                        write_rqc_log_message("alignment to %s (AP %d) submitted as %s (Job ID %s)" % (reference, mapping_ap, job_name, mapping_jobid))
                        JOBS[mapping_jobid]['analysis_project'] = additional_mappings[reference]
                        sys.stderr.write("\n")
                    else:
                        sys.stderr.write("alignment to %s complete, but unreleased. releasing now... " % reference)
                        try:
                            output = release_analysis_project(additional_mappings[reference], sequencing_product_id=SEQUENCING_PRODUCT_ID, release_to_jat=RELEASE_TO_JAT, library_name=LIBRARY_NAME, seq_unit_name=SEQUNIT, verbose=args.verbose)
                            RELEASE_DIRS.append((output['jat_template'], output['release_dir']))
                            sys.stderr.write("release successful!\n")
                        except Exception as e:
                            EXCEPTIONS_FOUND =  True
                            sys.stderr.write("released failed: %s\n" % e.message)

                else:
                    sys.stderr.write("alignment to %s complete and released, skipping alignment\n\n" % reference)
        else:
            sys.stderr.write("no unreleased reference alignments found in ITS\n\n")
    else:
        sys.stderr.write("skipping additional alignment\n\n")

    if DEBUG:
        sys.exit(0)

    
    s.listen(1)
    while len(JOBS) > 0:
        conn, addr = s.accept()
        data = conn.recv(1024)
        if not ":" in data:
            sys.stderr.write("unrecognized metatranscriptome_pipeline message: %s\n" % data)
        else:
            job_id,retmsg = data.strip().split(":")
            # BEGIN: handle job completion
            if not job_id in JOBS:
                sys.stderr.write("received message '%s' for unrecognized job %s\n" % (retmsg,job_id))
                continue
            job = JOBS[job_id]
            if retmsg == "0":
                if 'analysis_project' in job:
                    # figure out what release script, and log completion
                    apid = job['analysis_project']['analysis_project_id']
                    if job['analysis_project']['analysis_product_id'] in jgi_mt.reporting.ASSEMBLY_APRODS:
                        sys.stderr.write("assembly finished. releasing now... ")
                        write_rqc_log_message("assembly (AP %d) finished. %s (Job ID %s)" % (apid, job['job_name'], job_id))
                    elif job['analysis_project']['analysis_product_id'] in jgi_mt.reporting.MAP_TO_SELF_APRODS:
                        sys.stderr.write("mapping to self finished. releasing now... ")
                        write_rqc_log_message("alignment to self (AP %d) finished. %s (Job ID %s)" % (apid, job['job_name'], job_id))
                    else:
                        sys.stderr.write("mapping to %s finished. releasing now... " % job['analysis_project']['img_dataset_id'])
                        write_rqc_log_message("alignment to %s (AP %d) finished. %s (Job ID %s)" % (job['analysis_project']['img_dataset_id'], apid, job['job_name'], job_id))
                    # BEGIN: run release command
                    try:
                        output = release_analysis_project(job['analysis_project'], sequencing_product_id=SEQUENCING_PRODUCT_ID, release_to_jat=RELEASE_TO_JAT, library_name=LIBRARY_NAME, seq_unit_name=SEQUNIT, verbose=args.verbose)
                        RELEASE_DIRS.append((output['jat_template'], output['release_dir']))
                        sys.stderr.write("release successful!\n")
                    except Exception as e:
                        EXCEPTIONS_FOUND =  True
                        sys.stderr.write("released failed: %s\n" % e.message)
                    # END: run release command
                else:
                    write_rqc_log_message("%s (Job ID %s) finished" % (job['job_name'],job_id))
            elif retmsg == "SIGTERM":
                write_rqc_log_message("%s (Job ID %s) failed due to insufficient resources" % (job['job_name'],job_id))
            elif retmsg == "SIGKILL":
                write_rqc_log_message("%s (Job ID %s) was killed by user" % (job['job_name'],job_id))
            else:
                #do something to figure out why job failed and reschedule if necessary
                sys.stderr.write("Job %s failed with message %s\n" % (job_id,retmsg))
                write_rqc_log_message("%s (Job ID %s) failed for unknown reasons (retmsg = %s)" % (job['job_name'], job_id, retmsg))
                FAILED_JOBS.append((job_id,retmsg))
            del JOBS[job_id]
            # END: handle job completion
            print 'Received this message: %s' % data
            conn.close()

    sys.stderr.write("Pipeline finished\n")
    
    if len(FAILED_JOBS) == 0 and EXCEPTIONS_FOUND == False:
        if not RELEASE_TO_JAT:
            sys.stderr.write("Execute the following JAT commands to release all analyses\n")
            for template, directory in RELEASE_DIRS:
                sys.stderr.write("jat import %s %s\n" % (template,directory))
        else:
            try:
                cmd = [ 'generate_email_report.py', SEQUNIT , str(SEQUENCING_PROJECT_ID), LIBRARY_NAME ]
                if not args.no_email:
                    cmd[1:1] = [ '--email', args.email ]
                if args.verbose:
                    cmd[1:1] = [ '--verbose' ]
                    sys.stderr.write("Generating email report with the following command:\n%s\n" % " ".join(cmd))
                subprocess.check_output(cmd, stderr=subprocess.STDOUT)
                if not args.no_email:
                    write_rqc_log_message("email report generated and mailed to %s" % args.email)
                else:
                    write_rqc_log_message("email report generated, but not sent to anyone")
            except subprocess.CalledProcessError as cpe:
                write_rqc_log_message("failed to generate email report")
                sys.stderr.write("Error calling email report command:\n%s\n" % cpe.output)
        write_rqc_log_message("complete")
    else:
        write_rqc_log_message("failed")
            
    
    RQC_LOG.close()
    
# IN PROGRESS, DATE <FIRSTLINE>
# COMPLETE, DATE <LASTLINE>


