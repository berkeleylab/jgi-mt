#!/usr/bin/env python

import os as _os
import sys as _sys
import qcutils as _qcutils
import string as _string
import fnmatch as _fnmatch

QTRIM_EXT = ".qtrim"
THREE_P_TRIM_EXT = ".3ptrim"
ARTIFACT_EXT = ".artifact"
RRNA_EXT = ".rRNA"
CLEAN_EXT = ".clean"
REMOVED_EXT = ".removed"
ASSEMBLY_EXT = ".mtAssembly"
RNNOTATOR_EXT = ".rnnotator"
MEGAHIT_EXT = ".megahit"
RNNOTATOR_FASTA_NAME = "final_contigs.fa"
MEGAHIT_FASTA_NAME = "final.contigs.fa"
PREPROC_OUTDIR_SUFFIX = ".rRNA.filter"
RQC_PREPROC_EXT = ".anqrpht"

PREPROC_EXT = QTRIM_EXT+THREE_P_TRIM_EXT+ARTIFACT_EXT+RRNA_EXT
UNTRIMMED_PREPROC_EXT = QTRIM_EXT+ARTIFACT_EXT+RRNA_EXT

def get_base(*fastq_name):
    if len(fastq_name) == 0:
        _sys.stderr.write("****zero arguments passed to get_base in jgimt.py\n")
        return
    outbase = "-".join([_qcutils.get_seq_unit_basename(fq) for fq in fastq_name])
    if len(outbase) > 50: #reduce unweildly file names
        outbase = _qcutils.get_seq_unit_basename(fastq_name[0]) + ".." + _qcutils.get_seq_unit_basename(fastq_name[-1])
    return outbase
#end get_base

def get_preproc_dir(seq_unit):
    ret = get_base(seq_unit)+PREPROC_OUTDIR_SUFFIX
    return ret
#end get_preproc_dir

def get_filtered_fastq(seq_unit,legacy=False):
    outdir = get_preproc_dir(seq_unit)
    base = get_base(seq_unit)
    if not legacy:
        return outdir + "/" + base + RQC_PREPROC_EXT + ".fastq.gz"
    else:
        return outdir + "/" + base + PREPROC_EXT + CLEAN_EXT + ".fastq.gz"
#end get_filtered_fastq

def get_rqc_filtered_fastq_name(seq_unit):
    filtered_file_name = _string.replace(seq_unit,".fastq.gz",RQC_PREPROC_EXT + ".fastq.gz")
    return filtered_file_name 
#end get_rqc_filtered_fastq_name

def get_combined_assembly_dir(apid):
    return "combined.%s.mtAssembly" % str(apid)
#end get_combined_assembly_dir

def get_combined_assembly_base(apid):
    return "combined.%s.megahit" % str(apid)
#end get_combined_assembly_dir

def get_combined_assembly_fasta(apid):
    assem_outbase = get_combined_assembly_base(apid)
    metatranscriptome_fasta = "%s/%s" % (assem_outbase, MEGAHIT_FASTA_NAME)
    return metatranscriptome_fasta 
#end get_combined_assembly_fasta

def get_combined_assembly_mapping_dir(comb_asm_apid, fastq):
    fake_ref_name = "%s.%s" % (str(comb_asm_apid), MEGAHIT_FASTA_NAME)
    return get_mapping_dir(fake_ref_name, fastq)
#end get_combined_assembly_mapping_base

def get_combined_assembly_mapping_base(comb_asm_apid, fastq):
    fake_ref_name = "%s.%s" % (str(comb_asm_apid), MEGAHIT_FASTA_NAME)
    return get_mapping_base(fake_ref_name, fastq)
#end get_combined_assembly_mapping_base

def get_assembly_dir(*fastq):
    fq_base = "-".join(map(get_base,fastq))
    return fq_base+ASSEMBLY_EXT
#end get_assembly_dir

def get_assembly_fasta(*fastq):
    assembler = get_assembler(get_assembly_dir(*fastq))
    if assembler == 'megahit':
        return get_megahit_fasta(*fastq)
    else: 
        return get_rnnotator_fasta(*fastq)

def get_rnnotator_base(*fastq):
    fq_base = "-".join(map(get_base,fastq))
    return fq_base+RNNOTATOR_EXT
#end get_assembly_base

def get_rnnotator_fasta(*fastq):
    return "%s/%s" % (get_rnnotator_base(*fastq), RNNOTATOR_FASTA_NAME)
#end get_rnnotator_fasta

def get_megahit_base(*fastq):
    fq_base = "-".join(map(get_base,fastq))
    return fq_base+MEGAHIT_EXT 
#end get_assembly_base

def get_megahit_fasta(*fastq):
    return "%s/%s" % (get_megahit_base(*fastq), MEGAHIT_FASTA_NAME)
#end get_megahit_fasta

def get_assembler(assembly_dir):
    for f in _os.listdir(assembly_dir):
        if _fnmatch.fnmatch(f,'*%s'%MEGAHIT_EXT):
            return 'megahit'
        elif _fnmatch.fnmatch(f,'*%s'%RNNOTATOR_EXT):
            return 'rnnotator'
    return None
#end get_assembler

def get_mapping_dir(reference,fastq):
    return "%s.v.%s" % (get_base(fastq), _qcutils.ref_base(str(reference)))
#end get_mapping dir

def get_mapping_base(reference,fastq):
    return "%s.v.%s" % (get_base(fastq), _qcutils.ref_base(str(reference)))
#end get_mapping dir

def get_bam(*args, **kwargs):
    sorted_bam = False
    if 'sorted_bam' in kwargs:
        sorted_bam = kwargs['sorted_bam']

    mapping_base = None
    if 'reference' in kwargs and 'fastq' in kwargs:
        reference = kwargs['reference']
        fastq = kwargs['fastq']
        mapping_base = get_mapping_base(reference, fastq)
    elif 'base' in kwargs:
        mapping_base = kwargs['base']

    if mapping_base is None:
        raise TypeError("get_bam takes either the arguments 'reference' and 'fastq', or the argument 'base'")
    if sorted_bam:
        return "%s.sorted.bam" % (mapping_base)
    else:
        return "%s.bam" % (mapping_base)
#end get_bam

def get_reference_id(analysis_project_id, verbose=False):
    analysis_project  = _qcutils.get_analysis_project(analysis_project_id,verbose)
    if analysis_project['img_dataset_id']:
        return (int(analysis_project['img_dataset_id']), True)
    elif analysis_project['source_analysis_project_id']:
        return (int(analysis_project['source_analysis_project_id']), False)
    return None
#end get_reference_id

def separate_aps(analysis_projects):
    assembly_ap = None
    mapping_self_ap = None
    mapping_other_aps = list()
    for ap in analysis_projects:
        if ap['img_dataset_id'] or ap['source_analysis_project_id']:
            if ap['img_dataset_id']:
                mapping_other_aps.append(ap)
            if ap['source_analysis_project_id']:
                mapping_self_ap = ap
        else:
            assembly_ap = ap
    return (assembly_ap, mapping_self_ap, tuple(mapping_other_aps))
#end separate_aps

def get_proj_info (its_key, verbose=False):
    sequencing_project_id, library_name, sequencing_project_name, seq_unit_name = _qcutils.get_proj_info(its_key,verbose=verbose)[0]
    proposal_id, proposal_title, pi_name, pi_email = _qcutils.get_proposal_info(sequencing_project_id,verbose=verbose)
    
    return { 'sequencing_project_id': sequencing_project_id, 
             'sequencing_project_name': sequencing_project_name,
             'proposal_id': proposal_id, 
             'proposal_title': proposal_title,
             'pi_name': pi_name, 
             'pi_email': pi_email }
#end get_proj_info

def get_mapping_to_combined_aps(source_analysis_project_id, verbose=False):
    assembly_ap = _qcutils.get_analysis_project(source_analysis_project_id,verbose)
    spids = [ sp['sequencing_project_id'] for sp in assembly_ap['sequencing_projects'] ]
    ws = "http://proposals.jgi-psf.org/pmo_webservices/mapping_to_other_analysis_projects?src_ap_id=%d&src_sp_id=%d" 
    aps = list()
    for spid in spids:
        ap = _qcutils.get_pmo_data(ws % (source_analysis_project_id, spid), verbose=verbose)
        aps.append(ap[0]['uss_analysis_project'])
    return aps
#end get_mapping_to_combined_aps

def get_analysis_info(analysis_project_id, verbose=False, seq_unit_name=None, library_name=None):
    info = dict()
    info['analysis_project_id'] = analysis_project_id
    info['analysis_task_id'] = None
    info['is_assembly'] = False
    if verbose:
        _sys.stderr.write("Getting analysis tasks for APID %d\n" % info['analysis_project_id'])
    
    analysis_project = _qcutils.get_analysis_project(analysis_project_id, verbose)
    info['analysis_product_id'] = analysis_project['analysis_product_id']
    info['analysis_project_name'] = analysis_project['analysis_project_name']
    # Get analysis task ID
    if analysis_project['analysis_product_id'] in { 38, 39, 62, 65 }: # this is an alignment
        for at in analysis_project['analysis_tasks']:
            if "Mapping to" in at['analysis_task_type_name']:
                info['analysis_task_id'] = at['analysis_task_id']
                if analysis_project['img_dataset_id']:
                    info['img_dataset_id'] = analysis_project['img_dataset_id']
                elif analysis_project['source_analysis_project_id']:
                    info['source_analysis_project_id'] = analysis_project['source_analysis_project_id']
                else:
                    raise Exception("Could not find img_dataset_id or source_analysis_project_id in AP %d" % analysis_project_id)
                break
    else: # this is an assembly
        info['is_assembly'] = True
        for at in analysis_project['analysis_tasks']:
            # if this is an assembly, stop looking through the rest of the ATs
            if at['analysis_task_type_name'] in { "Assembly", "RNA Assembly", "Co-assembly" }:
                info['analysis_task_id'] = at['analysis_task_id']
                break
    if info['analysis_task_id'] is None:
        raise Exception("Could not find an analysis task for APID %d\n" % info['analysis_project_id'])

    if verbose:
        _sys.stderr.write("Using analysis task id %d\n" % info['analysis_task_id'])

    # Get sequencing project ID
    if verbose:
        _sys.stderr.write("Getting sequencing project ids for ATID %d\n" % info['analysis_task_id'])

    sequencing_project_ids = _qcutils.get_sequencing_project_ids(info['analysis_task_id'], verbose)
    info['sequencing_project_id'] = sequencing_project_ids
    if len(sequencing_project_ids) == 0:
        raise Exception("Did not find any sequencing project IDs for analysis task %d.\n" % info['analysis_task_id'])

    if verbose:
        _sys.stderr.write("Using sequencing project id(s) %s\n" % str(info['sequencing_project_id']))
    

    # Get library and sequence unit information
    if verbose:
        _sys.stderr.write("Getting libraries for SPID(s) %s\n" % str(info['sequencing_project_id']))

    info['library_name'] = list()
    info['seq_unit_name'] = list()
    
    if seq_unit_name:
        info['seq_unit_name'].append(seq_unit_name)
        if library_name:
            info['library_name'].append(library_name)
        else:
            lib = _qcutils.get_library_from_sequnit(seq_unit_name)
            info['library_name'].append(lib[0])
    else:
        for spid in sequencing_project_ids:
            sequence_data = _qcutils.get_usable_libraries(spid, verbose)
            for lib in sequence_data:
                info['library_name'].append(lib)
                for su in sequence_data[lib]:
                    info['seq_unit_name'].append(su)

    #info['library_name'] = tuple(sorted(info['library_name']))
    #info['seq_unit_name'] = tuple(sorted(info['seq_unit_name']))
    
    if verbose:
        _sys.stderr.write("Using library %s and sequence unit %s\n" % (info['library_name'],info['seq_unit_name']))
    
    if verbose:
        _sys.stderr.write("Getting project information for %s\n" % info['library_name'])
    spid = None
    try:
        info['sequencing_project_name'] = list()
        info['proposal_id'] = None
        info['proposal_title'] = None
        info['pi_name'] = None
        info['pi_email'] = None
        for spid in sequencing_project_ids:
            proj_info = get_proj_info(spid)
            info['sequencing_project_name'].append(proj_info['sequencing_project_name'])
            info['proposal_id'] = proj_info['proposal_id']
            info['proposal_title'] = proj_info['proposal_title']
            info['pi_name'] = proj_info['pi_name']
            info['pi_email'] = proj_info['pi_email']
    except StopIteration:
        raise Exception("Could not find project information for %d in ITS\n" % spid)
    return info
