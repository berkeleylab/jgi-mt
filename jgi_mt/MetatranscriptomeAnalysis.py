#!/usr/bin/env python


import sys as _sys
import glob as _glob

import qcutils as _qcutils
import jgi_mt as _jgi_mt
import reporting as _reporting

class MetatranscriptomeAnalysis: 
    
    def __init__(self, analysis_project_id, verbose=False, seq_unit_name=None, library_name=None):
        self.analysis_project_id = analysis_project_id
        self.analysis_task_id = None
        if verbose:
            _sys.stderr.write("Getting analysis tasks for APID %d\n" % self.analysis_project_id)
        
        self.is_assembly = False
        
        # Get analysis task ID
        for at in _qcutils.get_analysis_tasks(self.analysis_project_id, verbose):
            # if this is an assembly, stop looking through the rest of the ATs
            if at['analysis_task_name'] == "Assembly" or at['analysis_task_name'] == "RNA Assembly":
                self.analysis_task_id = at['analysis_task_id']
                self.is_assembly = True
                break
            elif "Mapping to" in at['analysis_task_name']:
                self.analysis_task_id = at['analysis_task_id']
        if self.analysis_task_id is None:
            _sys.stderr.write("Could not find an analysis task for APID %d\n" % self.analysis_project_id)
            _sys.exit(1)
        
        # get the reference ID i.e. taxon OID or source APID
        if not self.is_assembly:
            self.reference_id, self.is_img_ref = _jgi_mt.get_reference_id(self.analysis_project_id, verbose)
        
        if verbose:
            _sys.stderr.write("Using analysis task id %d\n" % self.analysis_task_id)
        
        # Get sequencing project ID
        if verbose:
            _sys.stderr.write("Getting sequencing project ids for ATID %d\n" % self.analysis_task_id)
        
        sequencing_project_ids = _qcutils.get_sequencing_project_ids(self.analysis_task_id, verbose)
        self.sequencing_project_id = None
        if len(sequencing_project_ids) == 1:
            self.sequencing_project_id = sequencing_project_ids[0]
        else:
            if len(sequencing_project_ids) == 0:
                _sys.stderr.write("Did not find any sequencing project IDs for analysis task %d.\n" % self.analysis_task_id)
                
            else:
                _sys.stderr.write("Found multiple SPIDs for analysis task %d. Support for multiple SPIDs not yet implemented.\n" % self.analysis_task_id)
                _sys.exit(1)
        
        if verbose:
            _sys.stderr.write("Using sequencing project id %d\n" % self.sequencing_project_id)
        

        # Get library and sequence unit information
        if verbose:
            _sys.stderr.write("Getting libraries for SPID %d\n" % self.sequencing_project_id)

        self.library_name = list()
        self.seq_unit_name = list()
        
        if seq_unit_name:
            self.seq_unit_name.append(seq_unit_name)
            if library_name:
                self.library_name.append(library_name)
            else:
                lib = _qcutils.get_library_from_sequnit(seq_unit_name)
                self.library_name.append(lib[0])
        else:
            sequence_data = _qcutils.get_usable_libraries(self.sequencing_project_id, verbose)
            for lib in sequence_data:
                self.library_name.append(lib)
                for su in sequence_data[lib]:
                    self.seq_unit_name.append(su)

        self.library_name = tuple(sorted(self.library_name))
        self.seq_unit_name = tuple(sorted(self.seq_unit_name))
        
        if verbose:
            _sys.stderr.write("Using library %s and sequence unit %s\n" % (self.library_name,self.seq_unit_name))
        
        if verbose:
            _sys.stderr.write("Getting project information for %s\n" % self.library_name)
        try:
            proj_info = _reporting.get_proj_info(self.sequencing_project_id,spid=True)
            self.sequencing_project_name = proj_info[1]
            self.proposal_id = proj_info[2]
            self.proposal_title = proj_info[3]
            self.pi_name = proj_info[4]
            self.pi_email = proj_info[5]
        except StopIteration:
            _sys.stderr.write("Could not find project information for %d in ITS\n" % self.sequencing_project_id)
            _sys.exit(1)
