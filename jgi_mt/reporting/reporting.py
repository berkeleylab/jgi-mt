#!/usr/bin/env python

import qcutils as _qcutils
import sys as _sys
import os as _os
import glob as _glob
import jgidb as _jgidb
import fnmatch as _fnmatch
import subprocess as _subprocess
import re as _re
import string as _string
import itertools as _itertools
import time as _time

import pysam as _pysam
import sdm_curl as _sdm_curl
import jgi_mt

def add_standard_release_args(parser):
    parser.add_argument("analysis_project_id", type=int, help="the analysis project ID for this alignment")
    parser.add_argument("--lib", type=str, help="the library name for the data being released")
    parser.add_argument("--sequnit", type=str, default=None, help="the sequence unit used for this analysis")
    parser.add_argument("--base", type=str, help="the basename for files to be released (i.e. sequnit)")
    parser.add_argument("--legacy", action="store_true", default=False, help="Reads were filtered using old pipeline. Do not use RQC single-letter extensions")
    parser.add_argument("--no_trim", action="store_true", default=False, help="reads were NOT 3-prime trimmed during preprocessing")
    parser.add_argument("--filtered", action="store_true", default=False, help="data was filtered here. default is to look in JAMO")
    parser.add_argument("-v", "--verbose", action="store_true", default=False, help="be informative about what is being done")
#end add_standard_release_args

def add_project_info_arguments(parser):
    proj_group = parser.add_argument_group(title="Project info options")
    proj_group.add_argument("-i","--spid",  metavar="ID", help="the sequencing project id. default is to get thru ITS")
    proj_group.add_argument("-n","--name", metavar="NAME", help="the sequencing project name. default is to get through ITS")
#end add_project_info_arguments

def add_reporting_arguments(parser):
    report_group = parser.add_argument_group(title="Reporting options")
    report_group.add_argument("-c", "--comment", type=str, help="additional text to prepend to release email")
    report_group.add_argument("-C", "--comment_file", metavar="FILE", help="additional text to prepend to release email, stored in a file")
#end add_reporting_arguments

def add_jat_arguments(parser):
    jat_group = parser.add_argument_group(title="JAT Options")
    jat_group.add_argument("-j", "--jat_template", metavar="TEMPLATE", type=str, help="release project to JAT using the specified template")
    jat_group.add_argument("--yaml", action="store_true", help="output JAT submission file in YAML format. default is to export JSON")
    jat_group.add_argument("--release_to", metavar="DEST", type=str, nargs='+', default=["img","portal"], help="who to release to [img,portal]")
    jat_group.add_argument("--jat_email", action="store_true", default=False, help="tell JAT to send email in metadata.json")
#end add_jat_arguments

def get_info_from_bbtools_log(log_file,regex):
    with open(log_file,'r') as file_in:
        for line in file_in:
            m = regex.search(line)
            if m:
                return m.group(1)
    return None
#end get_info_from_bbtools_log
    

def get_qc_info(analysis,verbose=False):
    ret = list()
    for i in xrange(len(analysis['sequencing_project_id'])):
        qc_info = { 'library_name': analysis['library_name'][i],
                    'seq_unit_name': analysis['seq_unit_name'][i],
                    'total_reads' : 0,
                    'post_qtrim' : 0,
                    'post_aftrm' : 0,
                    'post_rRNArm' : 0 }
        
        qc_stats = None
        try:
            qc_stats = __get_qc_stats_by_sequnit__(analysis['seq_unit_name'][i], verbose=verbose)
        except Exception as e1:
            if verbose:
                _sys.stderr.write("%s\n" % str(e1))
            try:
                qc_stats = __get_qc_stats_by_spid__(analysis['sequencing_project_id'][i], verbose=verbose)
            except Exception as e2:
                if verbose:
                    _sys.stderr.write("%s\n" % str(e2))
                raise Exception("Cannot find QC stats for sequnit %s or SPID %d\n" % (analysis['seq_unit_name'][i],analysis['sequencing_project_id'][i]))
        qc_info['total_reads'] = qc_stats['total_reads']
        qc_info['post_qtrim'] =  qc_info['total_reads']*(1.0 - qc_stats['perc_low_quality']/100)
        qc_info['post_aftrm'] =  qc_info['post_qtrim']*(1.0 - qc_stats['perc_artifact']/100)
        qc_info['post_rRNArm'] = qc_info['post_aftrm']*(1.0 - qc_stats['perc_rrna']/100)
        qc_info['filtered_reads'] = qc_stats['filtered_reads']
        qc_info['bbtools_cleaned'] = qc_stats['bbtools_cleaned']
        qc_info['fastq'] = qc_stats['fastq']
        ret.append(qc_info)
    return ret 
#end get_qc_info

def get_mapping_read_counts(mapping_log_file):
    mapped_reads = -1
    total_reads = -1
    with open(mapping_log_file,'r') as log_in:
        for line in log_in:
            if line.startswith("Reads Used"):
                total_reads = float(_re.split("\s+",line)[2])
            elif line.startswith("mapped:"):
                if mapped_reads == -1:
                    mapped_reads = 0
                mapped_reads += float(_re.split("\s+",line)[2])

    return (total_reads,mapped_reads)
#end get_mapping_read_counts

def get_reference_size(bam):
    samfile = _pysam.Samfile(bam,'rb')
    refsize = -1 
    if 'SQ' in samfile.header:
        refsize = reduce(lambda x,y: x+y, _itertools.imap(lambda x: x['LN'], samfile.header['SQ']))
    return refsize 
#end get_reference_size

def __parse_aligner_command_line_options__(cl):
    options = list()
    if 'BBMap' in cl: 
        discard = { "ref", "in", "in2", "out", "build", "ow", "overwrite", "nodisk", "int" }
        options = filter(lambda x: "=" in x, cl[cl.find('BBMap')+6:].split(" "))
        options = filter(lambda x: not x.split("=")[0] in discard, options)
    return " ".join(options)

def get_aligner_info(bam_file):
    ret = { "aligner": "unknown",
            "aligner_version": "unknown" }
    samfile = _pysam.Samfile(bam_file,'rb')
    if 'PG' in samfile.header:
        ret['aligner'] = samfile.header['PG'][0]['ID']
        ret['aligner_version'] = samfile.header['PG'][0]['VN']
        if 'CL' in samfile.header['PG'][0]:
            ret['aligner_parameters'] = __parse_aligner_command_line_options__(samfile.header['PG'][0]['CL']) 
        samfile.close()
    else:
        samfile.close()
    return ret
#end get_aligner_info


def get_mapping_info(directory, legacy=False):
    ret = dict()
    if not legacy:
        for f in _os.listdir(directory):
            if f == "map.log":
                ret['total_reads'],ret['mapped_reads'] = get_mapping_read_counts(_os.path.join(directory,f))
            elif _fnmatch.fnmatch(f,'*.sorted.bam'):
                ret['bam'] = _os.path.abspath(_os.path.join(directory,f))
            elif _fnmatch.fnmatch(f,'*.sorted.bai'):
                ret['bai'] = _os.path.abspath(_os.path.join(directory,f))
            elif _fnmatch.fnmatch(f,'*.unmapped.merged.fastq.gz'):
                ret['unmapped_merged_fastq'] = _os.path.abspath(_os.path.join(directory,f))
            elif _fnmatch.fnmatch(f,'*.unmapped.unmerged.fastq.gz'):
                ret['unmapped_unmerged_fastq'] = _os.path.abspath(_os.path.join(directory,f))
            elif _fnmatch.fnmatch(f,'*.bam.cov'):
                ret['cov'] = _os.path.abspath(_os.path.join(directory,f))
    else:
        for f in _os.listdir(directory):
            if _fnmatch.fnmatch(f,'*.total_reads'):
                ret['total_reads'] = _qcutils.read_int(_os.path.join(directory,f))
            elif _fnmatch.fnmatch(f,'*.mapped_reads'):
                ret['mapped_reads'] = _qcutils.read_int(_os.path.join(directory,f))
            elif _fnmatch.fnmatch(f,'*.sorted.bam'):
                ret['bam'] = _os.path.abspath(_os.path.join(directory,f))
            elif _fnmatch.fnmatch(f,'*.sorted.bai'):
                ret['bai'] = _os.path.abspath(_os.path.join(directory,f))
    ret['reference_size'] = get_reference_size(ret['bam'])
    ret.update(get_aligner_info(ret['bam']))
    return ret
#end get_mapping_info

def get_fastq_filter_params(filter_sh,legacy=False):
    params = dict()
    
    if not legacy: # parse shell script with BBTools commands
        for line in open(filter_sh,"r"):
            if "$BBDUK" in line:
                if 'quality_trimmed' in params:
                    params['artifact_filtered'] = True
                else:
                    for arg in line.strip().split():
                        if "maq" in arg:
                            params['quality_filtered'] = int(arg.split("=")[1])
                        elif "trimq" in arg:
                            params['quality_trimmed'] = int(arg.split("=")[1])
                        elif "maxns" in arg:
                            params['remove_n'] = int(arg.split("=")[1])
                    
                params['artifact_filtered'] = True
    else: #parse shell script with fastqTrimmer commands
        fq_cmd = None
        for line in open(filter_sh,"r"):
            if "$FQTRIMMER" in line:
                fq_cmd = line.strip().split()
            elif "$DUK" in line:
                params['artifact_filtered'] = True
        if fq_cmd is None:
            raise Exception("Couldn't find fastqTrimmer parameters in %s" % filter_sh)
        i = 0
        while i < len(fq_cmd):
            if fq_cmd[i] == "-b":
                params['quality_trimmed'] = int(fq_cmd[i+1])
                i += 1
            elif fq_cmd[i] == "-a":
                params['quality_filtered'] = int(fq_cmd[i+1])
                i += 1
            elif fq_cmd[i] == "-n":
                params['remove_n'] = int(fq_cmd[i+1])
                i += 1
            i += 1
    if len(params) != 4:
        raise Exception("Couldn't find BBDuk parameters in %s" % filter_sh)
    else:
        return params
#end get_fastq_filter_params

def get_taxon_name(taxon_oid):
    cursor = _jgidb.connectDb("IMG").cursor()
    name = cursor.execute("SELECT t.taxon_name FROM taxon t where t.taxon_oid = %s" % str(taxon_oid)).next()[0]
    return name
#end get_taxon_name 

def get_shell_script(run_directory):
    return "%s.sh" % run_directory
#end get_shell_script

def add_analysis_metadata(exporter, analysis):
    exporter.add_metadata("analysis_project_id", [analysis['analysis_project_id']])
    exporter.add_metadata("analysis_task_id", [analysis['analysis_task_id']])
    exporter.add_metadata("sequencing_project_id", analysis['sequencing_project_id'])
    exporter.add_metadata("library_name", analysis['library_name'])
    exporter.add_metadata("seq_unit_name", analysis['seq_unit_name'])
    if 'img_dataset_id' in analysis:
        exporter.add_metadata("img_dataset_id", int(analysis['img_dataset_id']))
    elif 'source_analysis_project_id' in analysis:
        exporter.add_metadata("source_analysis_project_id", [analysis['source_analysis_project_id']])
#end add_analysis_metadata

def write_jat_metadata_file(exporter, directory, yaml=False):
    JAT_REL_PATH = directory+"/metadata.%s"
    if yaml:
        JAT_REL_PATH = JAT_REL_PATH % "yaml"
    else:
        JAT_REL_PATH = JAT_REL_PATH % "json"

    jat_rel_file = open(JAT_REL_PATH, "w")
    if yaml:
        exporter.export_yaml(jat_rel_file)
    else:
        exporter.export_json(jat_rel_file)
    jat_rel_file.close()
#write write_jat_metadata_file

def get_bowtie2_parameters(alignment_sh):
    bowtie2_params = list()
    for line in open(alignment_sh,"r"):
        if "$BOWTIE2" in line:
            cmd = line.strip().split()
            i = 0
            while i < len(cmd):
                if cmd[i] == "-L":
                    bowtie2_params.append(cmd[i])
                    bowtie2_params.append(cmd[i+1])
                    i += 1
                elif cmd[i] == "-p":
                    bowtie2_params.append(cmd[i])
                    bowtie2_params.append(cmd[i+1])
                    i += 1
                elif cmd[i] == "--local":
                    bowtie2_params.append(cmd[i])
                elif cmd[i] == "--phred33":
                    bowtie2_params.append(cmd[i])
                i += 1
    return bowtie2_params
#end get_bowtie2_parameters

def get_qc_stats(query, curl=_qcutils.__get_Curl__(), verbose=False):
    if isinstance(query,str):
        if 'fastq.gz' in query:
            return __get_qc_stats_by_sequnit__(query,curl,verbose)
        else:
            raise Exception("Please provide SPID or SeqUnit\n" % type(query))
    elif isinstance(query,int):
        return __get_qc_stats_by_spid__(query,curl,verbose)
    else:
        raise Exception("Cannot except type %s. Please provide SPID or SeqUnit\n" % type(query))

def __get_qc_stats_by_spid__(sequencing_project_id, curl=_qcutils.__get_Curl__(), verbose=False):
    qdata = {'metadata.sequencing_project_id':sequencing_project_id, 'metadata.jat_label': 'rrna_cleaned_fastq'}
    if verbose:
        _sys.stderr.write("Querying JAMO with %s\n" % str(qdata))
    result = curl.post('api/metadata/query',data=qdata)
    if len(result) == 0:
        raise Exception ("Could not find rrna_cleaned_fastq for %d" % sequencing_project_id)
    if not _qcutils.jamo_copy_complete(result[0]):
        raise Exception ("Copying of rrna_cleaned_fastq file for sequencing_project_id %d has failed or has not completed" % sequencing_project_id)
    metadata = result[0]['metadata']
    ret =  dict()
    ret['total_reads'] = metadata['num_input_reads']
    ret['filtered_reads'] = metadata['num_final_reads']
    ret['perc_low_quality'] = metadata['perc_low_quality']
    ret['perc_artifact'] = metadata['perc_artifact']
    ret['perc_rrna'] = metadata['perc_rrna']
    ret['bbtools_cleaned'] = 'anqtphR' in result[0]['file_name'] or 'anqtp.hR' in result[0]['file_name']
    ret['fastq'] = [ result[0]['file_name'], ]
    return ret

def __get_qc_stats_by_sequnit__(seq_unit_name, curl=_qcutils.__get_Curl__(), verbose=False):
#    filtered_file_name = _string.replace(seq_unit_name,".fastq.gz",".anqtp.hR.fastq.gz")
    filtered_file_name = _string.replace(seq_unit_name,".fastq.gz",".anqrpht.fastq.gz")
    qdata = {'file_name': filtered_file_name}
    if verbose:
        _sys.stderr.write("Querying JAMO with %s\n" % str(qdata))
    result = curl.post('api/metadata/query',data=qdata)
    if len(result) == 0:
        raise Exception ("Could not find fastq %s in JAMO" % filtered_file_name)
    metadata = result[0]['metadata']['metatranscriptome_filter']
    ret =  dict()
    ret['filtered_reads'] = int(metadata['filtered_reads'])
    ret['perc_low_quality'] = float(metadata['perc_low_quality'])
    ret['perc_artifact'] = float(metadata['perc_artifact'])
    ret['perc_rrna'] = float(metadata['perc_rrna'])
    ret['bbtools_cleaned'] = True
    ret['fastq'] = [ result[0]['file_name'], ]
    # get the Raw FastQ to get the number of starting reads
    qdata = {'file_name': seq_unit_name}
    if verbose:
        _sys.stderr.write("Querying JAMO with %s\n" % str(qdata))
    result = curl.post('api/metadata/query',data=qdata)
    if len(result) == 0:
        raise Exception ("Could not find fastq %s in JAMO" % seq_unit_name)
    metadata = result[0]['metadata']
    ret['total_reads'] = metadata['read_stats']['file_num_reads']
    return ret
    
def get_mapping_stats(analysis_project_id, curl=_qcutils.__get_Curl__(), verbose=False):
    qdata = {'metadata.analysis_project_id':analysis_project_id, 'metadata.jat_label': 'metatranscriptome_alignment'}
    if verbose:
        _sys.stderr.write("Querying JAMO at %s with %s\n" % (curl.server, str(qdata)))
    result = curl.post('api/metadata/query',data=qdata)
    if len(result) == 0:
        raise Exception ("Could not find metatranscriptome_alignment for %d" % analysis_project_id)
    ret = { 'analysis_project_id' : analysis_project_id }
    metadata = result[0]['metadata']
    ret['total_reads'] = metadata['num_input_reads']
    ret['mapped_reads'] = metadata['num_aligned_reads']
    ret['aligner'] = metadata['aligner']
    ret['aligner_version'] = metadata['aligner_version']
    ret['aligner_parameters'] = metadata['aligner_parameters']
    ret['bam'] = _os.path.join(result[0]['file_path'],result[0]['file_name'])
    if not _qcutils.jamo_copy_complete(result[0]):
        result = curl.get('api/tape/file/%d'%result[0]['file_id'])
        orig_path = "%s/%s" % (result['origin_file_path'],result['origin_file_name'])
        if verbose:
            _sys.stderr.write("Getting reference size by reading BAM in original file location: %s\n" % orig_path)
        ret['reference_size'] = get_reference_size(orig_path)
    else:
        if verbose:
            _sys.stderr.write("Getting reference size by reading BAM from DNA: %s\n" % ret['bam'])
        
        ret['reference_size'] = get_reference_size(ret['bam'])
    return ret

def verbose_function(func):
    def checker(**kwargs):
        if not 'verbose' in kwargs:
            kwargs['verbose'] = False
        return func(**kwargs)
    return checker
#end checker

def jamo_function(func):
    def checker(**kwargs):
        if not 'curl' in kwargs:
            kwargs['curl'] = _qcutils.__get_Curl__()
        return func(**kwargs)
    return checker

def display_jamo_query(qdata):
    _sys.stderr.write("Querying JAMO with %s\n" % str(qdata))

@verbose_function
@jamo_function
def get_assembly_fasta(**kwargs):
    ''' 
    Returns the assembly fasta for the given SPID or APID
    '''
    curl = kwargs['curl']
    verbose = kwargs['verbose']
    qdata = { 'metadata.jat_label': {'$in': ['metatranscriptome_assembly', 'eukaryotic_metatranscriptome_assembly'] } }
    query = None
    if 'sequencing_project_id' in kwargs:
        query = kwargs['sequencing_project_id']
        qdata['metadata.sequencing_project_id'] = query
    elif 'analysis_project_id' in kwargs:
        query = kwargs['analysis_project_id']
        qdata['metadata.analysis_project_id'] = query
    if verbose:
        display_jamo_query(qdata)
    result = curl.post('api/metadata/query',data=qdata)
    if len(result) == 0:
        raise Exception ("Could not find metatranscriptome_assembly for %d" % query)
    path = None
    if not _qcutils.jamo_copy_complete(result[0]):
        result = curl.get('api/tape/file/%d'%result[0]['file_id'])
        path = "%s/%s" % (result['origin_file_path'],result['origin_file_name'])
        if verbose:
            _sys.stderr.write("Using assembly fasta from original location: %s\n" % path)
    else:
        path = "%s/%s" % (result[0]['file_path'], result[0]['file_name'])
        if verbose:
            _sys.stderr.write("Using assembly fasta from DNA: %s\n" % path)
    return path

@verbose_function
@jamo_function
def get_assembler_info(**kwargs):
    ''' 
    Returns the assembler info for the given SPID or APID assembly
    '''
    curl = kwargs['curl']
    verbose = kwargs['verbose']
    qdata = { 'metadata.jat_label': {'$in': ['metatranscriptome_assembly', 'eukaryotic_metatranscriptome_assembly'] } }
    query = None
    if 'sequencing_project_id' in kwargs:
        query = kwargs['sequencing_project_id']
        qdata['metadata.sequencing_project_id'] = query
    elif 'analysis_project_id' in kwargs:
        query = kwargs['analysis_project_id']
        qdata['metadata.analysis_project_id'] = query
    if verbose:
        display_jamo_query(qdata)
    result = curl.post('api/metadata/query',data=qdata)
    if len(result) == 0:
        raise Exception ("Could not find metatranscriptome_assembly for %d" % query)
    return {'assembler': result[0]['metadata']['assembler'],
            'assembler_version': result[0]['metadata']['assembler_version'],
            'assembler_parameters': result[0]['metadata']['assembler_parameters'] }

def extract_assembler_info(assembly_dir):
    assembly_sh = "%s.sh" % assembly_dir
    assembly_log = "%s.log" % assembly_dir
    assembler = jgi_mt.get_assembler(assembly_dir)
    assembler_version = None
    params = list()
    if assembler == 'megahit':
        assembler_version = get_megahit_version(assembly_dir)
        opts = _glob.glob("%s/*.megahit/opts.txt" % assembly_dir)[0]
        skip_next = False 
        for line in open(opts, "r"):
            if skip_next:
                skip_next = False
            elif line[:-1] == '-o' or line[:-1] == '-r' or line[:-1] == '--out-dir':
                skip_next = True
            else:
                params.append(line[:-1])
    else:
        assembler_version = get_rnnotator_version(assembly_dir)
        for line in open(assembly_sh,"r"):
            #$RNNOTATOR -n 16 -o $OUTDIR/${OUTBASE}.rnnotator -l $OUTDIR/${OUTBASE}.rnnotator.log -strP 300 $INPUT_FASTQ > $OUTDIR/rnnotator.out
            if "$RNNOTATOR" in line:
                cmd = line.strip().split()
                i = 0
                while i < len(cmd):
                    if cmd[i] == "-n":
                        params.append(cmd[i])
                        params.append(cmd[i+1])
                        i += 1
                    elif cmd[i] == "-strP":
                        params.append(cmd[i])
                        params.append(cmd[i+1])
                        i += 1
                    i += 1
    return { "assembler": assembler,
             "assembler_version": assembler_version,
             "assembler_parameters":  " ".join(params) }
#end get_assembler_info

def get_rnnotator_version(assembly_dir):
    log_file = _glob.glob("%s/*.rnnotator.log"%assembly_dir)[0]
    for line in open(log_file,"r"):
        if "Rnnotator version" in line:
            return line[:-1].split()[-1]
    return None
#end get_rnnotator_version

def get_megahit_version(assembly_dir):
    version = None
    with open("%s/megahit.err" % assembly_dir,'r') as err:
        for line in err:
            if 'MEGAHIT v' in line:
                version = line[line.find('v')+1:-1]
                break
    return version
#end get_megahit_version

def build_seg_qc_template(fastq_file_name, total_reads, perc_low_qual, perc_artifact, perc_rRNA, assembly_fasta, mapped_reads):
    analyst_fullname, analyst_email = _qcutils.get_analyst_info()
    post_qtrim = total_reads*(1.0 - perc_low_qual/100)
    post_aftrm = post_qtrim*(1.0 - perc_artifact/100)
    post_rRNArm = post_aftrm*(1.0 - perc_rRNA/100)
    comment = ""
    comment += "QC Statistics\n"
    comment += "- total reads:             %0.0f\n" % total_reads
    comment += "- low quality:             %0.6f %% (%0.0f/%0.0f)\n" % (perc_low_qual, total_reads - post_qtrim, total_reads)
    comment += "- artifact:                %0.6f %% (%0.0f/%0.0f)\n" % (perc_artifact, post_qtrim - post_aftrm, post_qtrim)
    comment += "- ribosomal RNA:           %0.6f %% (%0.0f/%0.0f)\n" % (perc_rRNA, post_aftrm - post_rRNArm, post_aftrm)
    comment += "- remaining reads:         %0.0f\n" % post_rRNArm
    comment += "\n"
    comment += "Assembly Statistics\n"
    assem_stats = _qcutils.get_fasta_stats(assembly_fasta)
    for key in assem_stats.keys():
        comment += ("- %s:" % key) + " "*(25 - len(key)) + str(assem_stats[key])+"\n"
    comment += "- percent mapped back:     %0.6f %% (%0.0f/%0.0f)\n" % (100*float(mapped_reads)/total_reads, mapped_reads, total_reads) 
    template  = "Analyst=%s\n" % analyst_fullname
    template += "%s\tFORCE_PASS\t10\t10\t\"%s\"\n" % (fastq_file_name,comment.replace("\n","\\n"))
    return template
    

def submit_seg_qc(template, verbose=False):
    bulk_submitter = '/global/dna/projectdirs/PI/rqc/prod/jgi-rqc/rqc_system/support/auto_segment_qc.py'
    seg_qc_cmd = " ".join([ bulk_submitter, "-t", "pass", "-i", template, "-l" ])
    if verbose:
        _sys.stderr.write("%s\n" % seg_qc_cmd)
    proc = _subprocess.check_call(seg_qc_cmd ,shell=True)

def make_reports(prefix, report_builder, json_exporter=None):
    # create text report
    text_file_name = _os.path.abspath("%sREADME.txt" % prefix)
    text = open(text_file_name,"w")
    report_builder.write_text_report(text)
    text.close()
    # create PDF report from LaTeX
    latex_file_name = "%sREADME.tex" % prefix
    latex = open(latex_file_name,"w")
    report_builder.write_latex_report(latex)
    latex.close()
    pdf_file_name = _os.path.abspath(create_latex_pdf(latex_file_name))
    if json_exporter:
        json_exporter.add_output("readme", text_file_name)
        json_exporter.add_output_metadata(text_file_name, "file_format", "text")
        if pdf_file_name:
            json_exporter.add_output("readme", pdf_file_name)
            json_exporter.add_output_metadata(pdf_file_name, "file_format", "pdf")
    return { 'txt': text_file_name, 'pdf': pdf_file_name }

def create_latex_pdf(latex_file_name):
    outdir = _os.path.dirname(_os.path.abspath(latex_file_name))
    if 'NERSC_HOST' in _os.environ.keys():
        if _os.environ['NERSC_HOST'].lower()=='genepool':
            pdf = "module load texlive;pdflatex"
        elif _os.environ['NERSC_HOST'].lower()=='denovo':
            pdf = "shifter --image=bryce911/bbtools -- pdflatex"

    cmd = pdf + ' -output-directory ' + outdir + " " + latex_file_name
    print cmd 
#    cmd = [ 'pdflatex', '-output-directory', outdir, latex_file_name ]
    retcode = _subprocess.check_call(cmd,shell=True,stdout=_subprocess.PIPE)
    if retcode != 0:
        return None
    else:
        pdf_name = _os.path.abspath(latex_file_name).replace(".tex",".pdf")
        return pdf_name

EUK_ASM_APRODS = { 11, 61 }
PROK_ASM_APRODS = { 10, 24 }
PROK_ALN_APRODS = { 38, 39 }
EUK_ALN_APRODS = { 62, 65 }
ASSEMBLY_APRODS = EUK_ASM_APRODS.union(PROK_ASM_APRODS)
MAP_TO_SELF_APRODS =  EUK_ALN_APRODS.union(PROK_ALN_APRODS)


def get_release_procedure(sequencing_product_id, analysis_product_id, verbose=False):
    jat_template = None
    release_cmd = None
    if analysis_product_id in PROK_ASM_APRODS :
        jat_template = "metatranscriptome_assembly"
        release_cmd = "release_mt_assembly.py"
    elif analysis_product_id in PROK_ALN_APRODS :
        jat_template = "metatranscriptome_alignment"
        release_cmd = "release_mt_alignment.py"
    elif analysis_product_id in EUK_ASM_APRODS :
        jat_template = "eukaryotic_metatranscriptome_assembly"
        release_cmd = "release_mt_assembly.py"
    elif analysis_product_id in EUK_ALN_APRODS :
        jat_template = "eukaryotic_metatranscriptome_alignment"
        release_cmd = "release_mt_alignment.py"
    else:
        raise Exception ("Unrecognized analysis product id: %d" % analysis_product_id) 
    return { "jat_template": jat_template, "release_cmd": release_cmd }
