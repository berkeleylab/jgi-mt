#!/usr/bin/env python
import qcutils as _qcutils
import os as _os
import subprocess as _subprocess
import re as _re
from time import localtime as _localtime
from time import strftime as _strftime
import string as _string
import getpass as _getpass
import itertools as _itertools
import textwrap


__TAB__="    "

class ReportBuilder: 
    
    def __init__(self, title="Metatranscriptome Analysis"):
        self.sections = list()
        self.report_title = title
        self.aligner_info = None
        self.assembler_info = None
        self.legacy_cleaning = False
        self.additional_comments = list()
    
    def set_title(self, title):
        self.report_title = title

    def add_additional_comment(self, comment):
        self.additional_comments.append(comment)

    def add_assembly_section(self, assembly_fasta, assembler_info):
        section = { 
                 "type": "assembly",
                 "name": "Metatranscriptome Assembly",
                 "fasta": assembly_fasta,
                 "data": get_report_data_assembly_stats(assembly_fasta) 
               }
        self.assembler_info = assembler_info 
        self.sections.append(section)
    
    def add_alignment_section(self, reference_name, mapping_info):
        section = { 
                 "type": "alignment",
                 "name": "Reference: %s" % reference_name,
                 "data": get_report_data_mapping_data(mapping_info) 
               }
        self.aligner_info = { 'aligner': mapping_info['aligner'],
                              'aligner_parameters': mapping_info['aligner_parameters'],
                              'aligner_version': mapping_info['aligner_version'] } 

        self.sections.append(section)

    def add_cleanedinputfile_section(self,input_file):
        section = {
            "type": "input_file",
            "name": "Data Input",
            "data": [('Preprocessed input file', input_file + " (See separate input file README for preprocessing methods and results)")]
            }
        self.sections.append(section)

    def add_qc_section(self, qc_info):
        section = { 
                 "type": "qc",
                 "name": "QC Statistics",
                 "data": get_report_data_qc_statistics(qc_info) 
               }
        self.legacy_cleaning = not qc_info['bbtools_cleaned']
        self.sections.append(section)

    def add_project_section(self, proj_info):
        section = { 
                 "type": "project",
                 "name": "Project Information",
                 "data": get_report_data_project_info(proj_info) 
               }
         
        self.sections.append(section)

    def write_text_report(self,ostream):
        ostream.write(format_text_header(self.report_title))
        
        if len(self.additional_comments) > 0: 
            for addl_comment in self.additional_comments:
                ostream.write("%s\n" % addl_comment)
            ostream.write("\n")
        
        ### ANALYSIS RESULTS ###
        for section in self.sections:
            ostream.write(format_text_section_name(section['name']))
            if section['type'] == 'assembly':
                ostream.write(format_text_assembly_stats(section['fasta'],section['data']))
            else:
                ostream.write(format_text_report_data(section['data']))
                ostream.write("\n")
        
        ### METHODS AND REFERENCES ###
        ostream.write(format_text_methods_header())
        ostream.write("\n")
#        ostream.write(format_text_methods_section("Data Cleaning"))
#        ostream.write(format_text_methods_data_cleaning(self.legacy_cleaning))
        assembler = None
        aligner = None
        if not self.assembler_info is None:
            ostream.write(format_text_methods_section("Metatranscriptome Assembly"))
            ostream.write(format_text_methods_assembly(self.assembler_info))
            assembler = self.assembler_info['assembler']
        if not self.aligner_info is None:
            ostream.write(format_text_methods_section("Alignment"))
            ostream.write(format_text_methods_alignment(self.aligner_info))
            aligner = self.aligner_info['aligner']
        ostream.write("\n")
        ostream.write(format_text_references(self.legacy_cleaning, assembler=assembler, aligner=aligner))

        ostream.write(format_text_closer())
    
    def write_latex_report(self,ostream):
        ostream.write(format_latex_header(self.report_title))
        
        if len(self.additional_comments) > 0: 
            for addl_comment in self.additional_comments:
                ostream.write("%s\\\\\n" % addl_comment)
            ostream.write("\n")

        ### ANALYSIS RESULTS ###
        for section in self.sections:
            ostream.write(format_latex_section_name(section['name'].replace('#','\#')))
            if section['type'] == 'assembly':
                ostream.write(format_latex_assembly_stats(section['fasta'],section['data']))
            else:
                ostream.write(format_latex_report_data(section['data']))
                ostream.write("\n")
        
        ### METHODS AND REFERENCES ###
        ostream.write(format_latex_methods_header())
        ostream.write("\n\\begin{enumerate}\n")
#        ostream.write(format_latex_methods_section("Data Cleaning"))
#        ostream.write(format_latex_methods_data_cleaning(self.legacy_cleaning))
        assembler = None
        aligner = None
        if not self.assembler_info is None:
            ostream.write(format_latex_methods_section("Metatranscriptome Assembly"))
            ostream.write(format_latex_methods_assembly(self.assembler_info))
            assembler = self.assembler_info['assembler']
        if not self.aligner_info is None:
            ostream.write(format_latex_methods_section("Alignment"))
            ostream.write(format_latex_methods_alignment(self.aligner_info))
            aligner = self.aligner_info['aligner']
        ostream.write("\n\\end{enumerate}\n")
        ostream.write(format_latex_references(self.legacy_cleaning, assembler=assembler, aligner=aligner))

        ostream.write(format_latex_closer())
        
def format_text_header(report_name):
    ret  = "###################################\n"
    ret += "%s\n" % report_name
    ret += "###################################\n"
    ret += "\n"
    return ret
#end format_text_header

def format_latex_header(report_name):
    ret =  "\\documentclass[11pt,a4paper]{article}\n"
    ret += "\\usepackage[T1]{fontenc}\n"
    ret += "\\usepackage[utf8]{inputenc}\n"
    ret += "\\usepackage[parfill]{parskip}\n"
    #ret += "\\usepackage{filemod}\n"
    #ret += "\\usepackage{authblk}\n"
    ret += "\\usepackage{tabularx}\n"
    ret += "\\usepackage{underscore}\n"
    ret += "\\usepackage[margin=0.7in]{geometry}\n"
    #ret += "\\let\\centering\\relax\n"
    ret += "\\title{%s}\n" % report_name
    #ret += "\\date{%s}\n" % _strftime("%B %d, %Y", _localtime())
    ret += "\\date{}\n"
    ret += "\\author{DOE Joint Genome Institute}\n"
    ret += "\n"
    #ret += "\\renewcommand\\Authands{ and }\n"
    ret += "\\begin{document}\n"
    ret += "  \\maketitle\n"
    ret += "\n"
    return ret
#end format_latex_header

def format_text_closer():
    ret = ""
    if _getpass.getuser() == 'qc_user':
        ret += "Report automatically generated by Rolling QC at "+_strftime("%a, %d %b %Y %H:%M:%S", _localtime())+"\n\n"
        ret += "If you have questions, please let us know:\n"
        ret += "Alex Copeland accopeland@lbl.gov\n"
    else:
        analyst_fullname, analyst_email = _qcutils.get_analyst_info()
        ret += "Report generated by "+analyst_fullname+" at "+_strftime("%a, %d %b %Y %H:%M:%S", _localtime())+"\n\n"
        ret += "If you have questions, please let us know:\n"
        ret += "%s %s \n" % (analyst_fullname, analyst_email)
        ret += "Alex Copeland accopeland@lbl.gov\n"
    return ret
#end format_text_closer

def format_latex_closer():
    ret = ""
    if _getpass.getuser() == 'qc_user':
        ret += "\\section*{}\n"
        ret += "Report automatically generated by Rolling QC at "+_strftime("%a, %d %b %Y %H:%M:%S", _localtime())+" \\\\\n"
        ret += "If you have questions, please let us know: \\\\\n"
        ret += "Alex Copeland accopeland@lbl.gov \\\\\n"
        ret += "\\end{document}\n"
        
    else:
        analyst_fullname, analyst_email = _qcutils.get_analyst_info()
        ret += "\\section*{}\n"
        ret += "Report generated by "+analyst_fullname+" at "+_strftime("%a, %d %b %Y %H:%M:%S", _localtime())+" \\\\\n"
        ret += "If you have questions, please let us know: \\\\\n"
        ret += "%s %s \\\\\n" % (analyst_fullname, analyst_email)
        ret += "Alex Copeland accopeland@lbl.gov \\\\\n"
        ret += "\\end{document}\n"
    return ret
#end format_latex_closer

__SECTION_COUNT__ = 1
def format_text_section_name(section_name):
    global __SECTION_COUNT__
    ret = str(__SECTION_COUNT__)+". "+section_name+"\n"
    __SECTION_COUNT__+=1
    return ret
#end format_text_name

def format_latex_section_name(section_name):
    ret = "\section{%s}\n" % section_name
    return ret
#end format_latex_name

def __singularize__(item):
    if isinstance(item,list) or isinstance(item,tuple):
        return ", ".join(str(id) for id in item)
    else:
       return str(item)
#end __singularize__ 

def get_report_data_project_info(analysis_info):
    data = list()
    if 'analysis_project_id' in analysis_info:
        data.append(("Analysis Project ID", str(analysis_info['analysis_project_id'])))
        data.append(("Analysis Task ID", str(analysis_info['analysis_task_id'])))
    if len(analysis_info['sequencing_project_id']) > 1:
        # this is a combined assembly
        data.append(("Analysis Project Name" , __singularize__(analysis_info['analysis_project_name'])))
        
    data.append(("Sequencing Project ID(s)" , __singularize__(analysis_info['sequencing_project_id'])))
    data.append(("Sequencing Project Name(s)" , __singularize__(analysis_info['sequencing_project_name'])))
    data.append(("Library Name(s)" , __singularize__(analysis_info['library_name'])))
    data.append(("Proposal Title" , analysis_info['proposal_title']))
    data.append(("Proposal ID" , str(analysis_info['proposal_id'])))
    data.append(("Principal Investigator" , "%s, %s" % (analysis_info['pi_name'], analysis_info['pi_email'])))
    return data
#end get_report_data_project_info

def __format_ref_size(size):
    ret_size = float(size)
    units = [ 'bp', 'kb', 'Mb', 'Gb' ]
    i = 0
    while i < len(units):
        if ret_size < 1000 or i == len(units)-1:
            break
        else:
            ret_size = ret_size/1000
            i += 1
    return "%.9G %s" % (round(ret_size,3),units[min(i,3)])


def get_report_data_mapping_data(mapping_info):
    data = list()
    if 'img_dataset_id' in mapping_info:
        data.append(("IMG Taxon OID", str(mapping_info['img_dataset_id'])))
    if 'analysis_project_id' in mapping_info:
        data.append(("Analysis Project ID", str(mapping_info['analysis_project_id'])))
    if 'sequencing_project_name' in mapping_info:
        data.append(("Sequencing Project Name" , __singularize__(mapping_info['sequencing_project_name'])))
    data.append(("File Name", _os.path.basename(mapping_info['bam'])))
    data.append(("Reference Size", __format_ref_size(mapping_info['reference_size'])))
    data.append(("Input Reads", str(mapping_info['total_reads'])))
    data.append(("Aligned Reads", str(mapping_info['mapped_reads'])))
    data.append(("Percent Mapped", "%0.6f" % (100*mapping_info['mapped_reads']/float(mapping_info['total_reads']))))
    return data
#end get_report_data_mapping_data

def get_report_data_qc_statistics(qc_info):
    perc_low_qual = (1.0 - qc_info['post_qtrim']/float(qc_info['total_reads']))*100
    perc_artifact = (1.0 - qc_info['post_aftrm']/float(qc_info['post_qtrim']))*100
    perc_rrna =     (1.0 - qc_info['post_rRNArm']/float(qc_info['post_aftrm']))*100
    data = list()
    if 'sequencing_project_name' in qc_info:
        data.append(("Sequencing Project Name" , __singularize__(qc_info['sequencing_project_name'])))
    data.append(("File Names(s)", __singularize__(qc_info['fastq'])))
    data.append(("Library", __singularize__(qc_info['library_name'])))
    data.append(("Sequence Unit(s)", __singularize__(qc_info['seq_unit_name'])))
    data.append(("Total Reads", "%0.0f" % (qc_info['total_reads'])))
    data.append(("Percent Low Quality", "%0.6f (%0.0f/%0.0f)" % (perc_low_qual, qc_info['total_reads'] - qc_info['post_qtrim'], qc_info['total_reads'])))
    data.append(("Percent Artifact", "%0.6f (%0.0f/%0.0f)" % (perc_artifact, qc_info['post_qtrim'] - qc_info['post_aftrm'], qc_info['post_qtrim'])))
    data.append(("Percent Ribosomal RNA", "%0.6f (%0.0f/%0.0f)" % (perc_rrna, qc_info['post_aftrm'] - qc_info['post_rRNArm'], qc_info['post_aftrm'])))
    data.append(("Remaining Reads", "%0.0f" % (qc_info['post_rRNArm'])))
    return data
#end get_report_data_qc_statistics

def format_text_report_data(report_data):
    max_length = max(map(lambda x: len(x[0]), report_data))+4
    ret = ""
    for key, value in report_data:
        ret += (__TAB__+"%s:" + " "*(max_length - len(key)) + "%s\n") % (key,value) 
    return ret
#end format_text_report_data

def format_latex_report_data(report_data):
    ret = "\\begin{tabularx}{\\textwidth}{ l X }\n"
    for (key, value) in report_data:
        ret += "    %s & %s \\\\\n" % (key,value.replace("%","\%"))
    ret += "\\end{tabularx}\n"
    ret += "\n"
    return ret
#end format_latex_report_data

def get_report_data_assembly_stats(assembly_fasta):
    stats = None
    cmd = list()
    stats = ''
    if 'NERSC_HOST' in _os.environ.keys():
        if _os.environ['NERSC_HOST'].lower()=='genepool':
            stats = "module load bbtools;stats.sh" 
        elif _os.environ['NERSC_HOST'].lower()=='denovo':
            stats = "shifter --image=bryce911/bbtools -- stats.sh"
    
    try:
        cmd = [ stats , "format=7", "in=%s" % assembly_fasta ]
        stats_in = _subprocess.Popen(" ".join(cmd), shell=True, stdout=_subprocess.PIPE).stdout
        stats = stats_in.readlines()
    except OSError, e:
        raise e
    return stats
#end get_report_data_assembly_stats

def format_text_assembly_stats(assembly_fasta, assembly_stats):
    ret = ""
    ret += format_text_report_data([("File Name",_os.path.basename(assembly_fasta)),])
    stats_in = _itertools.chain(assembly_stats)
    seq_dat = list()
    l = stats_in.next()[:-1].split("\t")
    r = stats_in.next()[:-1].split("\t")
    for i in xrange(4):
        seq_dat.append((l[i],r[i]))
    
    gc = 100*float(r[-2])
    gc_sd = 100*float(r[-1])
    seq_dat.append(("Percent GC", "%.2f +/- %.2f" %(gc,gc_sd)))
    ret += format_text_report_data(seq_dat)
    ret += stats_in.next()
    for line in stats_in:
        ret += __TAB__+line
    return ret
#end format_text_assembly_stats

def format_latex_assembly_stats(assembly_fasta, assembly_stats):
    ret = ""
    ret += format_latex_report_data([("File Name",_os.path.basename(assembly_fasta)),])
    ret += "\n"

    cmd = [ "stats.sh" , "in=%s" % assembly_fasta ]
    stats_in = _itertools.chain(assembly_stats)
    seq_dat = list()
    l = stats_in.next()[:-1].split("\t")
    r = stats_in.next()[:-1].split("\t")
    for i in xrange(4):
        seq_dat.append((l[i],r[i]))
    ret += "\\begin{tabular}{ l | r }\n"
    ret += "    \\textbf{Base} & \\textbf{Fraction} \\\\\n"
    ret += "\\hline\n"
    for (key, value) in seq_dat:
        ret += "   \\textbf{%s} & %s \\\\\n" % (key,value.replace("%","\%"))
    ret += "\\end{tabular}\n"
    ret += "\n"
    
    gc = 100*float(r[-2])
    gc_sd = 100*float(r[-1])

    
    stats_in.next()
    assem_stats = list()
    assem_stats.append(("Percent GC", "%.2f +/- %.2f" %(gc,gc_sd)))
    for i in xrange(6):
        dat = _re.split(":[ ]*\t",stats_in.next()[:-1],maxsplit=2)
        assem_stats.append((dat[0].replace("%","\\%"),dat[1].replace("%","\\%")))
    ret += format_latex_report_data(assem_stats)
    ret += "\n"
    stats_in.next()
    stats_in.next()
    space_re = _re.compile("[ ]*\t")
    line1 = space_re.split(stats_in.next().strip())
    line2 = space_re.split(stats_in.next().strip())
    line3 = space_re.split(stats_in.next().strip())
    stats_in.next()
    header = map(lambda x: " ".join(x),zip(line1,line2,line3))
    ret += "\\begin{tabular}{ p{2cm} || p{2cm} | p{2cm} }\n"
    ret += "\\textbf{" + "} & \\textbf{".join(header)+"} \\\\\n"
    ret += "\\hline\n"
    #for i in xrange(9):
    #    ret += " & ".join(space_re.split(stats_in.next()[:-1].replace("%","\\%"))) + " \\\\\n"
    for line in stats_in:
        ar = space_re.split(line[:-1].replace("%","\\%"))
        if len(ar) > 2:
            ret += " & ".join(ar) + " \\\\\n"
    ret += "\\end{tabular}\n"
    ret += "\n"
    return ret
#end format_latex_assembly_stats

methods_header = "Metatranscriptome Analysis Steps"
def format_text_methods_header():
    ret  = "===================================\n"
    ret += " %s\n" % methods_header
    ret += "===================================\n"
    ret += "\n"
    return ret
#end format_text_methods_header

def format_latex_methods_header():
    ret = "\\section*{%s}\n" % methods_header
    return ret
#end format_latex_methods_header

__METHODS_SECTION_COUNT__ = 1
def format_text_methods_section(section_name):
    global __METHODS_SECTION_COUNT__ 
    ret = "%d. %s\n" % (__METHODS_SECTION_COUNT__, section_name)
    __METHODS_SECTION_COUNT__ += 1
    return ret
#end format_text_methods_section

def format_latex_methods_section(section_name):
    ret = "\\item %s\n" % section_name
    return ret
#end format_latex_methods_section

def __format_text_methods__(methods):
    ret = ""
    for item, subitems in methods:
        ret += __TAB__+"- %s\n" % item
        if subitems:
            for subitem in subitems:
                ret += __TAB__+__TAB__+"-- %s\n" % subitem
        
    return ret
#end __format_text_methods__
    
def __clean_latex(options):
    return _string.replace(options, "--", "-{}-")

def __format_latex_methods__(methods):
    ret = "\\begin{itemize}\n"
    for item, subitems in methods:
        ret += "\\item %s\n" % __clean_latex(item)
        if subitems:
            ret += "\\begin{itemize}\n"
            for subitem in subitems:
                ret += "\\item %s\n" % __wrap(__clean_latex(subitem))
            ret += "\\end{itemize}\n"
    ret += "\\end{itemize}\n"

    return ret

#def __format_latex_methods__(methods):
#    ret = "\\begin{itemize}\n"
#    for item, subitems in methods:
#        ret += "\\item %s\n" % __clean_latex(item)
#        if subitems:
#            ret += "\\begin{itemize}\n"
#            for subitem in subitems:
#                ret += "\\item %s\n" % __wrap(__clean_latex(item))
#            ret += "\\end{itemize}\n"
#    ret += "\\end{itemize}\n"
#    return ret

def __wrap(text):
    wrapper = textwrap.TextWrapper(width = 60,break_long_words=False,subsequent_indent=__TAB__)
    return "\\\\".join(wrapper.wrap(text))
    
data_cleaning_legacy_line1 = "raw reads quality-trimmed to Q20 using in-house script fastqTrimmer"
data_cleaning_legacy_line2 = "artifacts were removed using in-house tool DUK"
data_cleaning_legacy_line3 = "options: k-mer=25"
data_cleaning_legacy_line4 = "ribosomal RNA and transfer RNA content was removed using bowtie2"
data_cleaning_legacy_line5 = "mapped against SILVA, Greengenes, IMG ribosomal RNA sequences, GtRNAdb, and tRNADB-CE"
data_cleaning_legacy_line6 = "options: seed length 25, --local mode"

data_cleaning_standard_line1 = "raw reads quality-trimmed to Q10 adapter-trimmed using BBDuk"
data_cleaning_standard_line2 = "options: ktrim=r k=25 mink=12 tpe=t tbo=t qtrim=r trimq=10 maq=10 maxns=3 minlen=50"
data_cleaning_standard_line3 = "reads were then filtered for process artifacts using BBDuk "
data_cleaning_standard_line4 = "options: k=16"
data_cleaning_standard_line5 = "ribosomal RNA reads were removed by mapping against a trimmed version of the Silva database using BBMap"
data_cleaning_standard_line6 = "options: fast=t minid=0.90 local=t"
data_cleaning_standard_line7 = "human reads were removed using BBMap"

def __get_methods_data_cleaning__(legacy):
    ret = list()
    if legacy:
        ret.append( [data_cleaning_legacy_line1, None] )
        ret.append( [data_cleaning_legacy_line2,
                    [data_cleaning_legacy_line3]] )
        ret.append( [data_cleaning_legacy_line4,
                    [data_cleaning_legacy_line5,
                     data_cleaning_legacy_line6]] )
    else:
        ret.append( [data_cleaning_standard_line1,
                    [data_cleaning_standard_line2]] )
        ret.append( [data_cleaning_standard_line3,
                    [data_cleaning_standard_line4]] )
        ret.append( [data_cleaning_standard_line5,
                    [data_cleaning_standard_line6]] )
        ret.append( [data_cleaning_standard_line7,None] )
    return ret
#end __get_methods_data_cleaning__

def format_text_methods_data_cleaning(legacy):
    methods = __get_methods_data_cleaning__(legacy)
    return __format_text_methods__(methods)
#end format_text_methods_data_cleaning

def format_latex_methods_data_cleaning(legacy):
    methods = __get_methods_data_cleaning__(legacy)
    return __format_latex_methods__(methods)
#end format_latex_methods_data_cleaning


assembly_rnnotator_line1 = "cleaned reads from all samples were combined"
assembly_rnnotator_line2 = "reads were merged with BBMerge and normalized with BBNorm"
assembly_rnnotator_line3 = "Rnnotator was used to assemble the metatranscriptome"

assembly_megahit_line1 = "cleaned reads from all samples were combined"
assembly_megahit_line2 = "MEGAHIT was used to assemble the metatranscriptome"

def __get_methods_assembly__(assembler_info):
    ret = list()
    assembler = assembler_info['assembler']
    assembler_version = assembler_info['assembler_version']
    assembler_parameters = assembler_info['assembler_parameters']
    if assembler == 'rnnotator':
        ret.append( [assembly_rnnotator_line1, None] )
        ret.append( [assembly_rnnotator_line2, None] )
        ret.append( [assembly_rnnotator_line3, 
                     ["options: %s" % assembler_parameters ,"version: %s" % assembler_version ] ] )
    elif assembler == 'megahit':
        ret.append( [assembly_megahit_line1, None] )
        ret.append( [assembly_megahit_line2, 
                     ["options: %s" % assembler_parameters ,"version: %s" % assembler_version ] ] )
    return ret
#end format_text_methods_assembly

def format_text_methods_assembly(assembler_info):
    methods = __get_methods_assembly__(assembler_info)
    return __format_text_methods__(methods)

def format_latex_methods_assembly(assembler_info):
    methods = __get_methods_assembly__(assembler_info)
    return __format_latex_methods__(methods)
#end format_latex_methods_assembly

mapping_bbmap_line1 = "cleaned reads were mapped to metagenome/isolate reference(s) and the metatranscriptome assembly using BBMap"

mapping_bowtie2_line1 = "cleaned reads were mapped to metagenome/isolate reference(s) using bowtie2"

def __get_methods_alignment__(aligner_info):
    ret = list()
    if aligner_info['aligner'] == 'bowtie2':
        ret.append( [mapping_bowtie2_line1, None ] )
    else:
        ret.append( [mapping_bbmap_line1, None ] )
    if 'aligner_parameters' in aligner_info and len(aligner_info['aligner_parameters'].strip()) > 0:
#        ret[-1][1] = [ "version: %s options: %s" % (aligner_info['aligner_version'],aligner_info['aligner_parameters']) ]
        ret[-1][1] = [ "version: %s" % aligner_info['aligner_version'] ,
                       "options: %s" % aligner_info['aligner_parameters']]
    return ret
#end __get_methods_alignment__

def format_text_methods_alignment(aligner_info):
    methods = __get_methods_alignment__(aligner_info)
    return __format_text_methods__(methods)
#end format_text_methods_alignment

def format_latex_methods_alignment(aligner_info):
    methods = __get_methods_alignment__(aligner_info)
    return __format_latex_methods__(methods)
#end format_latex_methods_alignment

bowtie2_ref = "Fast gapped-read alignment with Bowtie 2. Nature Methods, 2012."
greengenes_ref = "Greengenes, a Chimera-Checked 16S rRNA Gene Database and Workbench Compatible with ARB. Appl Environ Microbiol, 2006."
gtrnadb_ref = "GtRNAdb: A database of transfer RNA genes detected in genomic sequence. Nucl Acids Res, 2009."
rnnotator_ref = "Rnnotator: an automated de novo transcriptome assembly pipeline from stranded RNA-Seq reads. BMC Genomics, 2010."
silva_ref = "The SILVA ribosomal RNA gene database project: improved data processing and web-based tools. Nucl Acids Res, 2012."
trnadb_ref = "tRNADB-CE: tRNA gene database curated manually by experts. Nucl Acids Res, 2011."
bbmap_ref = "BBMap short read aligner, and other bioinformatic tools. http://sourceforge.net/projects/bbmap"
megahit_ref = "MEGAHIT: An ultra-fast single-node solution for large and complex metagenomics assembly via succinct de Bruijn graph. Bioinformatics, 2015."

def __get_refs__(assembler=None, aligner=None, legacy_cleaning=False):
    refs = list()
    if legacy_cleaning:
        refs.append(bowtie2_ref)
        refs.append(greengenes_ref)
        refs.append(gtrnadb_ref)
        refs.append(trnadb_ref)
#    else:
#        refs.append(bbmap_ref)
#    refs.append(silva_ref)
    if assembler:
        if assembler == 'rnnotator':
            refs.append(rnnotator_ref)
        else:
            refs.append(megahit_ref)
    if aligner:
        if aligner == 'bowtie2':
            if not bowtie2_ref in refs:
                refs.append(bowtie2_ref)
        else:
            if not bbmap_ref in refs:
                refs.append(bbmap_ref)
    return refs

def format_text_references(legacy_cleaning, assembler=None, aligner=None):
    refs = __get_refs__(assembler=assembler, aligner=aligner, legacy_cleaning=legacy_cleaning)
    ret = "References\n"
    for ref in refs:
        ret += __TAB__+"- %s\n" % ref
    ret += "\n"
    return ret
#end format_text_methods_references

def format_latex_references(legacy_cleaning, assembler=None, aligner=None):
    refs = __get_refs__(assembler=assembler, aligner=aligner, legacy_cleaning=legacy_cleaning)
    ret  = "\\section*{References}\n"
    ret += "\\begin{itemize}\n"
    ret += "\\renewcommand\labelitemi{--}\n"
    for ref in refs:
        ret += "\\item %s\n" % ref
    ret += "\\end{itemize}\n"
    ret += "\n"
    return ret
#end format_latex_methods_references

