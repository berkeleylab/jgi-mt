# jgi-mt

To update the python virtual environment in ./env/

    ./script/bootstrap

To run the tests described in ./test/

    ./script/test
