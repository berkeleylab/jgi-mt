Run the following command to rename the project:

    ./script/init

Then delete .git:

    rm -rf .git

Create a new git repository:

    git init
    git add .
    git commit -m "Initial project creation from skeleton."

Follow the instructions on bitbucket for pushing this repository up.

Run the tests:

    ./script/test

Start adding your own tests and fuctionality.
